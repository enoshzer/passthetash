#include "Wmiexec.h"

DWORD WmiexecCreateWin32Process(
	IN LPWSTR lpProgram,
	IN OUT PREMOTE_INSTANCE_CONTEXT lpRemoteInstance)
{
	DWORD dwOutParamsLength;
	LPBYTE lpOutParams;
	LPWSTR lpMethodCreate;
	LPWSTR lpWin32Process;

	lpWin32Process = SysAllocString(L"Win32_Process");
	lpMethodCreate = SysAllocString(L"Create");

	if (!GetRemoteObject(
		lpRemoteInstance->sConnectedSocket,
		lpWin32Process,
		lpRemoteInstance,
		lpRemoteInstance->dwCallID,
		lpRemoteInstance->wContextID))
	{
		PRINT_ERROR(L"IWbemServices::GetObject failed\n");
		SysFreeString(lpWin32Process);
		SysFreeString(lpMethodCreate);
		return 0;
	}

	lpRemoteInstance->dwCallID++;

	if (!ExecMethod(
		lpRemoteInstance->sConnectedSocket,
		lpWin32Process,
		lpMethodCreate,
		lpProgram,
		lpRemoteInstance,
		lpRemoteInstance->dwCallID,
		lpRemoteInstance->wContextID,
		&lpOutParams,
		&dwOutParamsLength))
	{
		PRINT_ERROR(L"IWbemServices::ExecMethod failed\n");
		SysFreeString(lpWin32Process);
		SysFreeString(lpMethodCreate);
		return 0;
	}

	if (dwOutParamsLength > (1117 + sizeof(DWORD)))
	{
		return *(DWORD*)&lpOutParams[1117];
	}

	SysFreeString(lpWin32Process);
	SysFreeString(lpMethodCreate);

	return 1;
}

BOOL WmiexecSetProxyBlanket(
	IN OUT PREMOTE_INSTANCE_CONTEXT lpRemoteInstance)
{
	LPVOID lpOldReallocBuffer;
	PCONTEXT_ITEM arrciAlterContext;

	arrciAlterContext = (PCONTEXT_ITEM)calloc(1, sizeof(CONTEXT_ITEM));

	if (arrciAlterContext == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	lpRemoteInstance->AuthInfo.sContextID++;

	arrciAlterContext[0].ContextID = ++lpRemoteInstance->wContextID;
	arrciAlterContext[0].NumTransItems = 1;
	arrciAlterContext[0].AbstractSyntax.Interface = IID_IWbemServices;
	arrciAlterContext[0].TransferSyntax.TransferSyntax = IID_INDR32bit;
	arrciAlterContext[0].TransferSyntax.Ver = 2;

	lpOldReallocBuffer = lpRemoteInstance->AuthInfo.arrAuthenticationContext;
	lpRemoteInstance->AuthInfo.arrAuthenticationContext = 
		(PAUTHENTICATION_CONTEXT)realloc(lpRemoteInstance->AuthInfo.arrAuthenticationContext, 
			((ULONG64)lpRemoteInstance->AuthInfo.sContextID + 1) * sizeof(AUTHENTICATION_CONTEXT));

	if (lpRemoteInstance->AuthInfo.arrAuthenticationContext == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].dwSequenceNumber = 0;
	lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].lpSigningKey = NULL;
	lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].wSigningKeyLength = 0;

	if (!CreateAuthBasedConnection(
		lpRemoteInstance->lpTargetHost,
		lpRemoteInstance->lpHighPort,
		lpRemoteInstance->AuthInfo.lpUsername,
		lpRemoteInstance->AuthInfo.lpDomain,
		lpRemoteInstance->AuthInfo.lpHash,
		DCERPC_PACKET_TYPE_ALTER_CTX,
		1,
		&lpRemoteInstance->AuthInfo.sContextID,
		arrciAlterContext,
		AUTH_INFO_AUTH_LEVEL_PKT,
		&lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].lpSigningKey,
		&lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].wSigningKeyLength,
		&lpRemoteInstance->sConnectedSocket,
		&lpRemoteInstance->dwCallID))
	{
		PRINT_ERROR(L"Alter Context (IWbemServices) authentication failed\n");
		free(arrciAlterContext);
		return FALSE;
	}

	free(arrciAlterContext);
	return TRUE;
}

BOOL WmiexecConnectServer(
	IN OUT PREMOTE_INSTANCE_CONTEXT lpRemoteInstance)
{
	IID* arriidQueryIIDs;
	PCONTEXT_ITEM arrciIRemUnknown;
	INTERFACE_CONTEXT icIWbemLoginClientID;
	INTERFACE_CONTEXT icIWbemServices;

	arrciIRemUnknown = (PCONTEXT_ITEM)calloc(1, sizeof(CONTEXT_ITEM));
	if (arrciIRemUnknown == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	arrciIRemUnknown[0].ContextID = 0;
	arrciIRemUnknown[0].NumTransItems = 1;
	arrciIRemUnknown[0].AbstractSyntax.Interface = IID_IRemUnknown2;
	arrciIRemUnknown[0].TransferSyntax.TransferSyntax = IID_INDR32bit;
	arrciIRemUnknown[0].TransferSyntax.Ver = 2;

	lpRemoteInstance->dwCallID = 0;

	if (!CreateAuthBasedConnection(
		lpRemoteInstance->lpTargetHost,
		lpRemoteInstance->lpHighPort,
		lpRemoteInstance->AuthInfo.lpUsername,
		lpRemoteInstance->AuthInfo.lpDomain,
		lpRemoteInstance->AuthInfo.lpHash,
		DCERPC_PACKET_TYPE_BIND,
		1,
		&lpRemoteInstance->AuthInfo.sContextID,
		arrciIRemUnknown,
		AUTH_INFO_AUTH_LEVEL_PKT,
		&lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].lpSigningKey,
		&lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].wSigningKeyLength,
		&lpRemoteInstance->sConnectedSocket,
		&lpRemoteInstance->dwCallID))
	{
		PRINT_ERROR(L"Failed to establish authenticated connection\n");
		free(arrciIRemUnknown);
		return FALSE;
	}

	free(arrciIRemUnknown);

	arriidQueryIIDs = (IID*)calloc(1, sizeof(IID));

	if (arriidQueryIIDs == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	arriidQueryIIDs[0] = IID_IWbemLoginClientID;

	if (!RemQueryInterface(
		lpRemoteInstance->sConnectedSocket,
		&lpRemoteInstance->dwCallID,
		1, 
		arriidQueryIIDs, 
		lpRemoteInstance, 
		&icIWbemLoginClientID.lpOXID, 
		&icIWbemLoginClientID.uuIPID))
	{
		PRINT_ERROR(L"IRemUnknown2::RemQueryInterface failed\n");
		free(arriidQueryIIDs);
		return FALSE;
	}

	free(arriidQueryIIDs);

	if (!AlterContext(
		lpRemoteInstance->sConnectedSocket, 
		IID_IWbemLoginClientID, 
		&lpRemoteInstance->dwCallID,
		&lpRemoteInstance->wContextID))
	{
		PRINT_ERROR(L"Alter Context (IWbemLoginClientID) failed\n");
		return FALSE;
	}

	if (!SetClientInfo(
		lpRemoteInstance->sConnectedSocket, 
		lpRemoteInstance, 
		icIWbemLoginClientID, 
		lpRemoteInstance->dwCallID, 
		lpRemoteInstance->wContextID))
	{
		PRINT_ERROR(L"IWbemLoginClientID::SetClientInfo failed\n");
		return FALSE;
	}

	if (!AlterContext(
		lpRemoteInstance->sConnectedSocket, 
		IID_IWbemLevel1Login, 
		&lpRemoteInstance->dwCallID, 
		&lpRemoteInstance->wContextID))
	{
		PRINT_ERROR(L"Alter Context (IWbemLevel1Login) failed\n");
		return FALSE;
	}

	if (!EstablishPosition(
		lpRemoteInstance->sConnectedSocket, 
		lpRemoteInstance,
		lpRemoteInstance->icCurrentInterface, 
		lpRemoteInstance->dwCallID, 
		lpRemoteInstance->wContextID))
	{
		PRINT_ERROR(L"IWbemLevel1Login::EstablishPosition failed\n");
		return FALSE;
	}

	lpRemoteInstance->dwCallID++;

	if (!NTLMLogin(
		lpRemoteInstance->sConnectedSocket,
		lpRemoteInstance, 
		lpRemoteInstance->icCurrentInterface, 
		lpRemoteInstance->dwCallID, 
		lpRemoteInstance->wContextID, 
		&icIWbemServices))
	{
		PRINT_ERROR(L"IWbemLevel1Login::NTLMLogin failed\n");
		return FALSE;
	}


	arriidQueryIIDs = (IID*)calloc(2, sizeof(IID));
	if (arriidQueryIIDs == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	arriidQueryIIDs[0] = icIWbemLoginClientID.uuIPID;
	arriidQueryIIDs[1] = lpRemoteInstance->icCurrentInterface.uuIPID;

	if (!RemRelease(
		lpRemoteInstance->sConnectedSocket,
		&lpRemoteInstance->dwCallID,
		2,
		arriidQueryIIDs,
		lpRemoteInstance))
	{
		PRINT_ERROR(L"IRemUnknown2::RemRelease failed\n");
		free(icIWbemLoginClientID.lpOXID);
		free(arriidQueryIIDs);
		return FALSE;
	}
	
	free(lpRemoteInstance->icCurrentInterface.lpOXID);
	free(icIWbemLoginClientID.lpOXID);
	free(arriidQueryIIDs);
	
	lpRemoteInstance->icCurrentInterface.lpOXID = icIWbemServices.lpOXID;
	lpRemoteInstance->icCurrentInterface.uuIPID = icIWbemServices.uuIPID;

	return TRUE;
}

BOOL WmiexecCreateRemoteInstance(
	IN LPWSTR lpTargetHost,
	IN LPWSTR lpUsername,
	IN LPWSTR lpDomain,
	IN LPWSTR lpHash,
	OUT PREMOTE_INSTANCE_CONTEXT* lpRemoteInstance)
{
	SHORT sAuthContextID = 0;
	DWORD dwCallID = 2;
	SOCKET sDcerpc = NULL;
	LPWSTR lpDcerpcPort;
	PCONTEXT_ITEM ciISystemActivator;
	PSERVER_ALIVE2_RESPONSE lpDcerpcInformation;
	
	lpDcerpcPort = SysAllocString(L"135");


	if (!CheckServerAlive(lpTargetHost, lpDcerpcPort, &dwCallID, &lpDcerpcInformation))
	{
		PRINT_ERROR(L"Failed to send ServerAlive2 message\n");
		SysFreeString(lpDcerpcPort);
		return FALSE;
	}

	ciISystemActivator = (PCONTEXT_ITEM)calloc(1, sizeof(CONTEXT_ITEM));

	if (ciISystemActivator == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	ciISystemActivator->ContextID = 1;
	ciISystemActivator->NumTransItems = 1;
	ciISystemActivator->AbstractSyntax.Interface = IID_ISystemActivator;
	ciISystemActivator->AbstractSyntax.InterfaceVer = 0;
	ciISystemActivator->AbstractSyntax.InterfaceVerMinor = 0;
	ciISystemActivator->TransferSyntax.TransferSyntax = IID_INDR32bit;
	ciISystemActivator->TransferSyntax.Ver = 2;

	if (!CreateAuthBasedConnection(
		lpTargetHost,
		lpDcerpcPort,
		lpUsername,
		lpDomain,
		lpHash,
		DCERPC_PACKET_TYPE_BIND,
		1,
		&sAuthContextID,
		ciISystemActivator,
		NULL,
		NULL,
		NULL,
		&sDcerpc,
		&dwCallID))
	{
		PRINT_ERROR(L"Failed to create remote instance\n");
		free(lpDcerpcInformation);
		free(ciISystemActivator);
		SysFreeString(lpDcerpcPort);
		return FALSE;
	}

	free(ciISystemActivator);

	if (!RemoteCreateInstance(
		lpTargetHost, 
		lpDcerpcPort,
		sDcerpc, 
		lpDcerpcInformation, 
		&dwCallID, 
		lpRemoteInstance))
	{
		PRINT_ERROR(L"ISystemActivator::RemoteCreateInstance failed\n");
		DisconnectSocket(sDcerpc, TRUE);
		free(lpDcerpcInformation);
		SysFreeString(lpDcerpcPort);
		return FALSE;
	}

	(*lpRemoteInstance)->AuthInfo.lpUsername = lpUsername;
	(*lpRemoteInstance)->AuthInfo.lpDomain = lpDomain;
	(*lpRemoteInstance)->AuthInfo.lpHash = lpHash;
	(*lpRemoteInstance)->AuthInfo.sContextID = 0;

	DisconnectSocket(sDcerpc, TRUE);
	free(lpDcerpcInformation);
	SysFreeString(lpDcerpcPort);

	return TRUE;
}

VOID FreeRemoteInstanceContext(PREMOTE_INSTANCE_CONTEXT lpRemoteInstance)
{
	if (lpRemoteInstance->icCurrentInterface.lpOXID != NULL)
		free(lpRemoteInstance->icCurrentInterface.lpOXID);
	if (lpRemoteInstance->AuthInfo.arrAuthenticationContext != NULL)
	{
		for (SHORT wAuthCtxIdx = lpRemoteInstance->AuthInfo.sContextID; wAuthCtxIdx >= 0; wAuthCtxIdx--)
		{
			if (&lpRemoteInstance->AuthInfo.arrAuthenticationContext[wAuthCtxIdx] != NULL)
				if (lpRemoteInstance->AuthInfo.arrAuthenticationContext[wAuthCtxIdx].lpSigningKey != NULL)
					free(lpRemoteInstance->AuthInfo.arrAuthenticationContext[wAuthCtxIdx].lpSigningKey);
		}
		free(lpRemoteInstance->AuthInfo.arrAuthenticationContext);
	}

	DisconnectSocket(lpRemoteInstance->sConnectedSocket, TRUE);
	free(lpRemoteInstance);
}

BOOL InitializeNetworking()
{
	return InitializeWinsock();
}

BOOL UninitializeNetworking()
{
	return ClearWinsock();
}