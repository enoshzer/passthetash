#pragma once
#include "Wmiexec_Dcerpc.h"

BOOL CheckServerAlive(
	IN LPWSTR lpHostName, 
	IN LPWSTR lpPort, 
	IN OUT LPDWORD lpdwCallID,
	OUT PSERVER_ALIVE2_RESPONSE* lpServerAlive2);