#include "NetworkConnection.h"

BOOL InitializeWinsock()
{
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		PRINT_ERROR(L"WSAStartup failed %i", WSAGetLastError());
		return FALSE;
	}

	return TRUE;
}

DWORD CreateSocketConnection(IN LPWSTR lpHostName, IN LPWSTR lpPort, OUT SOCKET* lpSocket)
{
	DWORD dwResultError;
	ADDRINFOW aiHints;
	ADDRINFOW* lpaiResult = NULL;

	*lpSocket = INVALID_SOCKET;

	ZeroMemory(&aiHints, sizeof(aiHints));
	aiHints.ai_family = AF_INET;
	aiHints.ai_socktype = SOCK_STREAM;
	aiHints.ai_protocol = IPPROTO_TCP;

	dwResultError = GetAddrInfoW(lpHostName, lpPort, &aiHints, &lpaiResult);
	if (dwResultError != 0)
		return dwResultError;

	for (ADDRINFOW* aiPtr = lpaiResult; aiPtr != NULL; aiPtr = aiPtr->ai_next)
	{
		*lpSocket = socket(aiPtr->ai_family, aiPtr->ai_socktype, aiPtr->ai_protocol);
		if (*lpSocket == INVALID_SOCKET)
		{
			dwResultError = WSAGetLastError();
			WSACleanup();
			return dwResultError;
		}

		dwResultError = connect(*lpSocket, aiPtr->ai_addr, (DWORD)aiPtr->ai_addrlen);
		if (dwResultError == SOCKET_ERROR)
		{
			closesocket(*lpSocket);
			*lpSocket = INVALID_SOCKET;
			continue;
		}

		break;
	}

	FreeAddrInfoW(lpaiResult);
	return 0;
}

BOOL ReadSocketData(IN SOCKET sSocket, OUT LPSTR lpReceiveBuffer, IN DWORD dwBytesToRead, OUT OPTIONAL PDWORD lpdwReadBytes)
{
	DWORD dwBytesRead;

	dwBytesRead = recv(sSocket, lpReceiveBuffer, dwBytesToRead, 0);
	if (lpdwReadBytes != NULL)
		*lpdwReadBytes = dwBytesRead;

	if (!dwBytesRead)
		return FALSE;
	else if (dwBytesRead == SOCKET_ERROR)
		return FALSE;
	else
		return TRUE;
}

BOOL WriteSocketData(IN SOCKET sSocket, IN LPSTR lpWriteBuffer, IN DWORD dwBytesToWrite)
{
	DWORD dwSentBytes;
	DWORD dwTotalSent = 0;

	dwSentBytes = send(sSocket, lpWriteBuffer, dwBytesToWrite, 0);

	if (dwSentBytes == SOCKET_ERROR)
		return FALSE;
	else
	{
		dwTotalSent = dwSentBytes;

		while (dwTotalSent < dwBytesToWrite)
		{
			dwSentBytes = send(sSocket, &lpWriteBuffer[dwTotalSent], (dwBytesToWrite - dwTotalSent), 0);
			dwTotalSent += dwSentBytes;
		}

		return TRUE;
	}
}

BOOL DisconnectSocket(SOCKET sSocket, BOOL bDoneReceiving)
{
	DWORD dwResult;
	
	dwResult = shutdown(sSocket, SD_SEND);
	if (bDoneReceiving && dwResult != SOCKET_ERROR)
		dwResult = closesocket(sSocket);

	return dwResult != SOCKET_ERROR;
}

BOOL DoneReceiveSocket(SOCKET sSocket)
{
	return closesocket(sSocket) != SOCKET_ERROR;
}

BOOL ClearWinsock()
{
	if (WSACleanup() != 0)
	{
		PRINT_ERROR(L"WSACleanup failed %i", WSAGetLastError());
		return FALSE;
	}

	return TRUE;
}