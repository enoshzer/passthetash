#pragma once
#include "Wmiexec_Dcerpc.h"

#pragma pack(1)
typedef struct _SET_CLIENT_ID_REQ {
	ORPCTHIS orpcthis;
	DWORD dwReferentID;
	DWORD dwClientMachineLength;
	DWORD dwUnknown1;
	DWORD dwClientMachineMaxCount;
	LPWSTR lpClientMachine;
	DWORD dwClientProcId;
	DWORD dwReserved;
} SET_CLIENT_ID_REQ, *PSET_CLIENT_ID_REQ;

#pragma pack(1)
typedef struct _SET_CLIENT_ID_RESP {
	DWORD dwUnknown1;
	DWORD dwUnknown2;
	DWORD dwUnknown3;
} SET_CLIENT_ID_RESP, *PSET_CLIENT_ID_RESP;

BOOL SetClientInfo(
	IN SOCKET sDcerpc,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	IN INTERFACE_CONTEXT icIWbemLoginClientID,
	IN DWORD dwCallID,
	IN WORD wContextID);