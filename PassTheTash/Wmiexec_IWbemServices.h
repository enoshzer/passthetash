#pragma once
#include "Wmiexec_Dcerpc.h"

#pragma pack(1)
typedef struct _IWBEM_SERVICES_STRING {
	DWORD Signature;
	DWORD dwLength;
	DWORD dwAllocHint;
	DWORD dwMaxLength;
	LPWSTR lpObjectPath;
	LPBYTE lpPadding;
} IWBEM_SERVICES_STRING, *PIWBEM_SERVICES_STRING;

#pragma pack(1)
typedef struct _GET_OBJECT_REQ {
	ORPCTHIS orpcthis;
	IWBEM_SERVICES_STRING ObjectPath;
	DWORD dwFlags;
	DWORD dwReferentID;
	DWORD dwIWbemContext;
	DWORD dwReserved;
} GET_OBJECT_REQ, *PGET_OBJECT_REQ;

#pragma pack(1)
typedef struct _EXEC_METHOD_REQ {
	ORPCTHIS orpcthis;
	IWBEM_SERVICES_STRING ObjectPath;
	IWBEM_SERVICES_STRING MethodName;
	DWORD dwFlags;
	DWORD dwReferentID;
	MInterfacePointer ppNamespace;
} EXEC_METHOD_REQ, *PEXEC_METHOD_REQ;

#pragma pack(1)
typedef struct _IWBEM_CLASS_OBJECT {
	DWORD Signature;
	DWORD dwSize;
	LPBYTE lpData;
} IWBEM_CLASS_OBJECT, *PIWBEM_CLASS_OBJECT;

#pragma pack(1)
typedef struct _OBJREF_CUSTOM_EXEC_METHOD {
	UUID clsid;
	DWORD cbExtension;
	DWORD dwSize;
	IWBEM_CLASS_OBJECT ClassObject;
} OBJREF_CUSTOM_EXEC_METHOD, *POBJREF_CUSTOM_EXEC_METHOD;

#pragma pack(1)
typedef struct _OBJREF_EXEC_METHOD {
	DWORD Signature;
	DWORD Flags;
	UUID iid;
	OBJREF_CUSTOM_EXEC_METHOD u_objref;
} OBJREF_EXEC_METHOD, *POBJREF_EXEC_METHOD;

#pragma pack(1)
typedef struct _EXEC_METHOD_PARAMS {
	DWORD wFirstCommandOffset;
	LPBYTE arrbFirstData;
	WORD wSecondCommandOffset;
	LPBYTE arrbSecondData;
	LPSTR lpCommandText;
	LPBYTE arrbThirdData;
} EXEC_METHOD_PARAMS, *PEXEC_METHOD_PARAMS;

BOOL GetRemoteObject(
	IN SOCKET sDcerpc,
	IN LPWSTR lpObjectPath,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	IN DWORD dwCallID,
	IN WORD wContextID);
BOOL ExecMethod(
	IN SOCKET sDcerpc,
	IN LPWSTR lpObjectPath,
	IN LPWSTR lpMethodName,
	IN LPWSTR lpParameters,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	IN DWORD dwCallID,
	IN WORD wContextID,
	OUT LPBYTE* lpOutParams,
	OUT LPDWORD lpdwOutParamsLength);