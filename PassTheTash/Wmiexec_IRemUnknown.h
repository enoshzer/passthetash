#pragma once
#include "Wmiexec_Dcerpc.h"

#pragma pack(1)
typedef struct _QIRESULT {
	DWORD HResult;
	DWORD PointerVal;
	STDOBJREF StdObjRef;
} QIResult, *PQIResult;

#pragma pack(1)
typedef struct _REM_QUERY_IFACE_REQ {
	ORPCTHIS orpcthis;
	IID iidIPID;
	DWORD Refs;
	WORD IIDs;
	WORD wPadding;
	DWORD dwArraySize;
	IID* arriidIIDs;
} REM_QUERY_IFACE_REQ, *PREM_QUERY_IFACE_REQ;

#pragma pack(1)
typedef struct _REM_QUERY_IFACE_RESP {
	ORPCTHAT orpcthat;
	DWORD PointerVal;
	DWORD ArraySize;
	PQIResult lpQIResults;
	DWORD HResult;
} REM_QUERY_IFACE_RESP, *PREM_QUERY_IFACE_RESP;

#pragma pack(1)
typedef struct _REM_INTERFACE_REF {
	IID iidInterface;
	DWORD dwPublicRefs;
	DWORD dwPrivateRefs;
} REM_INTERFACE_REF, *PREM_INTERFACE_REF;

#pragma pack(1)
typedef struct _REM_RELEASE_REQ {
	ORPCTHIS orpcthis;
	struct _INTERFACE_REFS {
		DWORD dwInterfacesCount;
		DWORD dwInterfacesMaxLength;
		PREM_INTERFACE_REF arrInterfaceRefs;
	} InterfaceRefs;
} REM_RELEASE_REQ, *PREM_RELEASE_REQ;

#pragma pack(1)
typedef struct _REM_RELEASE_RESP {
	ORPCTHAT orpcthat;
	DWORD dwHResult;
} REM_RELEASE_RESP, *PREM_RELEASE_RESP;

BOOL RemQueryInterface(
	IN SOCKET sDcerpc,
	IN OUT LPDWORD lpdwCallID,
	IN WORD wInterfacesCount,
	IN IID* arriidQueryIIDs,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	OUT LPBYTE* arrlpOXID,
	OUT UUID* arruuIPID);
BOOL RemRelease(
	IN SOCKET sDcerpc,
	IN OUT LPDWORD lpdwCallID,
	IN WORD wInterfacesCount,
	IN IID* arriidInterfaces,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance);