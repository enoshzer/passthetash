#include "Wmiexec_IRemUnknown.h"

BOOL CreateRemoteQueryInterface(
	IN CLSID clsidCasualityID,
	IN IID iidIPID,
	IN WORD wIntefaceCount,
	IN IID* lpInterfaces,
	OUT LPWORD lpwRemoteQuerySize,
	OUT PREM_QUERY_IFACE_REQ* lpRemoteQueryInterface);

BOOL GetRemoteQueryIntefaceResponseData(
	IN PREM_QUERY_IFACE_RESP lpRemoteQueryInteface,
	OUT LPBYTE* arrlpOXID,
	OUT UUID* arruuIPID);

BOOL CreateRemoteQueryInterface(
	IN CLSID clsidCasualityID,
	IN IID iidIPID,
	IN WORD wIntefaceCount,
	IN IID* lpInterfaces,
	OUT LPWORD lpwRemoteQuerySize,
	OUT PREM_QUERY_IFACE_REQ* lpRemoteQueryInterface)
{
	WORD wStructSize;
	PREM_QUERY_IFACE_REQ lpRemoteQuery;

	wStructSize = sizeof(REM_QUERY_IFACE_REQ) - sizeof(IID*) + (wIntefaceCount * sizeof(IID));
	lpRemoteQuery = (PREM_QUERY_IFACE_REQ)calloc(1, wStructSize);

	if (lpRemoteQuery == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	lpRemoteQuery->orpcthis.version.MajorVersion = 5;
	lpRemoteQuery->orpcthis.version.MinorVersion = 7;
	lpRemoteQuery->orpcthis.cid = clsidCasualityID;

	lpRemoteQuery->iidIPID = iidIPID;
	lpRemoteQuery->Refs = 5;
	lpRemoteQuery->IIDs = wIntefaceCount;
	lpRemoteQuery->dwArraySize = wIntefaceCount;

	for (WORD wInterfaceIdx = 0; wInterfaceIdx < wIntefaceCount; wInterfaceIdx++)
		((IID*)&lpRemoteQuery->arriidIIDs)[wInterfaceIdx] = lpInterfaces[wInterfaceIdx];

	*lpwRemoteQuerySize = wStructSize;
	*lpRemoteQueryInterface = lpRemoteQuery;
	return TRUE;
}

BOOL GetRemoteQueryIntefaceResponseData(
	IN PREM_QUERY_IFACE_RESP lpRemoteQueryInteface,
	OUT LPBYTE* arrlpOXID,
	OUT UUID* arruuIPID)
{
	BOOL bResult = TRUE;

	for (DWORD dwQIResultIdx = 0; dwQIResultIdx < lpRemoteQueryInteface->ArraySize; dwQIResultIdx++)
	{
		if (((PQIResult)&lpRemoteQueryInteface->lpQIResults)[dwQIResultIdx].HResult != 0)
			bResult = FALSE;
		else
		{
			arrlpOXID[dwQIResultIdx] = (LPBYTE)calloc(8, sizeof(BYTE));
			
			memcpy_s(arrlpOXID[dwQIResultIdx], 8, ((PQIResult)&lpRemoteQueryInteface->lpQIResults)[dwQIResultIdx].StdObjRef.OXID, 8);
			arruuIPID[dwQIResultIdx] = ((PQIResult)&lpRemoteQueryInteface->lpQIResults)[dwQIResultIdx].StdObjRef.IPID;
		}
	}

	return bResult;
}

BOOL RemQueryInterface(
	IN SOCKET sDcerpc,
	IN OUT LPDWORD lpdwCallID,
	IN WORD wInterfacesCount,
	IN IID* arriidQueryIIDs,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	OUT LPBYTE* arrlpOXID,
	OUT UUID* arruuIPID)
{
	BYTE bPaddingLength = 0;
	WORD wAuthInfoLength;
	WORD wStubDataLength;
	WORD wCurrentPacketSize;
	DWORD dwBytesRead;
	LPBYTE lpCurrentPacket;
	LPBYTE lpRequestStubData;
	LPVOID lpOldReallocBuffer;
	PAUTH_INFO lpAuthInfo;


	if (!CreateRemoteQueryInterface(
		lpRemoteInstance->clsCasualityID,
		lpRemoteInstance->icCurrentInterface.uuIPID,
		wInterfacesCount,
		arriidQueryIIDs,
		&wStubDataLength,
		(PREM_QUERY_IFACE_REQ*)&lpRequestStubData))
	{
		PRINT_ERROR(L"Failed to create RemoteQueryInterface header packet\n");
		return FALSE;
	}

	if (wStubDataLength % 16 != 0)
		bPaddingLength = (16 - (wStubDataLength % 16));
	wAuthInfoLength = sizeof(AUTH_INFO) - sizeof(LPVOID) + bPaddingLength;
	lpAuthInfo = (PAUTH_INFO)calloc(wAuthInfoLength, sizeof(BYTE));

	if (lpAuthInfo == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateAuthInfo(AUTH_INFO_AUTH_LEVEL_PKT, bPaddingLength, 0, lpRemoteInstance->AuthInfo.sContextID, NULL, lpAuthInfo);

	wCurrentPacketSize = sizeof(DCE_RPC_OPNUM_REQUEST) +
		wAuthInfoLength + wStubDataLength -
		(sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketSize, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateOpnumRequest(
		wCurrentPacketSize + sizeof(NTLMSSP_VERIFIER),
		sizeof(NTLMSSP_VERIFIER),
		++(*lpdwCallID),
		0,
		3, // IRemUnknown2::RemQueryInterface
		&lpRemoteInstance->uuRemUnknownInterface,
		wStubDataLength,
		wAuthInfoLength,
		lpAuthInfo,
		lpRequestStubData,
		(PDCE_RPC_OPNUM_REQUEST)lpCurrentPacket);
	free(lpRequestStubData);
	free(lpAuthInfo);

	lpOldReallocBuffer = lpCurrentPacket;
	lpCurrentPacket = (LPBYTE)realloc(lpCurrentPacket, wCurrentPacketSize + sizeof(NTLMSSP_VERIFIER));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	if (!CalculateMessageSignature(
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].lpSigningKey,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].wSigningKeyLength,
		lpCurrentPacket,
		wCurrentPacketSize,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].dwSequenceNumber++,
		(PNTLMSSP_VERIFIER)&(lpCurrentPacket[wCurrentPacketSize])))
	{
		PRINT_ERROR(L"Failed to calculate NTLMSSP Verifier\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketSize += sizeof(NTLMSSP_VERIFIER);

	WriteSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize);
	free(lpCurrentPacket);

	wCurrentPacketSize = sizeof(DCE_RPC_OPNUM_RESPONSE) - (sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketSize);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}


	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	if ((*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint <= 0)
	{
		PRINT_ERROR(L"Server error\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketSize = (WORD)(*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint;
	wCurrentPacketSize += (*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).DceRpcPacket.AuthLength;
	wCurrentPacketSize += sizeof(AUTH_INFO) - sizeof(LPVOID);
	if (((*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint % 16 != 0))
		wCurrentPacketSize += (16 - ((*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint) % 16);
	free(lpCurrentPacket);

	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketSize);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	if (!GetRemoteQueryIntefaceResponseData(
		(PREM_QUERY_IFACE_RESP)lpCurrentPacket,
		arrlpOXID,
		arruuIPID))
	{
		PRINT_ERROR(L"Interface query failed\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	free(lpCurrentPacket);
	return TRUE;
}

BOOL RemRelease(
	IN SOCKET sDcerpc,
	IN OUT LPDWORD lpdwCallID,
	IN WORD wInterfacesCount,
	IN IID* arriidInterfaces,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance)
{
	BYTE bPaddingLength = 0;
	WORD wAuthInfoLength;
	WORD wStubDataLength;
	WORD wCurrentPacketSize;
	DWORD dwBytesRead;
	LPBYTE lpCurrentPacket;
	LPBYTE lpRequestStubData;
	LPVOID lpOldReallocBuffer;
	PAUTH_INFO lpAuthInfo;

	wStubDataLength = sizeof(REM_RELEASE_REQ) + 
		(wInterfacesCount * sizeof(REM_INTERFACE_REF)) - sizeof(PREM_INTERFACE_REF);
	lpRequestStubData = (LPBYTE)calloc(wStubDataLength, sizeof(BYTE));

	if (lpRequestStubData == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	((PREM_RELEASE_REQ)lpRequestStubData)->orpcthis.version.MajorVersion = 5;
	((PREM_RELEASE_REQ)lpRequestStubData)->orpcthis.version.MinorVersion = 7;
	((PREM_RELEASE_REQ)lpRequestStubData)->orpcthis.cid = lpRemoteInstance->clsCasualityID;

	((PREM_RELEASE_REQ)lpRequestStubData)->InterfaceRefs.dwInterfacesCount = wInterfacesCount;
	((PREM_RELEASE_REQ)lpRequestStubData)->InterfaceRefs.dwInterfacesMaxLength = wInterfacesCount;

	for (WORD wInterfaceIdx = 0; wInterfaceIdx < wInterfacesCount; wInterfaceIdx++)
	{
		((PREM_INTERFACE_REF)&((PREM_RELEASE_REQ)lpRequestStubData)->
			InterfaceRefs.arrInterfaceRefs)[wInterfaceIdx].iidInterface = arriidInterfaces[wInterfaceIdx];
		((PREM_INTERFACE_REF)&((PREM_RELEASE_REQ)lpRequestStubData)->
			InterfaceRefs.arrInterfaceRefs)[wInterfaceIdx].dwPublicRefs = 5;
		((PREM_INTERFACE_REF)&((PREM_RELEASE_REQ)lpRequestStubData)->
			InterfaceRefs.arrInterfaceRefs)[wInterfaceIdx].dwPrivateRefs = 0;
	}

	if (wStubDataLength % 16 != 0)
		bPaddingLength = (16 - (wStubDataLength % 16));
	wAuthInfoLength = sizeof(AUTH_INFO) - sizeof(LPVOID) + bPaddingLength;
	lpAuthInfo = (PAUTH_INFO)calloc(wAuthInfoLength, sizeof(BYTE));

	if (lpAuthInfo == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateAuthInfo(AUTH_INFO_AUTH_LEVEL_PKT, bPaddingLength, 0, lpRemoteInstance->AuthInfo.sContextID, NULL, lpAuthInfo);

	wCurrentPacketSize = sizeof(DCE_RPC_OPNUM_REQUEST) +
		wAuthInfoLength + wStubDataLength -
		(sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketSize, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateOpnumRequest(
		wCurrentPacketSize + sizeof(NTLMSSP_VERIFIER),
		sizeof(NTLMSSP_VERIFIER),
		++(*lpdwCallID),
		0,
		5, // IRemUnknown2::RemRelease
		&lpRemoteInstance->uuRemUnknownInterface,
		wStubDataLength,
		wAuthInfoLength,
		lpAuthInfo,
		lpRequestStubData,
		(PDCE_RPC_OPNUM_REQUEST)lpCurrentPacket);
	free(lpRequestStubData);
	free(lpAuthInfo);

	lpOldReallocBuffer = lpCurrentPacket;
	lpCurrentPacket = (LPBYTE)realloc(lpCurrentPacket, wCurrentPacketSize + sizeof(NTLMSSP_VERIFIER));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	if (!CalculateMessageSignature(
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].lpSigningKey,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].wSigningKeyLength,
		lpCurrentPacket,
		wCurrentPacketSize,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].dwSequenceNumber++,
		(PNTLMSSP_VERIFIER)&(lpCurrentPacket[wCurrentPacketSize])))
	{
		PRINT_ERROR(L"Failed to calculate NTLMSSP Verifier\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketSize += sizeof(NTLMSSP_VERIFIER);

	WriteSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize);
	free(lpCurrentPacket);

	wCurrentPacketSize = sizeof(DCE_RPC_OPNUM_RESPONSE) - (sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketSize);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	if ((*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint <= 0)
	{
		PRINT_ERROR(L"Server error\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketSize = (WORD)(*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint;
	wCurrentPacketSize += (*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).DceRpcPacket.AuthLength;
	wCurrentPacketSize += sizeof(AUTH_INFO) - sizeof(LPVOID);
	if (((*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint % 16) != 0)
		wCurrentPacketSize += (16 - ((*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint) % 16);
	free(lpCurrentPacket);

	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketSize);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	if (((PREM_RELEASE_RESP)lpCurrentPacket)->dwHResult != 0)
	{
		PRINT_ERROR(L"Release interfaces failed\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	free(lpCurrentPacket);
	return TRUE;
}