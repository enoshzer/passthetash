#pragma once
#include "Mimikatz_Common.h"
#include "Utilities.h"

BOOL GetSid(IN PSID* lpSid, IN HANDLE hSource);
BOOL GetUnicodeString(IN PLSA_UNICODE_STRING lpUnicodeString, IN HANDLE hSource);