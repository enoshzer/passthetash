#include "Mimikatz_Common.h"

BOOL GetSid(IN PSID* lpSid, IN HANDLE hSource)
{
	BYTE nbAuth;
	BOOL bStatus = FALSE;
	DWORD dwSizeSid;
	LPVOID lpDesintaion;
	LPVOID lpOriginalPointer = (PBYTE)*lpSid + 1;

	*lpSid = NULL;
	if (ReadProcessMemory(hSource, lpOriginalPointer, &nbAuth, sizeof(BYTE), NULL))
	{
		lpOriginalPointer = (PBYTE)lpOriginalPointer - 1;
		dwSizeSid = (4 * nbAuth) + 6 + 1 + 1;

		lpDesintaion = LocalAlloc(LPTR, dwSizeSid);
		if (lpDesintaion != NULL)
		{
			*lpSid = (PSID)lpDesintaion;
			bStatus = ReadProcessMemory(hSource, lpOriginalPointer, lpDesintaion, dwSizeSid, NULL);
		}
	}

	return bStatus;
}

BOOL GetUnicodeString(IN PLSA_UNICODE_STRING lpUnicodeString, IN HANDLE hSource)
{
	BOOL bStatus = FALSE;
	LPVOID lpDestination;
	LPVOID lpOriginalPointer;

	lpOriginalPointer = lpUnicodeString->Buffer;
	lpUnicodeString->Buffer = NULL;

	if (lpOriginalPointer && lpUnicodeString->MaximumLength)
	{
		lpDestination = LocalAlloc(LPTR, lpUnicodeString->MaximumLength);
		if (lpDestination != NULL)
		{
			lpUnicodeString->Buffer = (PWSTR)lpDestination;
			bStatus = ReadProcessMemory(hSource, lpOriginalPointer, lpDestination, lpUnicodeString->MaximumLength, NULL);
		}
	}

	return bStatus;
}