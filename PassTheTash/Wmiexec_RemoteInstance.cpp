#include "Wmiexec_RemoteInstance.h"

BOOL AlterContext(
	IN SOCKET sDcerpc,
	IN UUID uuInterface,
	IN OUT LPDWORD lpdwCallID,
	IN OUT LPWORD lpwContextID)
{
	WORD wCurrentPacketSize;
	WORD wOffsetToContextItems;
	DWORD dwBytesRead;
	LPBYTE lpCurrentPacket;

	*lpwContextID += 1;

	if (!CreateAlterContextPacket(
		++(*lpdwCallID),
		uuInterface,
		*lpwContextID,
		0,
		NULL,
		(PDCE_RPC_FULL_REQUEST*)&lpCurrentPacket,
		&wCurrentPacketSize))
	{
		PRINT_ERROR(L"Failed to create AlterContext packet\n");
		return FALSE;
	}

	WriteSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize);
	free(lpCurrentPacket);

	wCurrentPacketSize = sizeof(DCE_RPC_FULL_RESPONSE);

	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketSize);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketSize = sizeof(DCE_RPC_ADDITIONAL_FULL_DATA);
	wCurrentPacketSize += ((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen;
	if (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen != 6)
	{
		wCurrentPacketSize += (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen == 0) ? (2) :
			(6 - (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen % 6));
	}

	wOffsetToContextItems = wCurrentPacketSize - sizeof(DCE_RPC_ADDITIONAL_FULL_DATA);
	wOffsetToContextItems += sizeof(DWORD);

	wCurrentPacketSize += sizeof(CONTEXT_ITEM_ACK);
	wCurrentPacketSize += ((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->DceRpcPacket.AuthLength;
	if (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->DceRpcPacket.AuthLength > 0)
		wCurrentPacketSize += sizeof(AUTH_INFO) - sizeof(LPVOID);

	wCurrentPacketSize -= sizeof(LPBYTE) * 2;
	wCurrentPacketSize -= sizeof(PCONTEXT_ITEM_ACK);
	wCurrentPacketSize -= sizeof(PAUTH_INFO);		 
	free(lpCurrentPacket);

	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketSize);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	if (((PCONTEXT_ITEM_ACK)&lpCurrentPacket[wOffsetToContextItems])[0].Result != 0)
	{
		PRINT_ERROR(L"Alter Context failed\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	free(lpCurrentPacket);
	return TRUE;
}