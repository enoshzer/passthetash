#pragma once
#include "Globals.h"

#define LM_NTLM_HASH_LENGTH			16
#define KULL_M_WIN_BUILD_XP			2600
#define KULL_M_WIN_BUILD_2K3		3790
#define KULL_M_WIN_BUILD_VISTA		6000
#define KULL_M_WIN_BUILD_7			7600
#define KULL_M_WIN_BUILD_8			9200
#define KULL_M_WIN_BUILD_BLUE		9600
#define KULL_M_WIN_BUILD_10_1507	10240
#define KULL_M_WIN_BUILD_10_1511	10586
#define KULL_M_WIN_BUILD_10_1607	14393
#define KULL_M_WIN_BUILD_10_1703	15063
#define KULL_M_WIN_BUILD_10_1709	16299
#define KULL_M_WIN_BUILD_10_1803	17134
#define KULL_M_WIN_BUILD_10_1809	17763
#define KULL_M_WIN_MIN_BUILD_XP		2500
#define KULL_M_WIN_MIN_BUILD_2K3	3000
#define KULL_M_WIN_MIN_BUILD_VISTA	5000
#define KULL_M_WIN_MIN_BUILD_7		7000
#define KULL_M_WIN_MIN_BUILD_8		8000
#define KULL_M_WIN_MIN_BUILD_BLUE	9400
#define KULL_M_WIN_MIN_BUILD_10		9800

typedef struct _MEMORY_ADDRESS {
	LPVOID lpAddress;
	HANDLE hMemory;
} MEMORY_ADDRESS, *PMEMORY_ADDRESS;

typedef struct _MODULE_INFORMATION {
	MEMORY_ADDRESS DllBase;
	ULONG SizeOfImage;
	ULONG TimeDateStamp;
	PLSA_UNICODE_STRING NameDontUseOutsideCallback;
} MODULE_INFORMATION, *PMODULE_INFORMATION;

typedef struct _SEKURLSA_PTH_DATA {
	PLUID	LogonId;
	LPBYTE	NtlmHash;
	BOOL	isReplaceOk;
	HANDLE	hLsassHandle;
} SEKURLSA_PTH_DATA, *PSEKURLSA_PTH_DATA;

typedef enum _OBJECT_INFORMATION_CLASS
{
	ObjectBasicInformation,
	ObjectNameInformation,
	ObjectTypeInformation,
	ObjectAllInformation,
	ObjectDataInformation
} OBJECT_INFORMATION_CLASS, *POBJECT_INFORMATION_CLASS;

typedef struct _OBJECT_BASIC_INFORMATION
{
	ULONG Attributes;
	ACCESS_MASK GrantedAccess;
	ULONG HandleCount;
	ULONG PointerCount;
	ULONG PagedPoolCharge;
	ULONG NonPagedPoolCharge;
	ULONG Reserved[3];
	ULONG NameInfoSize;
	ULONG TypeInfoSize;
	ULONG SecurityDescriptorSize;
	LARGE_INTEGER CreationTime;
} OBJECT_BASIC_INFORMATION, *POBJECT_BASIC_INFORMATION;

typedef struct _LSASS_CREDS_STRUCT {
	SIZE_T tailleStruct;
	ULONG offsetToLuid;
	ULONG offsetToLogonType;
	ULONG offsetToSession;
	ULONG offsetToUsername;
	ULONG offsetToDomain;
	ULONG offsetToCredentials;
	ULONG offsetToPSid;
	ULONG offsetToCredentialManager;
	ULONG offsetToLogonTime;
	ULONG offsetToLogonServer;
} LSASS_CREDS_STRUCT, *PLSASS_CREDS_STRUCT;

typedef struct _MODULE_INFORMATION_PACKAGE {
	MODULE_INFORMATION ModuleInformations;
	BOOL bIsPresent;
	BOOL bIsInit;
} MODULE_INFORMATION_PACKAGE, *PMODULE_INFORMATION_PACKAGE;

typedef struct _OS_CONTEXT {
	DWORD NtMajorVersion;
	DWORD NtMinorVersion;
	DWORD NtBuildNumber;
	HMODULE hNtdllModule;
} OS_CONTEXT, *POS_CONTEXT;

typedef VOID(NTAPI LSA_PROTECT_MEMORY)(IN PVOID Buffer, IN ULONG BufferSize);

typedef LSA_PROTECT_MEMORY *PLSA_PROTECT_MEMORY;
typedef NTSTATUS(*LSASS_ACQUIRE_KEYS)(HANDLE hLsassHandle, PMODULE_INFORMATION lpModuleInformation);
typedef NTSTATUS(*LSASS_HELPER_INIT)();

typedef struct _LSASS_CRYPT_HELPER {
	LSASS_HELPER_INIT lpInitCrypt;
	LSASS_HELPER_INIT lpCleanCrypt;
	LSASS_ACQUIRE_KEYS lpAcquireKeys;
	PLSA_PROTECT_MEMORY* lpLsaProtectMemory;
	PLSA_PROTECT_MEMORY* lpLsaUnprotectMemory;
} LSASS_CRYPT_HELPER, *PLSASS_CRYPT_HELPER;

typedef struct _LOGON_SESSION_DATA {
	const LSASS_CRYPT_HELPER*	lpLsassCryptHelper;
	PLUID						LogonId;
	PLSA_UNICODE_STRING			UserName;
	PLSA_UNICODE_STRING			LogonDomain;
	ULONG						LogonType;
	ULONG						Session;
	PVOID						pCredentials;
	PSID						pSid;
	PVOID						pCredentialManager;
	FILETIME					LogonTime;
	PLSA_UNICODE_STRING			LogonServer;
} LOGON_SESSION_DATA, *PLOGON_SESSION_DATA;

typedef struct _PPTH_FULL_DATA {
	PLOGON_SESSION_DATA lpSessionData;
	PSEKURLSA_PTH_DATA lpPthData;
} PTH_FULL_DATA, *PPTH_FULL_DATA;

extern POS_CONTEXT CurrentOsContext;

BOOL InitOsContext(OUT POS_CONTEXT* pOsContext);