#pragma once
#include "Wmiexec_Dcerpc.h"

#define		ACTVFLAGS_DISABLE_AAA				0x00000002
#define		ACTVFLAGS_ACTIVATE_32_BIT_SERVER	0x00000004
#define		ACTVFLAGS_ACTIVATE_64_BIT_SERVER	0x00000008
#define		ACTVFLAGS_NO_FAILURE_LOG			0x00000020
#define		SPD_FLAG_USE_CONSOLE_SESSION		0x00000001


#pragma pack(1)
typedef struct _CUSTOM_REMOTE_REQUEST_SCM_INFO {
	DWORD ClientImpLevel;
	USHORT cRequestedProtseqs;
	USHORT uPadding;
	DWORD dwReferentID;
	DWORD dwRequestedProtseqsMaxCount;
	USHORT* pRequestedProtseqs;
} CUSTOM_REMOTE_REQUEST_SCM_INFO, *PCUSTOM_REMOTE_REQUEST_SCM_INFO;

#pragma pack(1)
typedef struct _ACTIVATION_CONTEXT_INFO_DATA {
	CommonHeader commonHeader;
	PrivateHeader privateHeader;
	DWORD clientOK;
	DWORD bReserved1;
	DWORD dwReserved1;
	DWORD dwReserved2;
	DWORD dwReferentID;
	DWORD dwNullPointer;
	MInterfacePointer ClientContext;
} ActivationContextInfoData;

#pragma pack(1)
typedef struct _INSTANTIATION_INFO_DATA {
	CommonHeader commonHeader;
	PrivateHeader privateHeader;
	CLSID classId;
	DWORD classCtx;
	DWORD actvflags;
	DWORD fIsSurrogate;
	DWORD cIID;
	DWORD instFlag;
	DWORD dwInterfaceIdsReferentID;
	DWORD thisSize;
	COMVERSION clientCOMVersion;
	DWORD dwInterfaceIdsMaxCount;
	IID* InterfaceIds;
	DWORD dwUnused;
} InstantiationInfoData;

#pragma pack(1)
typedef struct _LOCATION_INFO_DATA {
	CommonHeader commonHeader;
	PrivateHeader privateHeader;
	DWORD machineName;
	DWORD processId;
	DWORD apartmentId;
	DWORD contextId;
} LocationInfoData;

#pragma pack(1)
typedef struct _SCM_REQUEST_INFO_DATA {
	CommonHeader commonHeader;
	PrivateHeader privateHeader;
	DWORD dwReserved;
	DWORD dwReferentID;
	CUSTOM_REMOTE_REQUEST_SCM_INFO remoteRequest;
	WORD wUnused;
	DWORD dwUnused;
} ScmRequestInfoData;

#pragma pack(1)
typedef struct _SECURITY_INFO_DATA {
	CommonHeader commonHeader;
	PrivateHeader privateHeader;
	DWORD dwAuthnFlags;
	DWORD dwReferentID;
	DWORD dwNullPointer;
#pragma pack(1)
	struct _SERVER_INFO {
		DWORD dwReserved1;
		DWORD dwNameReferentID;
		DWORD dwNullPointer;
		DWORD dwReserved2;
		DWORD dwNameLength;
		DWORD dwReserved3;
		DWORD dwMaxNameLength;
		LPBYTE lpName;
	} ServerInfo;
} SecurityInfoData;

#pragma pack(1)
typedef struct _SPECIAL_PROPERTIES_DATA {
	CommonHeader commonHeader;
	PrivateHeader privateHeader;
	ULONG dwSessionId;
	LONG fRemoteThisSessionId;
	LONG fClientImpersonating;
	LONG fPartitionIDPresent;
	DWORD dwDefaultAuthnLvl;
	GUID guidPartition;
	DWORD dwPRTFlags;
	DWORD dwOrigClsctx;
	DWORD dwFlags;
	DWORD Reserved3[8];
	DWORD dwUnused1;
	DWORD dwUnused2;
} SpecialPropertiesData;

#pragma pack(1)
typedef struct _PROPERTIES_OUTPUT_DATA {
	CommonHeader commonHeader;
	PrivateHeader privateHeader;
	DWORD NumInterfaces;
	DWORD InterfacesIdsReferentID;
	DWORD ReturnValuesReferentID;
	DWORD InterfacesPtrsReferentID;
	DWORD InterfacesIdsMaxCount;
	IID* arriidInterfacesIds;
	DWORD ReturnValuesMaxCount;
	DWORD* arrdwReturnValues;
	DWORD InterfacesPtrsMaxCount;
	struct InterfacePtr {
		DWORD InterfacePtrReferentID;
		MInterfacePointer mipInterface;
	} *InterfacesPtrs;
} PropertiesOutputData;

#pragma pack(1)
typedef struct _SCM_REPLY_INFO_DATA {
	CommonHeader commonHeader;
	PrivateHeader privateHeader;
	DWORD dwNullPointer;
	DWORD dwRemoteRequestReferentID;
	BYTE bOXID[8];
	DWORD dwOxidBindingsReferentID;
	IID IRemUnknownInterfacePointerId;
	DWORD dwAuthenticationHint;
	WORD MajorVersion;
	WORD MinorVersion;
	LPBYTE lpUnusedBuffer;
} ScmReplyInfoData;

#pragma pack(1)
typedef struct _REM_CRET_INSTNC_REQ {
	ORPCTHIS orpcthis;
	DWORD Unknown1;
	DWORD dwReferentID;
	MInterfacePointer pActProperties;
} REM_CRET_INSTNC_REQ, *PREM_CRET_INSTNC_REQ;

#pragma pack(1)
typedef struct _REM_CRET_INSTNC_RESP {
	ORPCTHAT orpcthat;
	DWORD dwReferentID;
	MInterfacePointer pActProperties;
} REM_CRET_INSTNC_RESP, *PREM_CRET_INSTNC_RESP;

#pragma pack(1)
typedef struct _OBJREF_CUSTOM_ICONTEXT {
	UUID clsid;
	DWORD cbExtension;
	DWORD dwSize;
	BYTE bIContextBuffer[48];
} OBJREF_CUSTOM_ICONTEXT;

#pragma pack(1)
typedef struct _OBJREF_ICONTEXT {
	DWORD Signature;
	DWORD Flags;
	UUID iid;
	OBJREF_CUSTOM_ICONTEXT u_objref;
} OBJREF_ICONTEXT;

BOOL RemoteCreateInstance(
	IN LPWSTR lpTargetHost,
	IN LPWSTR lpDcerpcPort,
	IN SOCKET sDcerpc,
	IN PSERVER_ALIVE2_RESPONSE lpDcerpcInformation,
	IN OUT LPDWORD lpdwCallID,
	OUT PREMOTE_INSTANCE_CONTEXT* lpRemoteInstance);