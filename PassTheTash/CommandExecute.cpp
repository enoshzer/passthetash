#include "CommandExecute.h"

DWORD WINAPI ReadWriteProcessConsole(LPVOID lpParam);

HANDLE CreateShellRemoteThread(DWORD dwProcessId, HANDLE hWriteHandle, HANDLE hReadHandle)
{
	DWORD dwError;
	SIZE_T stWrittenBytes;
	HANDLE hProcess;
	LPVOID lpRemoteProcessMemory;
	REMOT_THRD_PARAMS rmtParams;

	hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessId);
	if (hProcess == NULL)
	{
		PRINT_ERROR_AUTO(L"OpenProcess");
		return FALSE;
	}

	if (!DuplicateHandle(GetCurrentProcess(), hWriteHandle, hProcess, &rmtParams.hWritePipe, 0, FALSE, DUPLICATE_SAME_ACCESS) ||
		!DuplicateHandle(GetCurrentProcess(), hReadHandle, hProcess, &rmtParams.hReadPipe, 0, FALSE, DUPLICATE_SAME_ACCESS))
	{
		PRINT_ERROR(L"Failed to duplicate anonymous pipes handles\n");
		return FALSE;
	}

	lpRemoteProcessMemory = VirtualAllocEx(hProcess, NULL, sizeof(REMOT_THRD_PARAMS), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	if (lpRemoteProcessMemory == NULL)
	{
		PRINT_ERROR_AUTO(L"VirtualAllocEx");
		return FALSE;
	}
	
	if (!WriteProcessMemory(hProcess, lpRemoteProcessMemory, &rmtParams, sizeof(REMOT_THRD_PARAMS), &stWrittenBytes) ||
		stWrittenBytes != sizeof(REMOT_THRD_PARAMS))
	{
		dwError = GetLastError();
		PRINT_ERROR_AUTO(L"WriteProcessMemory");
		return FALSE;
	}

	return CreateRemoteThread(hProcess, NULL, NULL, ReadWriteProcessConsole, lpRemoteProcessMemory, 0, NULL);
}

DWORD WINAPI ReadWriteProcessConsole(LPVOID lpParam)
{
	DWORD dwError;
	DWORD dwReadBytes;
	DWORD dwWrittenBytes;
	DWORD dwMessageSize;
	LPBYTE lpMessage;
	HANDLE hStdout;
	HANDLE hStdin;
	PREMOT_THRD_PARAMS lprmtParams;

	dwMessageSize = 1024;
	
	lpMessage = (LPBYTE)calloc(dwMessageSize, sizeof(BYTE));
	if (lpMessage == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		ExitProcess(1);
	}

	lprmtParams = (PREMOT_THRD_PARAMS)lpParam;

	hStdin = GetStdHandle(STD_INPUT_HANDLE);
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleMode(hStdin, ENABLE_LINE_INPUT | ENABLE_PROCESSED_INPUT | ENABLE_ECHO_INPUT);
	SetConsoleMode(hStdout, ENABLE_PROCESSED_OUTPUT | DISABLE_NEWLINE_AUTO_RETURN);

	PRINT_SUCCESS(L"Remote process successfully created\n\n");

	while (1)
	{
		Sleep(200);
		do
		{

			if (!ReadFile(lprmtParams->hReadPipe, lpMessage, dwMessageSize, &dwReadBytes, NULL))
			{
				PRINT_ERROR_AUTO(L"ReadFile");
				dwError = GetLastError();
				free(lpMessage);
				ExitProcess(1);
			}

			if (dwReadBytes > 0)
			{
				//if (dwReadBytes == 2)
				if (!WriteConsoleA(hStdout, lpMessage, dwReadBytes, &dwWrittenBytes, NULL))
				{
					PRINT_ERROR_AUTO(L"WriteConsole");
					dwError = GetLastError();
					free(lpMessage);
					ExitProcess(1);
				}
			}
			Sleep(15);
		} while ((dwReadBytes == dwMessageSize) && (lpMessage[dwReadBytes - 1] != '\0'));

		do
		{
			if (!ReadConsoleA(hStdin, lpMessage, dwMessageSize - 1, &dwReadBytes, NULL))
			{
				PRINT_ERROR_AUTO(L"ReadConsole");
				dwError = GetLastError();
				free(lpMessage);
				ExitProcess(1);
			}

			if (!WriteFile(lprmtParams->hWritePipe, lpMessage, dwReadBytes, &dwWrittenBytes, NULL))
			{
				PRINT_ERROR_AUTO(L"WriteFile");
				dwError = GetLastError();
				free(lpMessage);
				ExitProcess(1);
			}
		} while ((dwWrittenBytes == dwMessageSize - 1) && (lpMessage[dwWrittenBytes - 1] != '\0'));
	}

	ExitProcess(0);
}