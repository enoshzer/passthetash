#pragma once
#include <ntstatus.h>
#define SECURITY_WIN32
#define WIN32_NO_STATUS
#include <Windows.h>
#include <bcrypt.h>
#include <iostream>
#include <LsaLookup.h>
#include <TlHelp32.h>
#include <Sspi.h>
#include <Ntsecapi.h>
#include <NTSecPKG.h>
#include <strsafe.h>

#if !defined(NT_SUCCESS)
#define NT_SUCCESS(Status) ((NTSTATUS)(Status) >= 0)
#endif

#if !defined(PRINT_ERROR)
#define PRINT_ERROR(...) (wprintf(L"[-] " TEXT(__FUNCTION__) L" ; " __VA_ARGS__ ))
#endif

#if !defined(PRINT_ERROR_AUTO)
#define PRINT_ERROR_AUTO(func) (wprintf(L"[-] " TEXT(__FUNCTION__) L" ; " func L" (0x%08x)\n", GetLastError()))
#endif

#if !defined(PRINT_INFO)
#define PRINT_INFO(...) (wprintf(L"[*] " __VA_ARGS__))
#endif

#if !defined(PRINT_IDENT)
#define PRINT_IDENT(...) (wprintf(L"\t" __VA_ARGS__))
#endif

#if !defined(PRINT_SUCCESS)
#define PRINT_SUCCESS(...) (wprintf(L"[+] " __VA_ARGS__))
#endif