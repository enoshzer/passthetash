#include "Wmiexec_IOXIDResolver.h"

BOOL CheckServerAlive(
	IN LPWSTR lpHostName,
	IN LPWSTR lpPort,
	IN OUT LPDWORD lpdwCallID,
	OUT PSERVER_ALIVE2_RESPONSE* lpServerAlive2)
{
	WORD wCurrentPacketSize;
	DWORD dwBytesRead;
	SOCKET sDcerpcSocket;
	LPVOID lpCurrentPacket;
	PCONTEXT_ITEM arrBindContexts;

	if (CreateSocketConnection(lpHostName, lpPort, &sDcerpcSocket) != 0)
	{
		PRINT_ERROR(L"Failed to create Winsock socket\n");
		return FALSE;
	}

	arrBindContexts = (PCONTEXT_ITEM)calloc(2, sizeof(CONTEXT_ITEM));

	arrBindContexts[0].ContextID = 0;
	arrBindContexts[0].NumTransItems = 1;
	arrBindContexts[0].AbstractSyntax.InterfaceVer = 0;
	arrBindContexts[0].AbstractSyntax.InterfaceVerMinor = 0;
	arrBindContexts[0].AbstractSyntax.Interface = IID_IObjectExporter;
	arrBindContexts[0].TransferSyntax.Ver = 2;
	arrBindContexts[0].TransferSyntax.TransferSyntax = IID_INDR32bit;

	arrBindContexts[1].ContextID = 1;
	arrBindContexts[1].NumTransItems = 1;
	arrBindContexts[1].AbstractSyntax.InterfaceVer = 0;
	arrBindContexts[1].AbstractSyntax.InterfaceVerMinor = 0;
	arrBindContexts[1].AbstractSyntax.Interface = IID_IObjectExporter;
	arrBindContexts[1].TransferSyntax.Ver = 1;
	arrBindContexts[1].TransferSyntax.TransferSyntax = IID_BindTimeFeature;

	wCurrentPacketSize = sizeof(DCE_RPC_FULL_REQUEST) - sizeof(PAUTH_INFO) - sizeof(PCONTEXT_ITEM) + (2 * sizeof(CONTEXT_ITEM));
	lpCurrentPacket = calloc(wCurrentPacketSize, sizeof(BYTE));
	CreateDceRpcRequest(DCERPC_PACKET_TYPE_BIND, wCurrentPacketSize, 0, *lpdwCallID, 2, arrBindContexts, NULL, (PDCE_RPC_FULL_REQUEST)lpCurrentPacket);
	free(arrBindContexts);

	WriteSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketSize);
	free(lpCurrentPacket);

	wCurrentPacketSize = sizeof(DCE_RPC_FULL_RESPONSE);
	lpCurrentPacket = (PDCE_RPC_FULL_RESPONSE)calloc(wCurrentPacketSize, sizeof(BYTE));

	if (!ReadSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Read socket error\n");
		return FALSE;
	}

	wCurrentPacketSize = sizeof(DCE_RPC_ADDITIONAL_FULL_DATA);
	wCurrentPacketSize += ((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen;
	if (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen != 6)
	{
		wCurrentPacketSize += (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen == 0) ? (2) :
			(6 - (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen % 6));
	}
	wCurrentPacketSize += (2 * sizeof(CONTEXT_ITEM_ACK));

	wCurrentPacketSize -= sizeof(LPBYTE) * 2;
	wCurrentPacketSize -= sizeof(PCONTEXT_ITEM_ACK);
	wCurrentPacketSize -= sizeof(PAUTH_INFO);

	free(lpCurrentPacket);
	lpCurrentPacket = calloc(wCurrentPacketSize, sizeof(BYTE));
	if (!ReadSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Read socket error\n");
		return FALSE;
	}

	wCurrentPacketSize = sizeof(DCE_RPC_OPNUM_REQUEST) - sizeof(LPVOID) - sizeof(PAUTH_INFO) - sizeof(LPBYTE) - sizeof(UUID);
	free(lpCurrentPacket);
	lpCurrentPacket = calloc(wCurrentPacketSize, sizeof(BYTE));
	CreateOpnumRequest(wCurrentPacketSize, 0, *lpdwCallID, 0, 5, NULL, 0, 0, NULL, NULL, (PDCE_RPC_OPNUM_REQUEST)lpCurrentPacket);

	WriteSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketSize);
	free(lpCurrentPacket);

	wCurrentPacketSize = sizeof(DCE_RPC_OPNUM_RESPONSE) - (sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = calloc(wCurrentPacketSize, sizeof(BYTE));

	if (!ReadSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Read socket error\n");
		return FALSE;
	}

	if ((*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint > 0)
	{
		wCurrentPacketSize = (WORD)(*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint;
		free(lpCurrentPacket);

		lpCurrentPacket = calloc(wCurrentPacketSize, sizeof(BYTE));

		if (!ReadSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
			dwBytesRead != wCurrentPacketSize)
		{
			PRINT_ERROR(L"Read socket error\n");
			return FALSE;
		}

		*lpServerAlive2 = (PSERVER_ALIVE2_RESPONSE)lpCurrentPacket;
	}
	else
		free(lpCurrentPacket);

	DisconnectSocket(sDcerpcSocket, TRUE);
	return TRUE;
}