#pragma once
#include "Wmiexec_RemoteInstance.h"
#include "Wmiexec_IOXIDResolver.h"
#include "Wmiexec_IRemUnknown.h"
#include "Wmiexec_ISystemActivator.h"
#include "Wmiexec_IWbemLevel1Login.h"
#include "Wmiexec_IWbemLoginClientID.h"
#include "Wmiexec_IWbemServices.h"

BOOL InitializeNetworking();
BOOL UninitializeNetworking();
VOID FreeRemoteInstanceContext(PREMOTE_INSTANCE_CONTEXT lpRemoteInstance);
BOOL WmiexecCreateRemoteInstance(
	IN LPWSTR lpTargetHost,
	IN LPWSTR lpUsername,
	IN LPWSTR lpDomain,
	IN LPWSTR lpHash,
	OUT PREMOTE_INSTANCE_CONTEXT* lpRemoteInstance);
BOOL WmiexecConnectServer(
	IN OUT PREMOTE_INSTANCE_CONTEXT lpRemoteInstance);
BOOL WmiexecSetProxyBlanket(
	IN OUT PREMOTE_INSTANCE_CONTEXT lpRemoteInstance);
DWORD WmiexecCreateWin32Process(
	IN LPWSTR lpProgram,
	IN OUT PREMOTE_INSTANCE_CONTEXT lpRemoteInstance);