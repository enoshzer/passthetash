#include "Wmiexec_IWbemLoginClientID.h"

BOOL IWbemLoginClientID_SetClientInfo(
	OUT LPWORD lpwStubLength,
	OUT PSET_CLIENT_ID_REQ* lpSetClientIDStub);
BOOL IWbemLoginClientId_SetClientInfoResponse(
	IN PSET_CLIENT_ID_RESP lpSetClientIdResponse);

BOOL IWbemLoginClientID_SetClientInfo(
	OUT LPWORD lpwStubLength,
	OUT PSET_CLIENT_ID_REQ* lpSetClientIDStub)
{
	WORD wStubLength;
	DWORD dwClientMachineLength = 0;
	LPWSTR lpClientMahcineName;
	LPVOID lpOldReallocBuffer;
	PSET_CLIENT_ID_REQ lpClientID;

	lpClientMahcineName = (LPWSTR)calloc(MAX_COMPUTERNAME_LENGTH + 1, sizeof(WCHAR));
	dwClientMachineLength = (MAX_COMPUTERNAME_LENGTH + 1);

	if (lpClientMahcineName == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!GetComputerNameW(lpClientMahcineName, &dwClientMachineLength))
	{
		PRINT_ERROR_AUTO(L"GetComputerNameW");
		free(lpClientMahcineName);
		return FALSE;
	}
	
	dwClientMachineLength++;

	lpOldReallocBuffer = lpClientMahcineName;
	lpClientMahcineName = (LPWSTR)realloc(lpClientMahcineName, dwClientMachineLength * sizeof(WCHAR));
	if (lpClientMahcineName == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	wStubLength = (WORD)(sizeof(SET_CLIENT_ID_REQ) - sizeof(LPBYTE) + (dwClientMachineLength * sizeof(WCHAR)));
	lpClientID = (PSET_CLIENT_ID_REQ)calloc(wStubLength, sizeof(BYTE));

	if (lpClientID == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	lpClientID->orpcthis.version.MajorVersion = 5;
	lpClientID->orpcthis.version.MinorVersion = 7;

	lpClientID->dwReferentID = 0x00020000;

	lpClientID->dwClientMachineLength = dwClientMachineLength;
	lpClientID->dwClientMachineMaxCount = dwClientMachineLength;
	memcpy_s(
		&lpClientID->lpClientMachine,
		dwClientMachineLength * sizeof(WCHAR),
		lpClientMahcineName,
		dwClientMachineLength * sizeof(WCHAR));
	
	*(DWORD*)((LPBYTE)&lpClientID->lpClientMachine + (dwClientMachineLength * sizeof(WCHAR))) = GetCurrentProcessId();

	*lpSetClientIDStub = lpClientID;
	*lpwStubLength = wStubLength;

	return TRUE;
}

BOOL IWbemLoginClientId_SetClientInfoResponse(IN PSET_CLIENT_ID_RESP lpSetClientIdResponse)
{
	return (!lpSetClientIdResponse->dwUnknown1 &&
		!lpSetClientIdResponse->dwUnknown2 &&
		!lpSetClientIdResponse->dwUnknown3);
}

BOOL SetClientInfo(
	IN SOCKET sDcerpc,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	IN INTERFACE_CONTEXT icIWbemLoginClientID,
	IN DWORD dwCallID,
	IN WORD wContextID)
{
	BYTE bPaddingLength = 0;
	WORD wAuthInfoLength;
	WORD wStubDataLength;
	WORD wCurrentPacketSize;
	DWORD dwBytesRead;
	LPBYTE lpCurrentPacket;
	LPBYTE lpRequestStubData;
	LPVOID lpOldReallocBuffer;
	PAUTH_INFO lpAuthInfo;

	if (!IWbemLoginClientID_SetClientInfo(&wStubDataLength, (PSET_CLIENT_ID_REQ*)&lpRequestStubData))
	{
		PRINT_ERROR(L"Failed to generate SetClientInfo stub data\n");
		return FALSE;
	}

	if (wStubDataLength % 16 != 0)
		bPaddingLength = (16 - (wStubDataLength % 16));
	wAuthInfoLength = sizeof(AUTH_INFO) - sizeof(LPVOID) + bPaddingLength;
	lpAuthInfo = (PAUTH_INFO)calloc(wAuthInfoLength, sizeof(BYTE));

	CreateAuthInfo(AUTH_INFO_AUTH_LEVEL_PKT, bPaddingLength, 0, lpRemoteInstance->AuthInfo.sContextID, NULL, lpAuthInfo);

	wCurrentPacketSize = sizeof(DCE_RPC_OPNUM_REQUEST) +
		wAuthInfoLength + wStubDataLength -
		(sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketSize, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateOpnumRequest(
		wCurrentPacketSize + sizeof(NTLMSSP_VERIFIER),
		sizeof(NTLMSSP_VERIFIER),
		dwCallID,
		wContextID,
		3, // IWbemLoginClientID::SetClientInfo
		&icIWbemLoginClientID.uuIPID,
		wStubDataLength,
		wAuthInfoLength,
		lpAuthInfo,
		lpRequestStubData,
		(PDCE_RPC_OPNUM_REQUEST)lpCurrentPacket);
	free(lpRequestStubData);
	free(lpAuthInfo);

	lpOldReallocBuffer = lpCurrentPacket;
	lpCurrentPacket = (LPBYTE)realloc(lpCurrentPacket, wCurrentPacketSize + sizeof(NTLMSSP_VERIFIER));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	if (!CalculateMessageSignature(
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].lpSigningKey,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].wSigningKeyLength,
		lpCurrentPacket,
		wCurrentPacketSize,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].dwSequenceNumber++,
		(PNTLMSSP_VERIFIER)&lpCurrentPacket[wCurrentPacketSize]))
	{
		PRINT_ERROR(L"Failed to calculate NTLMSSP verifier\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketSize += sizeof(NTLMSSP_VERIFIER);

	WriteSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize);
	free(lpCurrentPacket);

	wCurrentPacketSize = sizeof(DCE_RPC_OPNUM_RESPONSE) - (sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketSize);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketSize = (WORD)(((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint +
		((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->DceRpcPacket.AuthLength +
		sizeof(AUTH_INFO) - sizeof(LPVOID));
	if ((((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint > 0) &&
		((((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint % 16) != 0))
		wCurrentPacketSize += (16 - (((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint % 16));
	free(lpCurrentPacket);

	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketSize);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketSize, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketSize)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	if (IWbemLoginClientId_SetClientInfoResponse((PSET_CLIENT_ID_RESP)lpCurrentPacket))
	{
		free(lpCurrentPacket);
		return TRUE;
	}
	else
	{
		free(lpCurrentPacket);
		return FALSE;
	}
}