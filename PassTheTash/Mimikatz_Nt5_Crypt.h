#pragma once
#include "Mimikatz_Process.h"

extern PLSA_PROTECT_MEMORY lpNt5LsaProtectMemory;
extern PLSA_PROTECT_MEMORY lpNt5LsaUnprotectMemory;

NTSTATUS Nt5Clean();
NTSTATUS Nt5Initialize();
NTSTATUS Nt5AcquireKeys(HANDLE hLsassHandle, PMODULE_INFORMATION lpModuleInformation);