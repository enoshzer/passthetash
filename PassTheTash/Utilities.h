#pragma once
#include "Globals.h"

#define		ARGUMENT_KEY_TOKEN		'/'
#define		ARGUMENT_VALUE_TOKEN	':'

typedef struct _KEY_VALUE_PAIR {
	LPWSTR lpKey;
	LPWSTR lpValue;
} KEY_VALUE_PAIR, *PKEY_VALUE_PAIR;

typedef VOID(NTAPI *RtlGetNtVersionNumbers)(IN DWORD *MajorVersion, IN DWORD *MinorVersion, DWORD *BuildNumber);

BOOL GetRandomBytes(IN DWORD dwLength, OUT LPBYTE* lpRandomBytes);
VOID FreeArgumentList(IN DWORD dwArgc, IN PKEY_VALUE_PAIR arrArguments);
BOOL ToUpper(IN LPWSTR szString, IN DWORD dwLength, OUT LPWSTR* lpOutString);
BOOL ConvertStringToHex(IN LPWSTR szString, IN DWORD dwSize, OUT LPBYTE lpOutput);
BOOL GetNtVersionNumbers(OUT LPDWORD major, OUT LPDWORD minor, OUT LPDWORD build);
BOOL ParseArguments(IN DWORD dwArgc, IN LPWSTR* lpArgv, OUT PKEY_VALUE_PAIR* lparrArguments);
BOOL MD5Hash(IN LPBYTE lpBuffer, IN DWORD dwBufferLength, OUT LPBYTE* lpHash, OUT PDWORD lpdwHashLength);
BOOL GetArgumentByKey(IN DWORD dwArgc, IN PKEY_VALUE_PAIR arrArguments, IN LPCWSTR lpcShortKey, IN LPCWSTR lpcLongKey, OUT LPWSTR* lpOutput);
BOOL HmacMD5(IN LPSTR lpBuffer, IN DWORD dwBufferLength, IN LPSTR lpKey, IN DWORD dwKeyLength, OUT LPBYTE* lpHmacHash, OUT PWORD lpwHmacHashLength);
BOOL SearchForBytes(
	IN LPBYTE lpBufferToSearchIn,
	IN DWORD dwBufferToSearchInLength,
	IN LPBYTE lpBufferToSearch,
	IN DWORD dwBufferToSearchLength,
	OUT LPBYTE* lpFirstOccurance);