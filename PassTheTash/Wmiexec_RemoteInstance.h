#pragma once
#include "Wmiexec_Dcerpc.h"

BOOL AlterContext(
	IN SOCKET sDcerpc,
	IN UUID uuInterface,
	IN OUT LPDWORD lpdwCallID,
	IN OUT LPWORD lpwContextID);