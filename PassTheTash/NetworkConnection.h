#pragma once
#define WIN32_NO_STATUS
#define CINTERFACE
#define COBJMACROS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iphlpapi.h>
#include <Windows.h>
#include "Utilities.h"

BOOL ClearWinsock();
BOOL InitializeWinsock();
BOOL DoneReceiveSocket(SOCKET sSocket);
BOOL DisconnectSocket(SOCKET sSocket, BOOL bDoneReceiving);
BOOL WriteSocketData(IN SOCKET sSocket, IN LPSTR lpWriteBuffer, IN DWORD dwBytesToWrite);
DWORD CreateSocketConnection(IN LPWSTR lpHostName, IN LPWSTR lpPort, OUT SOCKET* lpSocket);
BOOL ReadSocketData(IN SOCKET sSocket, OUT LPSTR lpReceiveBuffer, IN DWORD dwBytesToRead, OUT OPTIONAL PDWORD lpdwReadBytes);
