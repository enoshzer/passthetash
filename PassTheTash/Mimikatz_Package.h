#pragma once
#include "Mimikatz_Process.h"
#include "Mimikatz_Utilities.h"

typedef struct _AUTHENTICATION_PACKAGE {
	const wchar_t* Name;
	BOOL bIsValid;
	const wchar_t* ModuleName;
	MODULE_INFORMATION_PACKAGE Module;
} AUTHENTICATION_PACKAGE, *PAUTHENTICATION_PACKAGE;