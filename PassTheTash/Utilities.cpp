#include "Utilities.h"

BOOL GetNtVersionNumbers(OUT LPDWORD major, OUT LPDWORD minor, OUT LPDWORD build)
{
	HMODULE hNtdll;
	RtlGetNtVersionNumbers pfnRtlGetNtVersionNumbers;

	hNtdll = LoadLibraryA("ntdll.dll");

	if (hNtdll == NULL)
		return FALSE;

	pfnRtlGetNtVersionNumbers = (RtlGetNtVersionNumbers)GetProcAddress(hNtdll, "RtlGetNtVersionNumbers");

	if (pfnRtlGetNtVersionNumbers == NULL)
		return FALSE;

	pfnRtlGetNtVersionNumbers(major, minor, build);
	*build &= 0x00007fff;
	return TRUE;
}

VOID FreeArgumentList(IN DWORD dwArgc, IN PKEY_VALUE_PAIR arrArguments)
{
	for (DWORD dwArgIdx = 0; dwArgIdx < dwArgc; dwArgIdx++)
	{
		if ((*(arrArguments + dwArgIdx)).lpValue != NULL &&
			(*(arrArguments + dwArgIdx)).lpKey != NULL)
		{
			free((*(arrArguments + dwArgIdx)).lpKey);
			free((*(arrArguments + dwArgIdx)).lpValue);
		}
	}

	free(arrArguments);
}

BOOL GetArgumentByKey(IN DWORD dwArgc, IN PKEY_VALUE_PAIR arrArguments, IN LPCWSTR lpcShortKey, IN LPCWSTR lpcLongKey, OUT LPWSTR* lpOutput)
{
	for (DWORD dwArgIdx = 0; dwArgIdx < dwArgc; dwArgIdx++)
	{
		if (!lstrcmpiW((*(arrArguments + dwArgIdx)).lpKey, lpcShortKey) ||
			!lstrcmpiW((*(arrArguments + dwArgIdx)).lpKey, lpcLongKey))
		{
			*lpOutput = (*(arrArguments + dwArgIdx)).lpValue;
			return TRUE;
		}
	}

	return FALSE;
}

BOOL ParseArguments(IN DWORD dwArgc, IN LPWSTR* lpArgv, OUT PKEY_VALUE_PAIR* lparrArguments)
{
	DWORD dwKeyLength;
	DWORD dwValueLength;
	LPWSTR lpValueTokenPointer;
	LPWSTR lpCurrentArgument;
	LPWSTR lpKey;
	LPWSTR lpValue;
	PKEY_VALUE_PAIR arrArguments;

	arrArguments = (PKEY_VALUE_PAIR)calloc(dwArgc, sizeof(KEY_VALUE_PAIR));

	if (arrArguments == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	for (DWORD dwArgIdx = 0; dwArgIdx < dwArgc; dwArgIdx++)
	{
		lpCurrentArgument = lpArgv[dwArgIdx];
		if (lpCurrentArgument[0] == ARGUMENT_KEY_TOKEN)
		{
			lpValueTokenPointer = wcsstr(lpCurrentArgument, L":");
			if (lpValueTokenPointer == NULL)
			{
				lpKey = &lpCurrentArgument[1];
				lpValue = NULL;
			}
			else
			{
				dwKeyLength = (DWORD)(lpValueTokenPointer - &lpCurrentArgument[1]) + 1;
				dwValueLength = lstrlenW(&lpValueTokenPointer[1]) + 1;

				lpKey = (LPWSTR)calloc(dwKeyLength, sizeof(WCHAR));
				if (lpKey == NULL)
				{
					PRINT_ERROR_AUTO(L"malloc");
					free(arrArguments);
					return FALSE;
				}

				lpValue = (LPWSTR)calloc(dwValueLength, sizeof(WCHAR));
				if (lpValue == NULL)
				{
					PRINT_ERROR_AUTO(L"calloc");
					free(arrArguments);
					return FALSE;
				}

				wcsncpy_s(lpKey, dwKeyLength, &lpCurrentArgument[1], _TRUNCATE);
				wcsncpy_s(lpValue, dwValueLength, &lpValueTokenPointer[1], _TRUNCATE);
			}
		}
		else
		{
			lpKey = NULL;
			lpValue = lpCurrentArgument;
		}

		*(arrArguments + dwArgIdx) = {
			lpKey,
			lpValue
		};
	}

	*lparrArguments = arrArguments;
	return TRUE;
}

BOOL HmacMD5(IN LPSTR lpBuffer, IN DWORD dwBufferLength, IN LPSTR lpKey, IN DWORD dwKeyLength, OUT LPBYTE* lpHmacHash, OUT PWORD lpwHmacHashLength)
{
	DWORD dwData = 0;
	DWORD dwHashObjectLength = 0;
	ULONG ulHashLength = 0;
	LPBYTE lpbHash = NULL;
	LPBYTE lpbHashObject = NULL;
	NTSTATUS ntStatus = STATUS_UNSUCCESSFUL;
	BCRYPT_ALG_HANDLE hAlgorithm = NULL;
	BCRYPT_HASH_HANDLE hHash = NULL;

	ntStatus = BCryptOpenAlgorithmProvider(&hAlgorithm, BCRYPT_MD5_ALGORITHM, NULL, BCRYPT_ALG_HANDLE_HMAC_FLAG);
	if (!NT_SUCCESS(ntStatus))
		return FALSE;

	ntStatus = BCryptGetProperty(hAlgorithm, BCRYPT_OBJECT_LENGTH, (LPBYTE)&dwHashObjectLength, sizeof(DWORD), &dwData, 0);
	if (!NT_SUCCESS(ntStatus))
	{
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	lpbHashObject = (LPBYTE)calloc(dwHashObjectLength, sizeof(BYTE));
	if (lpbHashObject == NULL)
	{
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	ntStatus = BCryptGetProperty(hAlgorithm, BCRYPT_HASH_LENGTH, (PUCHAR)&ulHashLength, sizeof(DWORD), &dwData, 0);
	if (!NT_SUCCESS(ntStatus))
	{
		free(lpbHashObject);
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	lpbHash = (LPBYTE)calloc(ulHashLength, sizeof(BYTE));
	if (lpbHash == NULL)
	{
		free(lpbHashObject);
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	ntStatus = BCryptCreateHash(hAlgorithm, &hHash, lpbHashObject, dwHashObjectLength, (PUCHAR)lpKey, dwKeyLength, 0);
	if (!NT_SUCCESS(ntStatus))
	{
		free(lpbHash);
		free(lpbHashObject);
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	ntStatus = BCryptHashData(hHash, (PUCHAR)lpBuffer, dwBufferLength, 0);
	if (!NT_SUCCESS(ntStatus))
	{
		BCryptDestroyHash(hHash);
		free(lpbHash);
		free(lpbHashObject);
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	ntStatus = BCryptFinishHash(hHash, (PUCHAR)lpbHash, ulHashLength, 0);
	if (NT_SUCCESS(ntStatus))
	{
		*lpHmacHash = lpbHash;
		*lpwHmacHashLength = (WORD)ulHashLength;
	}
	else
		free(lpbHash);

	BCryptDestroyHash(hHash);
	free(lpbHashObject);
	BCryptCloseAlgorithmProvider(hAlgorithm, 0);

	return NT_SUCCESS(ntStatus);
}

BOOL MD5Hash(IN LPBYTE lpBuffer, IN DWORD dwBufferLength, OUT LPBYTE* lpHash, OUT PDWORD lpdwHashLength)
{
	DWORD dwData = 0;
	DWORD dwHashObjectLength = 0;
	ULONG ulHashLength = 0;
	LPBYTE lpbHash = NULL;
	LPBYTE lpbHashObject = NULL;
	NTSTATUS ntStatus = STATUS_UNSUCCESSFUL;
	BCRYPT_ALG_HANDLE hAlgorithm = NULL;
	BCRYPT_HASH_HANDLE hHash = NULL;

	ntStatus = BCryptOpenAlgorithmProvider(&hAlgorithm, BCRYPT_MD5_ALGORITHM, NULL, 0);
	if (!NT_SUCCESS(ntStatus))
		return FALSE;

	ntStatus = BCryptGetProperty(hAlgorithm, BCRYPT_OBJECT_LENGTH, (LPBYTE)&dwHashObjectLength, sizeof(DWORD), &dwData, 0);
	if (!NT_SUCCESS(ntStatus))
	{
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	lpbHashObject = (LPBYTE)calloc(dwHashObjectLength, sizeof(BYTE));
	if (lpbHashObject == NULL)
	{
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	ntStatus = BCryptGetProperty(hAlgorithm, BCRYPT_HASH_LENGTH, (PUCHAR)&ulHashLength, sizeof(DWORD), &dwData, 0);
	if (!NT_SUCCESS(ntStatus))
	{
		free(lpbHashObject);
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	lpbHash = (LPBYTE)calloc(ulHashLength, sizeof(BYTE));
	if (lpbHash == NULL)
	{
		free(lpbHashObject);
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	ntStatus = BCryptCreateHash(hAlgorithm, &hHash, lpbHashObject, dwHashObjectLength, NULL, 0, 0);
	if (!NT_SUCCESS(ntStatus))
	{
		free(lpbHash);
		free(lpbHashObject);
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	ntStatus = BCryptHashData(hHash, (PUCHAR)lpBuffer, dwBufferLength, 0);
	if (!NT_SUCCESS(ntStatus))
	{
		BCryptDestroyHash(hHash);
		free(lpbHash);
		free(lpbHashObject);
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	ntStatus = BCryptFinishHash(hHash, (PUCHAR)lpbHash, ulHashLength, 0);
	if (NT_SUCCESS(ntStatus))
	{
		*lpHash = lpbHash;
		*lpdwHashLength = ulHashLength;
	}
	else
		free(lpbHash);

	BCryptDestroyHash(hHash);
	free(lpbHashObject);
	BCryptCloseAlgorithmProvider(hAlgorithm, 0);

	return NT_SUCCESS(ntStatus);
}

BOOL GetRandomBytes(IN DWORD dwLength, OUT LPBYTE* lpRandomBytes)
{
	LPBYTE lpRandom;
	BCRYPT_ALG_HANDLE hAlgorithm;

	if (!NT_SUCCESS(BCryptOpenAlgorithmProvider(&hAlgorithm, BCRYPT_RNG_ALGORITHM, NULL, 0)))
		return FALSE;

	lpRandom = (LPBYTE)calloc(dwLength, sizeof(BYTE));
	if (lpRandom == NULL)
	{
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		return FALSE;
	}

	if (!NT_SUCCESS(BCryptGenRandom(hAlgorithm, lpRandom, dwLength, 0)))
	{
		BCryptCloseAlgorithmProvider(hAlgorithm, 0);
		free(lpRandom);
		return FALSE;
	}

	*lpRandomBytes = lpRandom;
	BCryptCloseAlgorithmProvider(hAlgorithm, 0);
	return TRUE;
}

BOOL ConvertStringToHex(IN LPWSTR szString, IN DWORD dwSize, OUT LPBYTE lpOutput)
{
	DWORD i, j;
	for (i = 0; i < dwSize; i++)
	{
		if (!swscanf_s(&szString[i * 2], L"%02x", &j))
			return FALSE;
		lpOutput[i] = (BYTE)j;
	}

	return TRUE;
}

BOOL ToUpper(IN LPWSTR szString, IN DWORD dwLength, OUT LPWSTR* lpOutString)
{
	*lpOutString = (LPWSTR)calloc((ULONG64)dwLength + 1, sizeof(WCHAR));

	if (*lpOutString == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	for (DWORD dwChar = 0; dwChar < dwLength; dwChar++)
	{
		(*lpOutString)[dwChar] = towupper(szString[dwChar]);
	}

	return TRUE;
}

BOOL SearchForBytes(
	IN LPBYTE lpBufferToSearchIn,
	IN DWORD dwBufferToSearchInLength,
	IN LPBYTE lpBufferToSearch,
	IN DWORD dwBufferToSearchLength,
	OUT LPBYTE* lpFirstOccurance)
{
	for (
		DWORD dwBytePointer = 0; 
		(dwBytePointer + dwBufferToSearchLength) < dwBufferToSearchInLength; 
		dwBytePointer++
		)
	{
		if (!memcmp(&lpBufferToSearchIn[dwBytePointer], lpBufferToSearch, dwBufferToSearchLength))
		{
			*lpFirstOccurance = &lpBufferToSearchIn[dwBytePointer];
			return TRUE;
		}
	}
	
	return FALSE;
}