#include "Wmiexec_ISystemActivator.h"

VOID SetPropertySize(
	IN ActivationProperiesBlob* lpActivationBlob,
	IN DWORD dwPropertiesCount,
	IN DWORD dwProperyIndex,
	IN DWORD dwPropertySize);
BOOL CreateInterface(
	IN CommonHeader* lpCommonHeader,
	IN DWORD dwInterfaceSize,
	OUT LPDWORD  lpdwBufferSize,
	OUT LPBYTE* lpBuffer);
BOOL CreateIRemoteSCMActivator(
	IN LPWSTR lpTarget,
	IN UUID uuidCasualityID,
	IN WORD MajorVersion,
	IN WORD MinorVersion,
	OUT PREM_CRET_INSTNC_REQ* lpCreateRemoteInstanceBuffer,
	OUT LPDWORD lpdwBufferLength);
BOOL GetIRemoteSCMActivatorResponseData(
	IN LPWSTR lpTargetHost,
	IN ActivationProperiesBlob* lpActivationBlob,
	OUT LPBYTE* lpOXID,
	OUT IID* lpIPID,
	OUT IID* lpIRemUnknownInterfacePointer,
	OUT LPDWORD lpdwAuthenticationHint,
	OUT LPWSTR* lpHighPort);

BOOL CreateIRemoteSCMActivator(
	IN LPWSTR lpTarget,
	IN UUID uuidCasualityID,
	IN WORD MajorVersion,
	IN WORD MinorVersion,
	OUT PREM_CRET_INSTNC_REQ* lpCreateRemoteInstanceBuffer,
	OUT LPDWORD lpdwBufferLength)
{
	WORD wRequestedProtseqsCount;
	DWORD dwTargetNameLength;
	DWORD dwRequestLength;
	DWORD dwPropertiesCount;
	DWORD dwInterfacesCount;
	DWORD dwCurrentFieldLength;
	DWORD dwActivationBlobLength;
	LPBYTE lpCurrentProperty;
	LPBYTE lpTemporaryPointer;
	LPVOID lpOldReallocBuffer;
	OBJREF* lpObjRef;
	CommonHeader globalCommonHeader;
	ActivationProperiesBlob* lpActivationBlob;
	PREM_CRET_INSTNC_REQ lpRemoteCreateInstance;

	const BYTE bIContextDefaultBuffer[] = {
		0x01, 0x00, 0x01, 0x00, 0x63, 0x2c, 0x80, 0x2a,
		0xa5, 0xd2, 0xaf, 0xdd, 0x4d, 0xc4, 0xbb, 0x37,
		0x4d, 0x37, 0x76, 0xd7, 0x02, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00
	};

	dwInterfacesCount = 1;
	dwPropertiesCount = 6;
	wRequestedProtseqsCount = 1;

	globalCommonHeader.Version = 1;
	globalCommonHeader.Endianess = OBJREF_ENDIANESS_LITTLE_ENDIAN;
	globalCommonHeader.Length = sizeof(CommonHeader);
	globalCommonHeader.Filler = 0xCCCCCCCC;

	dwRequestLength = sizeof(REM_CRET_INSTNC_REQ) - sizeof(LPBYTE);
	lpRemoteCreateInstance = (PREM_CRET_INSTNC_REQ)calloc(dwRequestLength, sizeof(BYTE));

	if (lpRemoteCreateInstance == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	lpRemoteCreateInstance->orpcthis.cid = uuidCasualityID;
	lpRemoteCreateInstance->orpcthis.version.MajorVersion = MajorVersion;
	lpRemoteCreateInstance->orpcthis.version.MinorVersion = MinorVersion;
	lpRemoteCreateInstance->orpcthis.Flags = 1;
	lpRemoteCreateInstance->dwReferentID = 0x00020000;

	dwActivationBlobLength = sizeof(ActivationProperiesBlob) +
		(dwPropertiesCount * (sizeof(DWORD) + sizeof(CLSID))) -
		(sizeof(DWORD*) + sizeof(CLSID*) + sizeof(LPBYTE));
	lpActivationBlob = (ActivationProperiesBlob*)calloc(dwActivationBlobLength, sizeof(BYTE));

	if (lpActivationBlob == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	dwCurrentFieldLength = sizeof(CustomHeader) +
		(dwPropertiesCount * (sizeof(DWORD) + sizeof(CLSID))) -
		(sizeof(DWORD*) + sizeof(CLSID*));

	memcpy_s(&lpActivationBlob->customHeader.commonHeader, sizeof(CommonHeader), &globalCommonHeader, sizeof(CommonHeader));
	lpActivationBlob->customHeader.privateHeader.ObjectBufferLength =
		dwCurrentFieldLength - (sizeof(CommonHeader) + sizeof(PrivateHeader));

	lpActivationBlob->customHeader.headerSize = dwCurrentFieldLength;
	lpActivationBlob->customHeader.destCtx = 2;
	lpActivationBlob->customHeader.dwPropertiesCount = dwPropertiesCount;

	lpActivationBlob->customHeader.dwCLSIDsReferentID = 0x00020000;
	lpActivationBlob->customHeader.dwPropSizesReferentID = 0x00020004;

	lpActivationBlob->customHeader.dwCLSIDsCount = dwPropertiesCount;
	lpTemporaryPointer = (LPBYTE)&lpActivationBlob->customHeader.lpPropertiesCLSIDs;

	((CLSID*)lpTemporaryPointer)[0] = CLSID_SpecialSystemProperties;
	((CLSID*)lpTemporaryPointer)[1] = CLSID_InstantiationInfo;
	((CLSID*)lpTemporaryPointer)[2] = CLSID_ActivationContextInfo;
	((CLSID*)lpTemporaryPointer)[3] = CLSID_SecurityInfo;
	((CLSID*)lpTemporaryPointer)[4] = CLSID_ServerLocationInfo;
	((CLSID*)lpTemporaryPointer)[5] = CLSID_ScmRequestInfo;

	lpTemporaryPointer += (dwPropertiesCount * sizeof(CLSID));

	*((DWORD*)lpTemporaryPointer) = dwPropertiesCount;
	lpTemporaryPointer += sizeof(DWORD);

	// SpecialPropertiesData field

	dwCurrentFieldLength = sizeof(SpecialPropertiesData);
	lpCurrentProperty = (LPBYTE)calloc(dwCurrentFieldLength, sizeof(BYTE));

	if (lpCurrentProperty == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	memcpy_s(lpCurrentProperty, sizeof(CommonHeader), &globalCommonHeader, sizeof(CommonHeader));
	((SpecialPropertiesData*)lpCurrentProperty)->privateHeader.ObjectBufferLength = dwCurrentFieldLength -
		(sizeof(CommonHeader) + sizeof(PrivateHeader));

	((SpecialPropertiesData*)lpCurrentProperty)->dwSessionId = 0xFFFFFFFF;
	((SpecialPropertiesData*)lpCurrentProperty)->dwDefaultAuthnLvl = 2;
	((SpecialPropertiesData*)lpCurrentProperty)->dwOrigClsctx = 20;
	((SpecialPropertiesData*)lpCurrentProperty)->dwFlags = SPD_FLAG_USE_CONSOLE_SESSION;

	SetPropertySize(lpActivationBlob, dwPropertiesCount, 0, dwCurrentFieldLength);

	lpOldReallocBuffer = lpActivationBlob;
	lpActivationBlob = (ActivationProperiesBlob*)realloc(lpActivationBlob, (ULONG64)dwActivationBlobLength + dwCurrentFieldLength);

	if (lpActivationBlob == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	memcpy_s(&((LPBYTE)(lpActivationBlob))[dwActivationBlobLength], dwCurrentFieldLength, lpCurrentProperty, dwCurrentFieldLength);
	dwActivationBlobLength += dwCurrentFieldLength;
	free(lpCurrentProperty);

	// InstantinationInfoData field

	if (!CreateInterface(
		&globalCommonHeader,
		sizeof(InstantiationInfoData) - sizeof(IID*) + (dwInterfacesCount * sizeof(IID)),
		&dwCurrentFieldLength,
		&lpCurrentProperty))
	{
		PRINT_ERROR(L"Failed to create InstantiationInfoData interface\n");
		return FALSE;
	}

	((InstantiationInfoData*)lpCurrentProperty)->classId = CLSID_WbemLevel1Login;
	((InstantiationInfoData*)lpCurrentProperty)->classCtx = 20; // ?
	((InstantiationInfoData*)lpCurrentProperty)->cIID = dwInterfacesCount;
	((InstantiationInfoData*)lpCurrentProperty)->dwInterfaceIdsReferentID = 0x00020000;
	((InstantiationInfoData*)lpCurrentProperty)->thisSize = dwCurrentFieldLength;
	((InstantiationInfoData*)lpCurrentProperty)->clientCOMVersion.MajorVersion = MajorVersion;
	((InstantiationInfoData*)lpCurrentProperty)->clientCOMVersion.MinorVersion = MinorVersion;

	lpTemporaryPointer = (LPBYTE)&((InstantiationInfoData*)lpCurrentProperty)->InterfaceIds;

	((InstantiationInfoData*)lpCurrentProperty)->dwInterfaceIdsMaxCount = dwInterfacesCount;
	((IID*)lpTemporaryPointer)[0] = IID_IWbemLevel1Login;

	SetPropertySize(lpActivationBlob, dwPropertiesCount, 1, dwCurrentFieldLength);

	lpOldReallocBuffer = lpActivationBlob;
	lpActivationBlob = (ActivationProperiesBlob*)realloc(lpActivationBlob, (ULONG64)dwActivationBlobLength + dwCurrentFieldLength);

	if (lpActivationBlob == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	memcpy_s(&((LPBYTE)(lpActivationBlob))[dwActivationBlobLength], dwCurrentFieldLength, lpCurrentProperty, dwCurrentFieldLength);
	dwActivationBlobLength += dwCurrentFieldLength;
	free(lpCurrentProperty);

	// ActivationContextInfoData field

	if (!CreateInterface(
		&globalCommonHeader,
		sizeof(ActivationContextInfoData) + sizeof(OBJREF_ICONTEXT) - sizeof(LPBYTE),
		&dwCurrentFieldLength,
		&lpCurrentProperty))
	{
		PRINT_ERROR(L"Failed to create ActivationContextInfoData interface\n");
		return FALSE;
	}

	((ActivationContextInfoData*)lpCurrentProperty)->dwReferentID = 0x00020000;
	((ActivationContextInfoData*)lpCurrentProperty)->ClientContext.ulCntData = sizeof(OBJREF_ICONTEXT);
	((ActivationContextInfoData*)lpCurrentProperty)->ClientContext.dwArraySize = sizeof(OBJREF_ICONTEXT);

	lpTemporaryPointer = (LPBYTE)&((ActivationContextInfoData*)lpCurrentProperty)->ClientContext.abData;

	((OBJREF_ICONTEXT*)lpTemporaryPointer)->Signature = 'WOEM';
	((OBJREF_ICONTEXT*)lpTemporaryPointer)->Flags = OBJREF_FLAG_CUSTOM;
	((OBJREF_ICONTEXT*)lpTemporaryPointer)->iid = IID_IContext;
	((OBJREF_ICONTEXT*)lpTemporaryPointer)->u_objref.clsid = CLSID_ContextMarshaler;
	((OBJREF_ICONTEXT*)lpTemporaryPointer)->u_objref.dwSize = sizeof(bIContextDefaultBuffer);
	memcpy_s(
		((OBJREF_ICONTEXT*)lpTemporaryPointer)->u_objref.bIContextBuffer,
		sizeof(bIContextDefaultBuffer),
		&bIContextDefaultBuffer,
		sizeof(bIContextDefaultBuffer));

	SetPropertySize(lpActivationBlob, dwPropertiesCount, 2, dwCurrentFieldLength);

	lpOldReallocBuffer = lpActivationBlob;
	lpActivationBlob = (ActivationProperiesBlob*)realloc(lpActivationBlob, (ULONG64)dwActivationBlobLength + dwCurrentFieldLength);

	if (lpActivationBlob == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	memcpy_s(&((LPBYTE)(lpActivationBlob))[dwActivationBlobLength], dwCurrentFieldLength, lpCurrentProperty, dwCurrentFieldLength);
	dwActivationBlobLength += dwCurrentFieldLength;
	free(lpCurrentProperty);

	// SecurityInfoData field

	dwTargetNameLength = (lstrlenW(lpTarget) + 1) * sizeof(WCHAR);
	if (!CreateInterface(
		&globalCommonHeader,
		sizeof(SecurityInfoData) + dwTargetNameLength - sizeof(LPBYTE),
		&dwCurrentFieldLength,
		&lpCurrentProperty))
	{
		PRINT_ERROR(L"Failed to create SecurityInfoData interface\n");
		return FALSE;
	}

	((SecurityInfoData*)lpCurrentProperty)->dwReferentID = 0x00020000;
	((SecurityInfoData*)lpCurrentProperty)->ServerInfo.dwNameReferentID = 0x00020004;
	((SecurityInfoData*)lpCurrentProperty)->ServerInfo.dwNameLength = dwTargetNameLength / sizeof(WCHAR);
	((SecurityInfoData*)lpCurrentProperty)->ServerInfo.dwMaxNameLength = dwTargetNameLength / sizeof(WCHAR);

	memcpy_s(&((SecurityInfoData*)lpCurrentProperty)->ServerInfo.lpName, dwTargetNameLength, lpTarget, dwTargetNameLength);

	SetPropertySize(lpActivationBlob, dwPropertiesCount, 3, dwCurrentFieldLength);

	lpOldReallocBuffer = lpActivationBlob;
	lpActivationBlob = (ActivationProperiesBlob*)realloc(lpActivationBlob, (ULONG64)dwActivationBlobLength + dwCurrentFieldLength);

	if (lpActivationBlob == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	memcpy_s(&((LPBYTE)(lpActivationBlob))[dwActivationBlobLength], dwCurrentFieldLength, lpCurrentProperty, dwCurrentFieldLength);
	dwActivationBlobLength += dwCurrentFieldLength;
	free(lpCurrentProperty);

	// LocationInfoData field

	if (!CreateInterface(
		&globalCommonHeader,
		sizeof(LocationInfoData),
		&dwCurrentFieldLength,
		&lpCurrentProperty))
	{
		PRINT_ERROR(L"Failed to create LocationInfoData interface\n");
		return FALSE;
	}

	SetPropertySize(lpActivationBlob, dwPropertiesCount, 4, dwCurrentFieldLength);

	lpOldReallocBuffer = lpActivationBlob;
	lpActivationBlob = (ActivationProperiesBlob*)realloc(lpActivationBlob, (ULONG64)dwActivationBlobLength + dwCurrentFieldLength);

	if (lpActivationBlob == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	memcpy_s(&((LPBYTE)(lpActivationBlob))[dwActivationBlobLength], dwCurrentFieldLength, lpCurrentProperty, dwCurrentFieldLength);
	dwActivationBlobLength += dwCurrentFieldLength;
	free(lpCurrentProperty);

	// ScmRequestInfoData field

	if (!CreateInterface(
		&globalCommonHeader,
		sizeof(ScmRequestInfoData) +
		(wRequestedProtseqsCount * sizeof(USHORT)) - sizeof(USHORT*),
		&dwCurrentFieldLength,
		&lpCurrentProperty))
	{
		PRINT_ERROR(L"Failed to create ScmRequestInfoData interface\n");
		return FALSE;
	}

	((ScmRequestInfoData*)lpCurrentProperty)->dwReferentID = 0x00020000;
	((ScmRequestInfoData*)lpCurrentProperty)->remoteRequest.ClientImpLevel = 2;
	((ScmRequestInfoData*)lpCurrentProperty)->remoteRequest.cRequestedProtseqs = wRequestedProtseqsCount;
	((ScmRequestInfoData*)lpCurrentProperty)->remoteRequest.dwRequestedProtseqsMaxCount = wRequestedProtseqsCount;
	((ScmRequestInfoData*)lpCurrentProperty)->remoteRequest.dwReferentID = 0x00020004;

	lpTemporaryPointer = (LPBYTE)&((ScmRequestInfoData*)lpCurrentProperty)->remoteRequest.pRequestedProtseqs;

	((USHORT*)lpTemporaryPointer)[0] = 7; // ?

	SetPropertySize(lpActivationBlob, dwPropertiesCount, 5, dwCurrentFieldLength);

	lpOldReallocBuffer = lpActivationBlob;
	lpActivationBlob = (ActivationProperiesBlob*)realloc(lpActivationBlob, (ULONG64)dwActivationBlobLength + dwCurrentFieldLength);

	if (lpActivationBlob == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	memcpy_s(&((LPBYTE)(lpActivationBlob))[dwActivationBlobLength], dwCurrentFieldLength, lpCurrentProperty, dwCurrentFieldLength);
	dwActivationBlobLength += dwCurrentFieldLength;
	free(lpCurrentProperty);

	// Wrappers

	lpActivationBlob->dwSize = dwActivationBlobLength - (2 * sizeof(DWORD));
	lpActivationBlob->customHeader.totalSize = dwActivationBlobLength - (2 * sizeof(DWORD));

	dwCurrentFieldLength = sizeof(OBJREF) + dwActivationBlobLength - sizeof(ActivationProperiesBlob);

	lpObjRef = (OBJREF*)calloc(dwCurrentFieldLength, sizeof(BYTE));

	if (lpObjRef == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	lpObjRef->Signature = 'WOEM';
	lpObjRef->Flags = OBJREF_FLAG_CUSTOM;
	lpObjRef->iid = IID_IActivationPropertiesIn;

	lpObjRef->u_objref.clsid = CLSID_ActivationPropertiesIn;
	lpObjRef->u_objref.dwSize = dwActivationBlobLength + (2 * sizeof(DWORD));
	memcpy_s(&lpObjRef->u_objref.lpActivationProperies, dwActivationBlobLength, lpActivationBlob, dwActivationBlobLength);
	free(lpActivationBlob);

	dwRequestLength += dwCurrentFieldLength;
	lpRemoteCreateInstance->pActProperties.ulCntData = dwCurrentFieldLength;
	lpRemoteCreateInstance->pActProperties.dwArraySize = dwCurrentFieldLength;

	lpOldReallocBuffer = lpRemoteCreateInstance;
	lpRemoteCreateInstance = (PREM_CRET_INSTNC_REQ)realloc(lpRemoteCreateInstance, dwRequestLength);

	if (lpRemoteCreateInstance == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	memcpy_s(&lpRemoteCreateInstance->pActProperties.abData, dwCurrentFieldLength, lpObjRef, dwCurrentFieldLength);
	free(lpObjRef);

	*lpCreateRemoteInstanceBuffer = lpRemoteCreateInstance;
	*lpdwBufferLength = dwRequestLength;
	return TRUE;
}

BOOL GetIRemoteSCMActivatorResponseData(
	IN LPWSTR lpTargetHost,
	IN ActivationProperiesBlob* lpActivationBlob,
	OUT LPBYTE* lpOXID,
	OUT IID* lpIPID,
	OUT IID* lpIRemUnknownInterfacePointer,
	OUT LPDWORD lpdwAuthenticationHint,
	OUT LPWSTR* lpHighPort)
{
	DWORD dwTargetHostLength = 0;
	DWORD dwHighPortLength;
	LPBYTE lpSearchPointer;
	LPBYTE lpTargetHostSearch;
	LPVOID lpOldReallocBuffer;
	OBJREF_PROC_OUT* lpProcOutObjref;
	ScmReplyInfoData* lpScmReplyInfo;
	PropertiesOutputData* lpPropertiesOutput;

	lpPropertiesOutput =
		(PropertiesOutputData*)((LPBYTE)&lpActivationBlob->customHeader.lpPropertiesCLSIDs +
		(lpActivationBlob->customHeader.dwCLSIDsCount * (sizeof(DWORD) + sizeof(IID))) +
			sizeof(DWORD));

	lpSearchPointer = (LPBYTE)&lpPropertiesOutput->arriidInterfacesIds;
	lpSearchPointer += lpPropertiesOutput->NumInterfaces * sizeof(IID);
	lpSearchPointer += sizeof(DWORD);
	lpSearchPointer += lpPropertiesOutput->NumInterfaces * sizeof(DWORD);
	lpSearchPointer += 2 * sizeof(DWORD);
	lpProcOutObjref = (OBJREF_PROC_OUT*)&((MInterfacePointer*)lpSearchPointer)[0].abData;

	*lpIPID = lpProcOutObjref->u_objref.std.IPID;

	lpScmReplyInfo = (ScmReplyInfoData*)((LPBYTE)&lpPropertiesOutput->NumInterfaces + lpPropertiesOutput->privateHeader.ObjectBufferLength);

	*lpOXID = (LPBYTE)calloc(8, sizeof(BYTE));

	if (*lpOXID == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	memcpy_s(*lpOXID, 8, lpScmReplyInfo->bOXID, 8);

	*lpIRemUnknownInterfacePointer = lpScmReplyInfo->IRemUnknownInterfacePointerId;
	*lpdwAuthenticationHint = lpScmReplyInfo->dwAuthenticationHint;

	if (!_wcsicmp(lpTargetHost, L"127.0.0.1") ||
		!_wcsicmp(lpTargetHost, L"localhost"))
	{
		lpTargetHostSearch = (LPBYTE)calloc(MAX_COMPUTERNAME_LENGTH + 1, sizeof(WCHAR));
		dwTargetHostLength = (MAX_COMPUTERNAME_LENGTH + 1);

		if (!GetComputerNameW((LPWSTR)lpTargetHostSearch, &dwTargetHostLength))
		{
			PRINT_ERROR_AUTO("GetComputerNameW");
			free(lpTargetHostSearch);
			*lpHighPort = NULL;
			return FALSE;
		}

		dwTargetHostLength *= sizeof(WCHAR);

		lpOldReallocBuffer = lpTargetHostSearch;
		lpTargetHostSearch = (LPBYTE)realloc(lpTargetHostSearch, dwTargetHostLength);

		if (lpTargetHostSearch == NULL)
		{
			PRINT_ERROR_AUTO(L"realloc");
			free(lpOldReallocBuffer);
			return FALSE;
		}
	}
	else
	{
		lpTargetHostSearch = (LPBYTE)lpTargetHost;
		dwTargetHostLength = lstrlenW(lpTargetHost) * sizeof(WCHAR);
	}

	if (!SearchForBytes(
		(LPBYTE)&lpScmReplyInfo->lpUnusedBuffer,
		((lpScmReplyInfo->privateHeader.ObjectBufferLength + sizeof(CommonHeader) + sizeof(PrivateHeader))
			- (sizeof(lpScmReplyInfo) + sizeof(LPBYTE))),
		lpTargetHostSearch,
		dwTargetHostLength,
		&lpSearchPointer))
	{
		PRINT_ERROR(L"Failed to find the target's host in the RemoteCreateInstance response\n");
		*lpHighPort = NULL;
		return FALSE;
	}
	else
	{
		// Skips the "target_host[" string
		lpSearchPointer += dwTargetHostLength + sizeof(WCHAR);
		dwHighPortLength = lstrlenW((LPWSTR)lpSearchPointer);

		// Calculation without the "]" at the end
		*lpHighPort = (LPWSTR)calloc(dwHighPortLength, sizeof(WCHAR));
		memcpy_s(*lpHighPort, (dwHighPortLength - 1) * sizeof(WCHAR), lpSearchPointer, (dwHighPortLength - 1) * sizeof(WCHAR));
		return TRUE;
	}
}

VOID SetPropertySize(
	IN ActivationProperiesBlob* lpActivationBlob,
	IN DWORD dwPropertiesCount,
	IN DWORD dwProperyIndex,
	IN DWORD dwPropertySize)
{
	LPBYTE lpTemporaryPointer;

	lpTemporaryPointer = (LPBYTE)&lpActivationBlob->customHeader.lpPropertiesCLSIDs;
	lpTemporaryPointer += (dwPropertiesCount * sizeof(CLSID));
	lpTemporaryPointer += sizeof(DWORD);

	((DWORD*)lpTemporaryPointer)[dwProperyIndex] = dwPropertySize;
}

BOOL CreateInterface(
	IN CommonHeader* lpCommonHeader,
	IN DWORD dwInterfaceSize,
	OUT LPDWORD  lpdwBufferSize,
	OUT LPBYTE* lpBuffer)
{
	DWORD dwSize;
	LPBYTE lpInterface;

	dwSize = dwInterfaceSize; //dwInterfaceSize + (8 - (dwInterfaceSize % 8));
	lpInterface = (LPBYTE)calloc(dwSize, sizeof(BYTE));

	if (lpInterface == NULL)
	{
		PRINT_ERROR_AUTO("calloc");
		return FALSE;
	}

	memcpy_s(lpInterface, sizeof(CommonHeader), lpCommonHeader, sizeof(CommonHeader));
	*((DWORD*)(lpInterface + sizeof(CommonHeader))) = dwSize - (sizeof(CommonHeader) + sizeof(PrivateHeader));

	*lpBuffer = lpInterface;
	*lpdwBufferSize = dwSize;
	return TRUE;
}

BOOL RemoteCreateInstance(
	IN LPWSTR lpTargetHost,
	IN LPWSTR lpDcerpcPort,
	IN SOCKET sDcerpc,
	IN PSERVER_ALIVE2_RESPONSE lpDcerpcInformation,
	IN OUT LPDWORD lpdwCallID,
	OUT PREMOTE_INSTANCE_CONTEXT* lpRemoteInstance)
{
	WORD wCurrentPacketLength;
	DWORD dwBytesRead;
	DWORD dwCurrentFieldLength;
	CLSID* lpclsCasualityID;
	LPBYTE lpCurrentField;
	LPBYTE lpCurrentPacket;
	LPVOID lpOldReallocBuffer;
	PREMOTE_INSTANCE_CONTEXT lpRemoteInstanceContext;

	if (!GetRandomBytes(sizeof(CLSID), (LPBYTE*)&lpclsCasualityID))
	{
		PRINT_ERROR(L"Failed to generate casuality id\n");
		return FALSE;
	}

	CreateIRemoteSCMActivator(
		(LPWSTR)(((LPBYTE)&lpDcerpcInformation->SecurityOffset) + (2 * sizeof(WORD))),
		*lpclsCasualityID,
		lpDcerpcInformation->VersionMajor,
		lpDcerpcInformation->VersionMinor,
		(PREM_CRET_INSTNC_REQ*)&lpCurrentField,
		&dwCurrentFieldLength);

	wCurrentPacketLength = (WORD)sizeof(DCE_RPC_OPNUM_REQUEST) - sizeof(LPBYTE) - sizeof(LPVOID) - sizeof(PAUTH_INFO) -
		sizeof(UUID) + (WORD)dwCurrentFieldLength;

	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateOpnumRequest(
		wCurrentPacketLength,
		0,
		(*lpdwCallID)++,
		1,
		4, // ISystemActivator::RemoteCreateInstance
		NULL,
		dwCurrentFieldLength,
		0,
		NULL,
		lpCurrentField,
		(PDCE_RPC_OPNUM_REQUEST)lpCurrentPacket);
	free(lpCurrentField);

	lpOldReallocBuffer = lpCurrentPacket;
	lpCurrentPacket = (LPBYTE)realloc(lpCurrentPacket, wCurrentPacketLength + sizeof(NTLMSSP_VERIFIER));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	WriteSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketLength);
	free(lpCurrentPacket);

	wCurrentPacketLength = sizeof(DCE_RPC_OPNUM_RESPONSE) - (sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketLength, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketLength)
	{
		if (dwBytesRead > 0)
		{
			if (*(DWORD*)&lpCurrentField[dwBytesRead - sizeof(DWORD)] == ERROR_ACCESS_DENIED)
			{
				PRINT_ERROR(L"Access denied on machine\n");
				free(lpCurrentField);
				return FALSE;
			}
			else
			{
				PRINT_ERROR(L"Server returned error no. %i\n", *(DWORD*)&lpCurrentField[dwBytesRead - sizeof(DWORD)]);
				free(lpCurrentField);
				return FALSE;
			}
		}
		else
		{
			PRINT_ERROR(L"Read socket error\n");
			free(lpCurrentField);
			return FALSE;
		}
	}

	if ((*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint > 0)
	{
		dwCurrentFieldLength = (WORD)(*(PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket).AllocHint;
		free(lpCurrentPacket);

		lpCurrentField = (LPBYTE)calloc(dwCurrentFieldLength, sizeof(BYTE));

		if (lpCurrentField == NULL)
		{
			PRINT_ERROR_AUTO(L"calloc");
			return FALSE;
		}

		if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentField, dwCurrentFieldLength, &dwBytesRead) ||
			dwBytesRead != dwCurrentFieldLength)
		{
			if (dwBytesRead > 0)
			{
				if (*(DWORD*)&lpCurrentField[dwBytesRead - (2 * sizeof(DWORD))] == ERROR_ACCESS_DENIED)
				{
					PRINT_ERROR(L"Access denied on machine\n");
					free(lpCurrentField);
					return FALSE;
				}
				else
				{
					PRINT_ERROR(L"Server returned error no. %i\n", *(DWORD*)&lpCurrentField[dwBytesRead - (2 * sizeof(DWORD))]);
					free(lpCurrentField);
					return FALSE;
				}
			}
			else
			{
				PRINT_ERROR(L"Read socket error\n");
				free(lpCurrentField);
				return FALSE;
			}
		}

		if (*(DWORD*)&lpCurrentField[dwCurrentFieldLength - (2 * sizeof(DWORD))] == ERROR_ACCESS_DENIED)
		{
			PRINT_ERROR(L"Access denied on machine\n");
			free(lpCurrentField);
			return FALSE;
		}
		else
		{
			lpRemoteInstanceContext = (PREMOTE_INSTANCE_CONTEXT)calloc(1, sizeof(REMOTE_INSTANCE_CONTEXT));

			if (lpRemoteInstanceContext == NULL)
			{
				PRINT_ERROR_AUTO(L"calloc");
				return FALSE;
			}

			if (!GetIRemoteSCMActivatorResponseData(
				lpTargetHost,
				&((OBJREF*)&((PREM_CRET_INSTNC_RESP)lpCurrentField)->pActProperties.abData)->u_objref.lpActivationProperies,
				&lpRemoteInstanceContext->icCurrentInterface.lpOXID,
				&lpRemoteInstanceContext->icCurrentInterface.uuIPID,
				&lpRemoteInstanceContext->uuRemUnknownInterface,
				&lpRemoteInstanceContext->dwAuthenticationHint,
				&lpRemoteInstanceContext->lpHighPort))
			{
				PRINT_ERROR(L"Failed to get RemoteCreateInstance data\n");
				free(lpRemoteInstanceContext);
				free(lpCurrentField);
				return FALSE;
			}

			lpRemoteInstanceContext->clsCasualityID = *lpclsCasualityID;
			lpRemoteInstanceContext->lpTargetHost = lpTargetHost;

			lpRemoteInstanceContext->AuthInfo.arrAuthenticationContext = (PAUTHENTICATION_CONTEXT)calloc(1, sizeof(AUTHENTICATION_CONTEXT));

			if (lpRemoteInstanceContext->AuthInfo.arrAuthenticationContext == NULL)
			{
				PRINT_ERROR_AUTO(L"calloc");
				return FALSE;
			}

			lpRemoteInstanceContext->AuthInfo.arrAuthenticationContext[0].dwSequenceNumber = 0;
			lpRemoteInstanceContext->AuthInfo.arrAuthenticationContext[0].lpSigningKey = NULL;
			lpRemoteInstanceContext->AuthInfo.arrAuthenticationContext[0].wSigningKeyLength = 0;
			lpRemoteInstanceContext->AuthInfo.sContextID = -1;


			*lpRemoteInstance = lpRemoteInstanceContext;
			free(lpCurrentField);
		}
	}
	else
	{
		PRINT_ERROR(L"Server error\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	return TRUE;
}