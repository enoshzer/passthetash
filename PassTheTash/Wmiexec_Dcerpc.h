#pragma once
#include "Wmiexec_Authentication.h"

#define		MAX_XMIT_FRAG			5840
#define		MAX_RECV_FRAG			5840

#pragma pack(1)
typedef struct _SYNTAX {
	union {
		UUID Interface;
		UUID TransferSyntax;
	};
	union {
		DWORD Ver;
		struct {
			WORD InterfaceVer;
			WORD InterfaceVerMinor;
		};
	};
} SYNTAX, *PSYNTAX;

#pragma pack(1)
typedef struct _CONTEXT_ITEM {
	WORD ContextID;
	WORD NumTransItems;
	SYNTAX AbstractSyntax;
	SYNTAX TransferSyntax;
} CONTEXT_ITEM, *PCONTEXT_ITEM;

#pragma pack(1)
typedef struct _CONTEXT_ITEM_ACK {
	WORD Result;
	union {
		WORD BindTimeFeatues;
		WORD Unused;
	};
	SYNTAX TransferSyntax;
} CONTEXT_ITEM_ACK, *PCONTEXT_ITEM_ACK;

#pragma pack(1)
typedef struct _DCE_RPC_PACKET {
	BYTE Version;
	BYTE MinorVerison;
	BYTE PacketType;
	BYTE PacketFlags;
	DWORD DataRepresentation;
	WORD FragLength;
	WORD AuthLength;
	DWORD CallID;
} DCE_RPC_PACKET, *PDCE_RPC_PACKET;

#pragma pack(1)
typedef struct _DCE_RPC_AUTH3_PACKET {
	DCE_RPC_PACKET DceRpcPacket;
	WORD MaxXmitFrag;
	WORD MaxRecvFrag;
	PAUTH_INFO AuthInfo;
} DCE_RPC_AUTH3_PACKET, *PDCE_RPC_AUTH3_PACKET;

#pragma pack(1)
typedef struct _DCE_RPC_FULL_REQUEST {
	DCE_RPC_PACKET DceRpcPacket;
	WORD MaxXmitFrag;
	WORD MaxRecvFrag;
	DWORD AssocGroup;
	DWORD NumCtxItems;
	PCONTEXT_ITEM ContextItems;
	PAUTH_INFO AuthInfo;
} DCE_RPC_FULL_REQUEST, *PDCE_RPC_FULL_REQUEST;

#pragma pack(1)
typedef struct _DCE_RPC_FULL_RESPONSE {
	DCE_RPC_PACKET DceRpcPacket;
	WORD MaxXmitFrag;
	WORD MaxRecvFrag;
	DWORD AssocGroup;
	WORD ScndryAddrLen;
} DCE_RPC_FULL_RESPONSE, *PDCE_RPC_FULL_RESPONSE;

#pragma pack(1) 
typedef struct _DCE_RPC_ADDITIONAL_FULL_DATA {
	LPBYTE ScndryAddr;
	LPBYTE Padding;
	DWORD NumCtxItems;
	PCONTEXT_ITEM_ACK ContextItems;
	PAUTH_INFO AuthInfo;
} DCE_RPC_ADDITIONAL_FULL_DATA, *PDCE_RPC_ADDITIONAL_FULL_DATA;

#pragma pack(1)
typedef struct _DCE_RPC_OPNUM_REQUEST {
	DCE_RPC_PACKET DceRpcPacket;
	DWORD AllocHint;
	WORD ContextID;
	WORD Opnum;
	UUID ObjectUUID;
	LPVOID StubData;
	LPBYTE AuthPadding;
	PAUTH_INFO AuthInfo;
} DCE_RPC_OPNUM_REQUEST, *PDCE_RPC_OPNUM_REQUEST;

#pragma pack(1)
typedef struct _DCE_RPC_OPNUM_RESPONSE {
	DCE_RPC_PACKET DceRpcPacket;
	DWORD AllocHint;
	WORD ContextID;
	BYTE CancelCount;
	BYTE Unknown;
	LPVOID StubData;
	LPBYTE AuthPadding;
	PAUTH_INFO AuthInfo;
} DCE_RPC_OPNUM_RESPONSE, *PDCE_RPC_OPNUM_RESPONSE;

#pragma pack(1)
typedef struct _SERVER_ALIVE2_RESPONSE {
	WORD VersionMajor;
	WORD VersionMinor;
	BYTE Unknown0[8];
	WORD NumEntries;
	WORD SecurityOffset;
} SERVER_ALIVE2_RESPONSE, *PSERVER_ALIVE2_RESPONSE;

#pragma pack(1)
typedef struct _STRING_BINDING {
	WORD wTowerID;
	LPWSTR szNetworkAddr;
} STRING_BINDING, *PSTRING_BINDING;

typedef struct _SECURITY_BINDING {
	WORD AuthnSvc;
	WORD AuthzSvc;
	WORD PrincName;
} SECURITY_BINDING, *PSECURITY_BINDING;

typedef struct _INTEFACE_CONTEXT {
	LPBYTE lpOXID;
	UUID uuIPID;
} INTERFACE_CONTEXT, *PINTEFACE_CONTEXT;

typedef struct _AUTHENTICATION_CONTEXT {
	DWORD dwSequenceNumber;
	LPBYTE lpSigningKey;
	WORD wSigningKeyLength;
	DWORD dwAssocGroup;
} AUTHENTICATION_CONTEXT, *PAUTHENTICATION_CONTEXT;

typedef struct _REMOTE_INSTANCE_CONTEXT {
	WORD wContextID;
	DWORD dwCallID;
	UUID uuRemUnknownInterface;
	CLSID clsCasualityID;
	DWORD dwAuthenticationHint;
	LPWSTR lpTargetHost;
	LPWSTR lpHighPort;
	SOCKET sConnectedSocket;
	INTERFACE_CONTEXT icCurrentInterface;
	struct AuthInfo {
		LPWSTR lpUsername;
		LPWSTR lpDomain;
		LPWSTR lpHash;
		PAUTHENTICATION_CONTEXT arrAuthenticationContext;
		SHORT sContextID;
	} AuthInfo;
} REMOTE_INSTANCE_CONTEXT, *PREMOTE_INSTANCE_CONTEXT;

VOID CreateDceRpcAuth3Request(
	IN WORD FragLength,
	IN WORD AuthLength,
	IN DWORD CallID,
	IN PAUTH_INFO lpAuthInfo,
	OUT PDCE_RPC_AUTH3_PACKET lpDceRpcPacket);
VOID CreateBasicDceRpcPacket(
	IN BYTE Type,
	IN BYTE Flags,
	IN WORD FragLength,
	IN WORD AuthLength,
	IN DWORD CallID,
	OUT PDCE_RPC_PACKET lpDceRpcPacket);
VOID CreateDceRpcRequest(
	IN BYTE PacketType,
	IN WORD FragLength,
	IN WORD AuthLength,
	IN DWORD CallID,
	IN BYTE NumCtxItems,
	IN OPTIONAL PCONTEXT_ITEM lpContextItems,
	IN OPTIONAL PAUTH_INFO lpAuthInfo,
	OUT PDCE_RPC_FULL_REQUEST lpDceRpcBindMessage);
BOOL CreateAlterContextPacket(
	IN DWORD dwCallID,
	IN UUID uuInterface,
	IN WORD wContextID,
	IN WORD wAuthLength,
	IN OPTIONAL PAUTH_INFO lpAuthInfo,
	OUT PDCE_RPC_FULL_REQUEST* lpDceRpcPacket,
	OUT LPWORD lpwDceRpcPacketLength);
VOID CreateOpnumRequest(
	IN WORD FragLength,
	IN WORD AuthLength,
	IN DWORD CallID,
	IN WORD ContextID,
	IN WORD Opnum,
	IN OPTIONAL UUID* lpObjectUUID, 
	IN DWORD dwStubDataLength,
	IN DWORD dwAuthInfoLength,
	IN OPTIONAL PAUTH_INFO lpAuthInfo,
	IN OPTIONAL LPBYTE lpStubData,
	OUT PDCE_RPC_OPNUM_REQUEST lpDceRpcMessage);
BOOL CreateAuthBasedConnection(
	IN LPWSTR lpTargetHost,
	IN LPWSTR lpPort,
	IN LPWSTR lpUsername,
	IN LPWSTR lpDomain,
	IN LPWSTR lpHash,
	IN BYTE bRequestType,
	IN BYTE bContextItemsCount,
	IN OUT PSHORT psAuthContextID,
	IN OPTIONAL PCONTEXT_ITEM lpContextItems,
	IN OPTIONAL BYTE AuthLevel,
	OUT LPBYTE* lpSingingKey,
	OUT PWORD lpwSigningKeyLength,
	IN OUT OPTIONAL SOCKET* lpDcerpcSocket,
	OUT LPDWORD lpdwCallID);