#pragma once
#include "Wmiexec_Dcerpc.h"

#pragma pack(1)
typedef struct _ESTABLISH_POS_REQ {
	ORPCTHIS orpcthis;
	DWORD dwPointerVal;
	DWORD dwReserved;
} ESTABLISH_POS_REQ, *PESTABLISH_POS_REQ;

#pragma pack(1)
typedef struct _ESTABLISH_POS_REPS {
	ORPCTHAT orpcthat;
	DWORD dwSuccess;
	DWORD dwReserved;
} ESTABLISH_POS_RESP, *PESTABLISH_POS_RESP;

#pragma pack(1)
typedef struct _NTLM_LOGIN_REQ {
	ORPCTHIS orpcthis;
	struct _NETWORK_RESOURCE {
		DWORD dwReferentID;
		DWORD dwLength;
		DWORD dwReserved;
		DWORD dwMaxCount;
		LPBYTE lpNetworkResource;
		LPBYTE lpPadding;
	} NetworkResource;
	struct _PREFERRED_LOCALE {
		DWORD dwReferentID;
		DWORD dwLength;
		DWORD dwReserved;
		DWORD dwMaxCount;
		LPBYTE lpPreferredLocale;
		LPBYTE lpPadding;
	} PreferredLocale;
	DWORD dwFlags;
	DWORD dwIWbemContextNull;
} NTLM_LOGIN_REQ, *PNTLM_LOGIN_REQ;

#pragma pack(1)
typedef struct _NTLM_LOGIN_RESP {
	ORPCTHAT orpcthat;
	DWORD dwReferentID;
	MInterfacePointer ppNamespace;
} NTLM_LOGIN_RESP, *PNTLM_LOGIN_RESP;

BOOL EstablishPosition(
	IN SOCKET sDcerpc,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	IN INTERFACE_CONTEXT icIWbemLevel1Login,
	IN DWORD dwCallID,
	IN WORD wContextID);
BOOL NTLMLogin(
	IN SOCKET sDcerpc,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	IN INTERFACE_CONTEXT icIWbemLevel1Login,
	IN DWORD dwCallID,
	IN WORD wContextID,
	OUT INTERFACE_CONTEXT* lpicIWbemServices);