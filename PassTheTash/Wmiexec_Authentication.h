#pragma once
#include "Wmiexec_Common.h"

#define NTLMSSP_REVISION_W2K3 0x0F

#pragma pack(1)
typedef struct _SECUIRTY_STRING_NET {
	WORD Length;
	WORD MaxLength;
	DWORD Offset;
} SECURITY_STRING_NET, *PSECUIRTY_STRING_NET;

#pragma pack(1)
typedef struct _SECURITY_BLOB {
	DWORD Signature;
	DWORD Reserved;
	ULONG64 Timestamp;
	BYTE ClientNonce[8];
	DWORD Unknown0;
	LPBYTE TargetInformationBlock;
	DWORD Unknown1;
	DWORD Padding;
} SECURITY_BLOB, *PSECURITY_BLOB;

#pragma pack(1)
typedef struct _VERSION {
	BYTE MajorVersion;
	BYTE MinorVersion;
	WORD BuildNumber;
	BYTE Reserved[3];
	BYTE CurrentRevision;
} VERSION, *PVERSION;

#pragma pack(1)
typedef struct _AUTH_INFO {
	BYTE Type;
	BYTE Level;
	BYTE PadLen;
	BYTE Rsrvd;
	DWORD ContextID;
	LPVOID SecureServiceProvider;
} AUTH_INFO, *PAUTH_INFO;

#pragma pack(1)
typedef struct _NTLMSSP {
	CHAR Identifier[8];
	DWORD MessageType;
} NTLMSSP, *PNTLMSSP;

#pragma pack(1)
typedef struct _NTLMSSP_AUTH3 {
	NTLMSSP NTLMSSP;
	SECURITY_STRING_NET LanManagerResponse;
	SECURITY_STRING_NET NTLMResponse;
	SECURITY_STRING_NET DomainName;
	SECURITY_STRING_NET UserName;
	SECURITY_STRING_NET Workstation;
	SECURITY_STRING_NET SessionKey;
	DWORD NegotiationFlags;
} NTLMSSP_AUTH3, *PNTLMSSP_AUTH3;

#pragma pack(1)
typedef struct _NTLMSSP_NEGOTIATE {
	NTLMSSP NTLMSSP;
	DWORD NegotiationFlags;
	ULONG64 WorkstationDomain;
	ULONG64 WorkstationName;
	VERSION Version;
} NTLMSSP_NEGOTIATE, *PNTLMSSP_NEGOTIATE;

#pragma pack(1)
typedef struct _NTLMSSP_CHALLENGE {
	NTLMSSP NTLMSSP;
	SECURITY_STRING_NET TargetName;
	DWORD NegotiationFlags;
	BYTE Challenge[8];
	BYTE Reserved[8];
	SECURITY_STRING_NET TargetInfo;
	VERSION Version;
} NTLMSSP_CHALLENGE, *PNTLMSSP_CHALLENGE;

#pragma pack(1)
typedef struct _NTLMSSP_VERIFIER {
	DWORD VersionNumber;
	BYTE Checksum[8];
	DWORD SequenceNumber;
} NTLMSSP_VERIFIER, *PNTLMSSP_VERIFIER;

VOID CreateNTLMSSPNegotiation(
	IN BOOL bSign,
	IN BYTE MajorVersion,
	IN BYTE MinorVersion,
	IN WORD BuildNumber,
	OUT PNTLMSSP_NEGOTIATE lpNTLMSSP);
VOID CreateAuthInfo(
	IN BYTE AuthLevel,
	IN BYTE AuthPadding,
	IN WORD wNTLMSSPLength,
	IN WORD wContextID,
	IN OPTIONAL LPBYTE lpNTLMSSP,
	OUT PAUTH_INFO lpAuthInfo);
VOID CreateNTLMSSPAuth3(
	IN LPBYTE lpNTChallengeResponse,
	IN WORD wNTChallengeResponseLength,
	IN LPBYTE lpDomainName,
	IN WORD wDomainNameLength,
	IN LPBYTE lpUserName,
	IN WORD wUserNameLength,
	IN LPBYTE lpWorkstation,
	IN WORD wWorkstationLength,
	OUT PNTLMSSP_AUTH3 lpNTLMSSPAuth3);
BOOL CreateNTLMSSPChallengeResponse(
	IN LPWSTR lpUsername,
	IN LPWSTR lpDomain,
	IN LPBYTE lpHash,
	IN DWORD dwHashLength,
	IN LPBYTE lpChallenge,
	IN ULONG64 ul64Timestamp,
	IN WORD wTargetInformationLength,
	IN LPVOID lpTargetInformation,
	OUT WORD* lpwResponseLength,
	OUT LPBYTE* lpNTLMResponse,
	OUT OPTIONAL WORD* lpwSessionKeyLength,
	OUT OPTIONAL LPBYTE* lpSessionKey);
BOOL FindAttributeByType(
	IN PNTLMSSP_CHALLENGE lpNtlmChallenge,
	IN DWORD dwAttributeType,
	OUT LPBYTE* lpAttributePointer,
	OUT OPTIONAL PWORD lpwAttributeLength);
BOOL CalculateMessageSignature(
	IN LPBYTE lpSigningKey,
	IN DWORD dwSigningKeyLength,
	IN LPBYTE lpMessage,
	IN DWORD dwMessageLength,
	IN DWORD dwSequenceNumber,
	OUT PNTLMSSP_VERIFIER lpNTLMSSPVerifier);