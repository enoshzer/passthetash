#include "Wmiexec_Authentication.h"

/// <summary>
/// Creates an AuthInfo data field for a dce/rpc packet with a selected NTLMSSP data field
/// </summary>
/// <param name="AuthLevel">The authentication level</param>
/// <param name="wNTLMSSPLength">The length of the NTLMSSP struct</param>
/// <param name="lpNTLMSSP">The NTLMSSP data field</param>
/// <param name="lpAuthInfo">A pointer to the variable that will hold the AuthInfo field's data</param>
VOID CreateAuthInfo(
	IN BYTE AuthLevel,
	IN BYTE AuthPadding,
	IN WORD wNTLMSSPLength,
	IN WORD wContextID,
	IN OPTIONAL LPBYTE lpNTLMSSP,
	OUT PAUTH_INFO lpAuthInfo)
{
	PAUTH_INFO lpTmpAuthInfo;

	if (AuthPadding > 0)
		lpTmpAuthInfo = (PAUTH_INFO)((LPBYTE)lpAuthInfo + AuthPadding);
	else
		lpTmpAuthInfo = lpAuthInfo;

	lpTmpAuthInfo->Type = AUTH_INFO_AUTH_TYPE_NTLM;
	lpTmpAuthInfo->Level = AuthLevel;
	lpTmpAuthInfo->PadLen = AuthPadding;
	lpTmpAuthInfo->Rsrvd = 0;
	lpTmpAuthInfo->ContextID = wContextID;

	if (lpNTLMSSP != NULL && wNTLMSSPLength > 0)
		memcpy_s(&lpTmpAuthInfo->SecureServiceProvider, wNTLMSSPLength, lpNTLMSSP, wNTLMSSPLength);
}

/// <summary>
/// Creates a NTLMSSP Negotiation data field
/// </summary>
/// <param name="lpNTLMSSP">A pointer to the variable that will hold the NTLMSSP field's data</param>
VOID CreateNTLMSSPNegotiation(
	IN BOOL bSign,
	IN BYTE MajorVersion, 
	IN BYTE MinorVersion, 
	IN WORD BuildNumber, 
	OUT PNTLMSSP_NEGOTIATE lpNTLMSSP)
{
	strcpy_s(lpNTLMSSP->NTLMSSP.Identifier, 8, "NTLMSSP");
	lpNTLMSSP->NTLMSSP.MessageType = NTLM_SSP_MESSAGE_TYPE_NEGOTIATE;
	lpNTLMSSP->NegotiationFlags = (
		NTLM_SSP_NEGOTIATE_FLAG_UNICODE |
		NTLM_SSP_NEGOTIATE_FLAG_OEM | 
		NTLM_SSP_NEGOTIATE_FLAG_TARGET |
		NTLM_SSP_NEGOTIATE_FLAG_NTLM_KEY |
		NTLM_SSP_NEGOTIATE_FLAG_ALWAYS_SIGN |
		NTLM_SSP_NEGOTIATE_FLAG_EXTENDED_SECURITY |
		/*NTLM_SSP_NEGOTIATE_FLAG_TARGET_INFO |*/
		NTLM_SSP_NEGOTIATE_FLAG_VERSION |
		NTLM_SSP_NEGOTIATE_FLAG_128 |
		NTLM_SSP_NEGOTIATE_FLAG_56
		);

	if (bSign)
		lpNTLMSSP->NegotiationFlags |= NTLM_SSP_NEGOTIATE_FLAG_SIGN;

	lpNTLMSSP->WorkstationDomain = NULL;
	lpNTLMSSP->WorkstationDomain = NULL;
	lpNTLMSSP->WorkstationName = NULL;
	lpNTLMSSP->Version.MajorVersion = MajorVersion;
	lpNTLMSSP->Version.MinorVersion = MinorVersion;
	lpNTLMSSP->Version.BuildNumber = BuildNumber;
	lpNTLMSSP->Version.CurrentRevision = NTLMSSP_REVISION_W2K3;
}

VOID CreateNTLMSSPAuth3(
	IN LPBYTE lpNTChallengeResponse,
	IN WORD wNTChallengeResponseLength,
	IN LPBYTE lpDomainName,
	IN WORD wDomainNameLength,
	IN LPBYTE lpUserName,
	IN WORD wUserNameLength,
	IN LPBYTE lpWorkstation,
	IN WORD wWorkstationLength,
	OUT PNTLMSSP_AUTH3 lpNTLMSSPAuth3)
{
	DWORD dwOffsetPointer;
	LPBYTE lpMessagePointer;

	dwOffsetPointer = sizeof(NTLMSSP_AUTH3);
	lpMessagePointer = (LPBYTE)lpNTLMSSPAuth3 + dwOffsetPointer;

	strcpy_s(lpNTLMSSPAuth3->NTLMSSP.Identifier, 8, "NTLMSSP");
	lpNTLMSSPAuth3->NTLMSSP.MessageType = NTLM_SSP_MESSAGE_TYPE_AUTH3;

	lpNTLMSSPAuth3->NegotiationFlags = (
		NTLM_SSP_NEGOTIATE_FLAG_UNICODE |
		NTLM_SSP_NEGOTIATE_FLAG_TARGET |
		NTLM_SSP_NEGOTIATE_FLAG_SIGN |
		NTLM_SSP_NEGOTIATE_FLAG_NTLM_KEY |
		NTLM_SSP_NEGOTIATE_FLAG_ALWAYS_SIGN |
		NTLM_SSP_NEGOTIATE_FLAG_EXTENDED_SECURITY |
		NTLM_SSP_NEGOTIATE_FLAG_TARGET_INFO |
		NTLM_SSP_NEGOTIATE_FLAG_VERSION |
		NTLM_SSP_NEGOTIATE_FLAG_128 |
		NTLM_SSP_NEGOTIATE_FLAG_56
		);

	lpNTLMSSPAuth3->DomainName.Length = wDomainNameLength;
	lpNTLMSSPAuth3->DomainName.MaxLength = wDomainNameLength;
	lpNTLMSSPAuth3->DomainName.Offset = dwOffsetPointer;

	if (lpDomainName != NULL && wDomainNameLength > 0)
		memcpy_s(lpMessagePointer, wDomainNameLength, lpDomainName, wDomainNameLength);
	else if (wDomainNameLength > 0)
		ZeroMemory(lpMessagePointer, wDomainNameLength);

	lpMessagePointer += wDomainNameLength;
	dwOffsetPointer += wDomainNameLength;

	lpNTLMSSPAuth3->UserName.Length = wUserNameLength;
	lpNTLMSSPAuth3->UserName.MaxLength = wUserNameLength;
	lpNTLMSSPAuth3->UserName.Offset = dwOffsetPointer;

	if (lpUserName != NULL && wUserNameLength > 0)
		memcpy_s(lpMessagePointer, wUserNameLength, lpUserName, wUserNameLength);
	else if (wUserNameLength > 0)
		ZeroMemory(lpMessagePointer, wUserNameLength);

	lpMessagePointer += wUserNameLength;
	dwOffsetPointer += wUserNameLength;

	lpNTLMSSPAuth3->Workstation.Length = wWorkstationLength;
	lpNTLMSSPAuth3->Workstation.MaxLength = wWorkstationLength;
	lpNTLMSSPAuth3->Workstation.Offset = dwOffsetPointer;

	if (lpWorkstation != NULL && wWorkstationLength > 0)
		memcpy_s(lpMessagePointer, wWorkstationLength, lpWorkstation, wWorkstationLength);
	else if (wWorkstationLength > 0)
		ZeroMemory(lpMessagePointer, wWorkstationLength);

	lpMessagePointer += wWorkstationLength;
	dwOffsetPointer += wWorkstationLength;

	lpNTLMSSPAuth3->LanManagerResponse.Length = NTLM_SSP_MESSAGE_LM_RESPONSE_LENGTH;
	lpNTLMSSPAuth3->LanManagerResponse.MaxLength = NTLM_SSP_MESSAGE_LM_RESPONSE_LENGTH;
	lpNTLMSSPAuth3->LanManagerResponse.Offset = dwOffsetPointer;

	ZeroMemory(lpMessagePointer, NTLM_SSP_MESSAGE_LM_RESPONSE_LENGTH);

	lpMessagePointer += NTLM_SSP_MESSAGE_LM_RESPONSE_LENGTH;
	dwOffsetPointer += NTLM_SSP_MESSAGE_LM_RESPONSE_LENGTH;

	lpNTLMSSPAuth3->NTLMResponse.Length = wNTChallengeResponseLength;
	lpNTLMSSPAuth3->NTLMResponse.MaxLength = wNTChallengeResponseLength;
	lpNTLMSSPAuth3->NTLMResponse.Offset = dwOffsetPointer;

	if (lpNTChallengeResponse != NULL && wNTChallengeResponseLength > 0)
		memcpy_s(lpMessagePointer, wNTChallengeResponseLength, lpNTChallengeResponse, wNTChallengeResponseLength);
	else if (wNTChallengeResponseLength > 0)
		ZeroMemory(lpMessagePointer, wNTChallengeResponseLength);

	lpMessagePointer += wNTChallengeResponseLength;
	dwOffsetPointer += wNTChallengeResponseLength;

	lpNTLMSSPAuth3->SessionKey.Length = 0;
	lpNTLMSSPAuth3->SessionKey.MaxLength = 0;
	lpNTLMSSPAuth3->SessionKey.Offset = dwOffsetPointer;
}

/// <summary>
/// Creates a NTLMSSP data field of the dce/rpc packet as part of the AuthInfo data field for the AUTH3 message
/// </summary>
/// <param name="lpUsername">The username of the authenticated user</param>
/// <param name="lpTarget">The target domain name</param>
/// <param name="lpHash">The NTLM hash of the user</param>
/// <param name="dwHashLength">The length of the NTLM hash</param>
/// <param name="lpChallenge">The challenge that was received from the server</param>
/// <param name="ul64Timestamp">The current timestamp of the system</param>
/// <param name="dwTargetInformationLength">The length of the TargetInformation parameter</param>
/// <param name="lpTargetInformation">The TargetInformation data</param>
/// <param name="lpwResponseLength">A pointer to a number that will hold the length of the produced response message</param>
/// <param name="lpNTLMResponse">A pointer to a variable that will hold the NTLMSSP field's data</param>
/// <param name="lpwSessionKeyLength">A pointer to a number that will hold the length of the produced SessionKey</param>
/// <param name="lpSessionKey">A pointer to a variable that will hold the generated SessionKey</param>
/// <result>Whether the calculation to the response message has succeeded</result>
BOOL CreateNTLMSSPChallengeResponse(
	IN LPWSTR lpUsername,
	IN LPWSTR lpDomain,
	IN LPBYTE lpHash,
	IN DWORD dwHashLength,
	IN LPBYTE lpChallenge,
	IN ULONG64 ul64Timestamp,
	IN WORD wTargetInformationLength,
	IN LPVOID lpTargetInformation,
	OUT WORD* lpwResponseLength,
	OUT LPBYTE* lpNTLMResponse,
	OUT OPTIONAL WORD* lpwSessionKeyLength,
	OUT OPTIONAL LPBYTE* lpSessionKey)
{
	WORD wHmacLength;
	WORD wSecurityBlobLength;
	WORD wMessageBufferLength;
	WORD wSecurityBlobHmacLength;
	DWORD dwTargetLength;
	DWORD dwUsernameLength;
	DWORD dwUsernameTargetLength;
	LPWSTR lpUpperUsername;
	LPWSTR lpUsernameTarget;
	LPBYTE lpHmacHash;
	LPBYTE lpSecurityBlobHmac;
	LPBYTE lpMessageBuffer;
	LPBYTE lpClientNonce;
	PSECURITY_BLOB lpSecurityBlob;

	// Allocating the buffer for the UsernameTarget data
	dwTargetLength = lstrlenW(lpDomain);
	dwUsernameLength = lstrlenW(lpUsername);
	dwUsernameTargetLength = dwUsernameLength + dwTargetLength;

	if (!ToUpper(lpUsername, dwUsernameLength, &lpUpperUsername))
	{
		PRINT_ERROR(L"Failed to convert string to uppercase\n");
		return FALSE;
	}

	lpUsernameTarget = (LPWSTR)calloc(dwUsernameTargetLength, sizeof(WCHAR));

	if (lpUsernameTarget == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	// Copying the username and the target from their buffers to the new UsernameTarget buffer
	memcpy_s(lpUsernameTarget, dwUsernameLength * sizeof(WCHAR), lpUpperUsername, dwUsernameLength * sizeof(WCHAR));
	memcpy_s(&lpUsernameTarget[dwUsernameLength], dwTargetLength * sizeof(WCHAR), lpDomain, dwTargetLength * sizeof(WCHAR));
	free(lpUpperUsername);

	// Producing the NTLMv2 hash from the NTLM hash as the key and the UsernameTarget as the message with a MD5-HMAC
	if (!HmacMD5(
		(LPSTR)lpUsernameTarget,
		dwUsernameTargetLength * sizeof(WCHAR),
		(LPSTR)lpHash,
		dwHashLength,
		&lpHmacHash,
		&wHmacLength))
	{
		PRINT_ERROR(L"Failed to calculate md5-hmac\n");
		return FALSE;
	}

	// Allocating the security blob
	wSecurityBlobLength = sizeof(SECURITY_BLOB) - sizeof(LPBYTE) + wTargetInformationLength;
	lpSecurityBlob = (PSECURITY_BLOB)calloc(wSecurityBlobLength, sizeof(BYTE));

	if (lpSecurityBlob == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	lpSecurityBlob->Signature = 0x0101;
	lpSecurityBlob->Timestamp = ul64Timestamp;

	if (!GetRandomBytes(8, &lpClientNonce))
	{
		PRINT_ERROR(L"Failed to generate client nonce\n");
		free(lpSecurityBlob);
		return FALSE;
	}

	memcpy_s(lpSecurityBlob->ClientNonce, 8, lpClientNonce, 8);
 	free(lpClientNonce);

	// Copying the TargetInformation to the security blob
	if (lpTargetInformation != NULL && wTargetInformationLength > 0)
		memcpy_s(&lpSecurityBlob->TargetInformationBlock, wTargetInformationLength, lpTargetInformation, wTargetInformationLength);

	// Allocating a new buffer and copying the NTLMv2 and the security blob to feed as a message to a new MD5-HMAC value
	wMessageBufferLength = wSecurityBlobLength + (8 * sizeof(BYTE));
	lpMessageBuffer = (LPBYTE)calloc(wMessageBufferLength, sizeof(BYTE));

	if (lpMessageBuffer == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	memcpy_s(lpMessageBuffer, 8 * sizeof(BYTE), lpChallenge, 8 * sizeof(BYTE));
	memcpy_s(&lpMessageBuffer[8], wSecurityBlobLength, lpSecurityBlob, wSecurityBlobLength);

	// Computes the new MD5-HMAC with a key as the NTLMv2 and the message as the previous buffer to send an integrity check to the server
	//	 without sending the NTLMv2 as a cleartext
	if (!HmacMD5(
		(LPSTR)lpMessageBuffer,
		wMessageBufferLength,
		(LPSTR)lpHmacHash,
		wHmacLength,
		&lpSecurityBlobHmac,
		&wSecurityBlobHmacLength))
	{
		PRINT_ERROR(L"Failed to calculate md5-hmac\n");
		return FALSE;
	}

	free(lpMessageBuffer);

	// Allocating the resulting buffer and copying its data - the resulting MD5-HMAC + the security blob
	wMessageBufferLength = wHmacLength + wSecurityBlobLength;
	lpMessageBuffer = (LPBYTE)calloc(wMessageBufferLength, sizeof(BYTE));

	if (lpMessageBuffer == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	memcpy_s(lpMessageBuffer, wSecurityBlobHmacLength, lpSecurityBlobHmac, wSecurityBlobHmacLength);
	memcpy_s(&lpMessageBuffer[wSecurityBlobHmacLength], wSecurityBlobLength, lpSecurityBlob, wSecurityBlobLength);
	free(lpSecurityBlob);

	*lpwResponseLength = wMessageBufferLength;
	*lpNTLMResponse = lpMessageBuffer;

	// If a pointer to the SessionKey and the SessionKey length are supplied, the SessionKey is computed
	if (lpwSessionKeyLength != NULL &&
		lpSessionKey != NULL)
		if (!HmacMD5(
			(LPSTR)lpSecurityBlobHmac,
			wSecurityBlobHmacLength,
			(LPSTR)lpHmacHash,
			wHmacLength,
			lpSessionKey,
			lpwSessionKeyLength))
			return FALSE;

	return TRUE;
}

/// <summary>
/// Finds a timestamp value from the TargetInfo field of the NTLMSSP challenge message
/// </summary>
/// <param name="lpNtlmChallenge">The NTLMSSP challenge message</param>
/// <param name="lpul64Timestamp">A pointer to a 8-byte number that will hold the resulting timestamp</param>
BOOL FindAttributeByType(
	IN PNTLMSSP_CHALLENGE lpNtlmChallenge,
	IN DWORD dwAttributeType,
	OUT LPBYTE* lpAttributePointer,
	OUT OPTIONAL PWORD lpwAttributeLength)
{
	WORD wSkipBytes;
	LPBYTE lpDataPointer;

	for (wSkipBytes = 0, lpDataPointer = (LPBYTE)lpNtlmChallenge + lpNtlmChallenge->TargetInfo.Offset;
		lpDataPointer < ((LPBYTE)lpNtlmChallenge + lpNtlmChallenge->TargetInfo.Offset + lpNtlmChallenge->TargetInfo.Length);
		lpDataPointer += wSkipBytes)
	{
		if (*(WORD*)lpDataPointer == dwAttributeType)
		{
			lpDataPointer += 2;
			if (lpwAttributeLength != NULL)
				*lpwAttributeLength = *(WORD*)lpDataPointer;
			lpDataPointer += 2;
			*lpAttributePointer = lpDataPointer;
			return TRUE;
		}
		else
		{
			lpDataPointer += 2;
			wSkipBytes = *(WORD*)lpDataPointer;
			lpDataPointer += 2;
		}
	}

	if (lpwAttributeLength != NULL)
		*lpwAttributeLength = 0;
	*lpAttributePointer = NULL;
	return FALSE;
}

BOOL CalculateMessageSignature(
	IN LPBYTE lpSigningKey,
	IN DWORD dwSigningKeyLength,
	IN LPBYTE lpMessage,
	IN DWORD dwMessageLength,
	IN DWORD dwSequenceNumber,
	OUT PNTLMSSP_VERIFIER lpNTLMSSPVerifier)
{
	WORD wMD5HmacHashLength;
	DWORD dwMD5SigningKeyLength;
	DWORD dwServerConstantLength;
	DWORD dwSigningReadyMessgeLength;
	DWORD dwSingingKeyServerConstantLength;
	LPBYTE lpMD5HmacHash;
	LPBYTE lpSigningReadyMessage;
	LPBYTE lpMD5SigningKey;
	LPBYTE lpSigningKeyServerConstant;
	LPCSTR bServerSigningConstant = "session key to client-to-server signing key magic constant";

	dwServerConstantLength = (DWORD)strlen(bServerSigningConstant) + 1;
	dwSingingKeyServerConstantLength = dwServerConstantLength + dwSigningKeyLength;
	lpSigningKeyServerConstant = (LPBYTE)calloc(dwSingingKeyServerConstantLength, sizeof(BYTE));

	if (lpSigningKeyServerConstant == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}


	memcpy_s(lpSigningKeyServerConstant, dwSigningKeyLength, lpSigningKey, dwSigningKeyLength);
	memcpy_s(&lpSigningKeyServerConstant[dwSigningKeyLength], dwServerConstantLength, bServerSigningConstant, dwServerConstantLength);

	if (!MD5Hash(lpSigningKeyServerConstant, dwSingingKeyServerConstantLength, &lpMD5SigningKey, &dwMD5SigningKeyLength))
	{
		PRINT_ERROR(L"Failed to generate signing key MD5\n");
		return FALSE;
	}

	dwSigningReadyMessgeLength = dwMessageLength + sizeof(DWORD);
	lpSigningReadyMessage = (LPBYTE)calloc(dwSigningReadyMessgeLength, sizeof(BYTE));

	if (lpSigningReadyMessage == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	*(DWORD*)lpSigningReadyMessage = dwSequenceNumber;
	memcpy_s(&lpSigningReadyMessage[sizeof(DWORD)], dwMessageLength, lpMessage, dwMessageLength);

	if (!HmacMD5((LPSTR)lpSigningReadyMessage, dwSigningReadyMessgeLength, (LPSTR)lpMD5SigningKey, dwMD5SigningKeyLength, &lpMD5HmacHash, &wMD5HmacHashLength))
	{
		PRINT_ERROR(L"Failed to generate md5-hmac for message signing\n");
		return FALSE;
	}

	free(lpSigningReadyMessage);

	lpNTLMSSPVerifier->VersionNumber = 1;
	lpNTLMSSPVerifier->SequenceNumber = dwSequenceNumber;

	memcpy_s(lpNTLMSSPVerifier->Checksum, 8 * sizeof(BYTE), lpMD5HmacHash, 8 * sizeof(BYTE));

	return TRUE;
}