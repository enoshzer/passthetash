#pragma once
#include "Mimikatz_Common.h"

typedef struct _PEB_LDR_DATA {
	ULONG Length;
	BOOLEAN Initialized;
	PVOID SsHandle;
	LIST_ENTRY InLoadOrderModulevector;
	LIST_ENTRY InMemoryOrderModulevector;
	LIST_ENTRY InInitializationOrderModulevector;
} PEB_LDR_DATA, *PPEB_LDR_DATA;

typedef struct _PEB {
	BOOLEAN InheritedAddressSpace;
	BOOLEAN ReadImageFileExecOptions;
	BOOLEAN BeingDebugged;
	struct BitField {
		BYTE ImageUsesLargePages : 1;
		BYTE SpareBits : 7;
	};
	HANDLE Mutant;
	PVOID ImageBaseAddress;
	PPEB_LDR_DATA Ldr;
	/// ...
} PEB, *PPEB;

typedef struct _LDR_DATA_TABLE_ENTRY
{
	LIST_ENTRY InLoadOrderLinks;
	LIST_ENTRY InMemoryOrderLinks;
	LIST_ENTRY InInitializationOrderLinks;
	PVOID DllBase;
	PVOID EntryPoint;
	ULONG SizeOfImage;
	LSA_UNICODE_STRING FullDllName;
	LSA_UNICODE_STRING BaseDllName;
	/// ...
} LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;

#if defined(_M_X64) || defined(_M_ARM64) // TODO:ARM64
typedef struct _LSA_UNICODE_STRING_F32 {
	USHORT Length;
	USHORT MaximumLength;
	DWORD  Buffer;
} LSA_UNICODE_STRING_F32, *PLSA_UNICODE_STRING_F32;

typedef LSA_UNICODE_STRING_F32 UNICODE_STRING_F32, *PUNICODE_STRING_F32;

typedef struct _LDR_DATA_TABLE_ENTRY_F32
{
	LIST_ENTRY32 InLoadOrderLinks;
	LIST_ENTRY32 InMemoryOrderLinks;
	LIST_ENTRY32 InInitializationOrderLinks;
	DWORD DllBase;
	DWORD EntryPoint;
	DWORD SizeOfImage;
	UNICODE_STRING_F32 FullDllName;
	UNICODE_STRING_F32 BaseDllName;
	/// ...
} LDR_DATA_TABLE_ENTRY_F32, *PLDR_DATA_TABLE_ENTRY_F32;

typedef struct _PEB_LDR_DATA_F32 {
	ULONG Length;
	BOOLEAN Initialized;
	DWORD SsHandle;
	LIST_ENTRY32 InLoadOrderModulevector;
	LIST_ENTRY32 InMemoryOrderModulevector;
	LIST_ENTRY32 InInitializationOrderModulevector;
} PEB_LDR_DATA_F32, *PPEB_LDR_DATA_F32;

typedef struct _PEB_F32 {
	BOOLEAN InheritedAddressSpace;
	BOOLEAN ReadImageFileExecOptions;
	BOOLEAN BeingDebugged;
	struct BitField_F32 {
		BYTE ImageUsesLargePages : 1;
		BYTE SpareBits : 7;
	};
	DWORD Mutant;
	DWORD ImageBaseAddress;
	DWORD Ldr;
	/// ...
} PEB_F32, *PPEB_F32;
#endif

typedef struct _PROCESS_BASIC_INFORMATION {
	DWORD_PTR ExitStatus;
	PPEB PebBaseAddress;
	DWORD_PTR AffinityMask;
	DWORD_PTR BasePriority;
	ULONG_PTR UniqueProcessIds;
	DWORD_PTR InheritedFromUniqueProcessId;
} PROCESS_BASIC_INFORMATION, *PPROCESS_BASIC_INFORMATION;

typedef enum _PROCESSINFOCLASS {
	ProcessBasicInformation,
	ProcessQuotaLimits,
	ProcessIoCounters,
	ProcessVmCounters,
	ProcessTimes,
	ProcessBasePriority,
	ProcessRaisePriority,
	ProcessDebugPort,
	ProcessExceptionPort,
	ProcessAccessToken,
	ProcessLdtInformation,
	ProcessLdtSize,
	ProcessDefaultHardErrorMode,
	ProcessIoPortHandlers,		  // Note: this is kernel mode only
	ProcessPooledUsageAndLimits,
	ProcessWorkingSetWatch,
	ProcessUserModeIOPL,
	ProcessEnableAlignmentFaultFixup,
	ProcessPriorityClass,
	ProcessWx86Information,
	ProcessHandleCount,
	ProcessAffinityMask,
	ProcessPriorityBoost,
	ProcessDeviceMap,
	ProcessSessionInformation,
	ProcessForegroundInformation,
	ProcessWow64Information,
	ProcessImageFileName,
	ProcessLUIDDeviceMapsEnabled,
	ProcessBreakOnTermination,
	ProcessDebugObjectHandle,
	ProcessDebugFlags,
	ProcessHandleTracing,
	ProcessIoPriority,
	ProcessExecuteFlags,
	ProcessTlsInformation,
	ProcessCookie,
	ProcessImageInformation,
	ProcessCycleTime,
	ProcessPagePriority,
	ProcessInstrumentationCallback,
	ProcessThreadStackAllocation,
	ProcessWorkingSetWatchEx,
	ProcessImageFileNameWin32,
	ProcessImageFileMapping,
	ProcessAffinityUpdateMode,
	ProcessMemoryAllocationMode,
	ProcessGroupInformation,
	ProcessTokenVirtualizationEnabled,
	ProcessConsoleHostProcess,
	ProcessWindowInformation,
	MaxProcessInfoClass			 // MaxProcessInfoClass should always be the last enum
} PROCESSINFOCLASS;

typedef struct _KULL_M_PATCH_PATTERN {
	DWORD Length;
	BYTE *Pattern;
} KULL_M_PATCH_PATTERN, *PKULL_M_PATCH_PATTERN;

typedef struct _KULL_M_PATCH_OFFSETS {
	LONG off0;
#if defined(_M_ARM64)
	LONG armOff0;
#endif
	LONG off1;
#if defined(_M_ARM64)
	LONG armOff1;
#endif
	LONG off2;
#if defined(_M_ARM64)
	LONG armOff2;
#endif
	LONG off3;
#if defined(_M_ARM64)
	LONG armOff3;
#endif
	LONG off4;
#if defined(_M_ARM64)
	LONG armOff4;
#endif
	LONG off5;
#if defined(_M_ARM64)
	LONG armOff5;
#endif
	LONG off6;
#if defined(_M_ARM64)
	LONG armOff6;
#endif
	LONG off7;
#if defined(_M_ARM64)
	LONG armOff7;
#endif
	LONG off8;
#if defined(_M_ARM64)
	LONG armOff8;
#endif
	LONG off9;
#if defined(_M_ARM64)
	LONG armOff9;
#endif
} KULL_M_PATCH_OFFSETS, *PKULL_M_PATCH_OFFSETS;

typedef struct _KULL_M_PATCH_GENERIC {
	DWORD MinBuildNumber;
	KULL_M_PATCH_PATTERN Search;
	KULL_M_PATCH_PATTERN Patch;
	KULL_M_PATCH_OFFSETS Offsets;
} KULL_M_PATCH_GENERIC, *PKULL_M_PATCH_GENERIC;

typedef struct _MEMORY_RANGE {
	MEMORY_ADDRESS MemoryAddress;
	SIZE_T size;
} MEMORY_RANGE, *PMEMORY_RANGE;

typedef struct _MEMORY_SEARCH {
	MEMORY_RANGE MemoryRange;
	LPVOID lpResult;
} MEMORY_SEARCH, *PMEMORY_SEARCH;

typedef struct _MODULE_INFORMATION_BY_NAME {
	PLSA_UNICODE_STRING lpName;
	PMODULE_INFORMATION lpModuleInformation;
	BOOL bIsFound;
} MODULE_INFORMATION_BY_NAME, *PMODULE_INFORMATION_BY_NAME;

typedef PPEB(NTAPI* RtlGetCurrentPeb)();
typedef BOOL(CALLBACK *PMODULE_INFO_CALLBACK)(IN PMODULE_INFORMATION pModuleInformation, IN OPTIONAL LPVOID lpArgs);
typedef VOID(NTAPI* RtlInitUincodeString)(OUT PLSA_UNICODE_STRING DestinationString, IN PCWSTR SourceString);
typedef BOOLEAN(NTAPI* RtlEqualUnicodeString)(IN PLSA_UNICODE_STRING String1, IN PLSA_UNICODE_STRING String2, IN BOOLEAN CaseInsensitive);
typedef NTSTATUS(NTAPI *NtQueryInformationProcess)(IN HANDLE ProcessHandle,
	IN PROCESSINFOCLASS ProcessInformationClass,
	OUT PVOID ProcessInformation,
	IN ULONG ProcessInformationLength,
	OUT PULONG ReturnLength);

VOID AdjustTimeDateStamp(PMODULE_INFORMATION miModule);
BOOL GetProcessPidFromName(IN LPCWSTR szName, OUT PDWORD dwProcId);
BOOL GetProcessPeb(IN HANDLE hProcess, OUT PPEB pPeb, IN BOOL isWOW);
BOOL ReadMemory(IN HANDLE hProcess, LPVOID lpSourcePointer, LPVOID lpDestination, SIZE_T nSize);
BOOL GetProcessNtHeaders(IN HANDLE hProcess, IN LPVOID lpAddress, OUT PIMAGE_NT_HEADERS *pHeaders);
BOOL SearchProcessMemory(IN LPVOID lpPatternAddress, IN SIZE_T cbLength, IN PMEMORY_SEARCH lpSearch);
BOOL GetBasicModuleInformationByName(IN HANDLE hModule, IN PCWSTR szName, OUT PMODULE_INFORMATION lpModuleInformation);
NTSTATUS GetBasicModuleInformation(IN HANDLE hModule, OUT PMODULE_INFO_CALLBACK pfnCallback, IN OPTIONAL LPVOID lpArgs);
PKULL_M_PATCH_GENERIC GetHighestCompatibleReference(PKULL_M_PATCH_GENERIC lpGenerics, SIZE_T cbGenericsCount, DWORD dwBuildNumber);
BOOL GetSessionsList(HANDLE hLsassHandle, PMODULE_INFORMATION_PACKAGE lpAuthPackageInfo, PLIST_ENTRY *pLogonSessionList, PULONG *pLogonSessionListCount);