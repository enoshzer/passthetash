#pragma once
#include "Globals.h"

typedef struct _REMOT_THRD_PARAMS {
	HANDLE hWritePipe;
	HANDLE hReadPipe;
} REMOT_THRD_PARAMS, *PREMOT_THRD_PARAMS;

HANDLE CreateShellRemoteThread(DWORD dwProcessId, HANDLE hWriteHandle, HANDLE hReadHandle);