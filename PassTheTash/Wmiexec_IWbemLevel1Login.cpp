#include "Wmiexec_IWbemLevel1Login.h"

BOOL EstablishPosition(
	IN SOCKET sDcerpc,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	IN INTERFACE_CONTEXT icIWbemLevel1Login,
	IN DWORD dwCallID,
	IN WORD wContextID)
{
	BYTE bPaddingLength = 0;
	WORD wAuthInfoLength;
	WORD wStubDataLength;
	WORD wCurrentPacketLength;
	DWORD dwBytesRead;
	LPBYTE lpCurrentPacket;
	LPBYTE lpRequestStubData;
	LPVOID lpOldReallocBuffer;
	PAUTH_INFO lpAuthInfo;

	wStubDataLength = sizeof(ESTABLISH_POS_REQ);
	lpRequestStubData = (LPBYTE)calloc(wStubDataLength, sizeof(BYTE));

	if (lpRequestStubData == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	((PESTABLISH_POS_REQ)lpRequestStubData)->orpcthis.version.MajorVersion = 5;
	((PESTABLISH_POS_REQ)lpRequestStubData)->orpcthis.version.MinorVersion = 7;
	((PESTABLISH_POS_REQ)lpRequestStubData)->orpcthis.cid = lpRemoteInstance->clsCasualityID;

	if (wStubDataLength % 16 != 0)
		bPaddingLength = (16 - (wStubDataLength % 16));
	wAuthInfoLength = sizeof(AUTH_INFO) - sizeof(LPVOID) + bPaddingLength;
	lpAuthInfo = (PAUTH_INFO)calloc(wAuthInfoLength, sizeof(BYTE));

	if (lpAuthInfo == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateAuthInfo(AUTH_INFO_AUTH_LEVEL_PKT, bPaddingLength, 0, lpRemoteInstance->AuthInfo.sContextID, NULL, lpAuthInfo);

	wCurrentPacketLength = sizeof(DCE_RPC_OPNUM_REQUEST);
	wCurrentPacketLength += wAuthInfoLength + wStubDataLength;
	wCurrentPacketLength -= (sizeof(PAUTH_INFO) + sizeof(LPBYTE) + sizeof(LPVOID));
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateOpnumRequest(
		wCurrentPacketLength + sizeof(NTLMSSP_VERIFIER),
		sizeof(NTLMSSP_VERIFIER),
		dwCallID,
		wContextID,
		3, // IWbemLevel1Login::EstablishPosition
		&icIWbemLevel1Login.uuIPID,
		wStubDataLength,
		wAuthInfoLength,
		lpAuthInfo,
		lpRequestStubData,
		(PDCE_RPC_OPNUM_REQUEST)lpCurrentPacket);
	free(lpRequestStubData);
	free(lpAuthInfo);

	lpOldReallocBuffer = lpCurrentPacket;
	lpCurrentPacket = (LPBYTE)realloc(lpCurrentPacket, wCurrentPacketLength + sizeof(NTLMSSP_VERIFIER));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	if (!CalculateMessageSignature(
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].lpSigningKey,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].wSigningKeyLength,
		lpCurrentPacket,
		wCurrentPacketLength,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].dwSequenceNumber++,
		(PNTLMSSP_VERIFIER)&lpCurrentPacket[wCurrentPacketLength]))
	{
		PRINT_ERROR(L"Failed to calculate NTLMSSP verifier\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketLength += sizeof(NTLMSSP_VERIFIER);

	WriteSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketLength);
	free(lpCurrentPacket);

	wCurrentPacketLength = sizeof(DCE_RPC_OPNUM_RESPONSE) - (sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketLength);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketLength, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketLength)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketLength = (WORD)(((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint +
		((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->DceRpcPacket.AuthLength +
		sizeof(AUTH_INFO) - sizeof(LPVOID));
	if ((((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint > 0) &&
		((((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint % 16) != 0))
		wCurrentPacketLength += (16 - (((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint % 16));
	free(lpCurrentPacket);

	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketLength, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketLength)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	if (((PESTABLISH_POS_RESP)lpCurrentPacket)->dwSuccess)
	{
		free(lpCurrentPacket);
		return TRUE;
	}
	else
	{
		free(lpCurrentPacket);
		return FALSE;
	}
}

BOOL NTLMLogin(
	IN SOCKET sDcerpc,
	IN PREMOTE_INSTANCE_CONTEXT lpRemoteInstance,
	IN INTERFACE_CONTEXT icIWbemLevel1Login,
	IN DWORD dwCallID,
	IN WORD wContextID,
	OUT INTERFACE_CONTEXT* lpicIWbemServices)
{
	BYTE bPaddingLength = 0;
	WORD wAuthInfoLength;
	WORD wStubDataLength;
	WORD wCurrentPacketLength;
	DWORD dwBytesRead;
	DWORD dwPreferredLocaleLength;
	DWORD dwFixedNetworkResourceLength;
	LPVOID lpOldReallocBuffer;
	LPBYTE lpCurrentPacket;
	LPBYTE lpRequestStubData;
	LPBYTE lpRequestStubDataPointer;
	LPWSTR lpFixedNetworkResource;
	LPCWSTR szPreferredLocale = L"en-US,en";
	LPCWSTR szNetworkResource = L"\\root\\cimv2";
	PAUTH_INFO lpAuthInfo;

	dwPreferredLocaleLength = lstrlenW(szPreferredLocale) + 1;
	dwFixedNetworkResourceLength = lstrlenW(szNetworkResource) + lstrlenW(lpRemoteInstance->lpTargetHost) + lstrlenW(L"\\\\") + 1;
	lpFixedNetworkResource = (LPWSTR)calloc(dwFixedNetworkResourceLength, sizeof(WCHAR));

	if (StringCbPrintfW(
		lpFixedNetworkResource,
		dwFixedNetworkResourceLength * sizeof(WCHAR),
		L"\\\\%s%s",
		lpRemoteInstance->lpTargetHost,
		szNetworkResource) != S_OK)
	{
		PRINT_ERROR(L"Failed to format network resource string\n");
		free(lpFixedNetworkResource);
		return FALSE;
	}

	wStubDataLength = sizeof(NTLM_LOGIN_REQ);

	bPaddingLength = (8 - (dwFixedNetworkResourceLength % 8)) * sizeof(WCHAR);
	wStubDataLength -= (2 * sizeof(LPBYTE));
	wStubDataLength += (WORD)(dwFixedNetworkResourceLength * sizeof(WCHAR));
	wStubDataLength += bPaddingLength;

	bPaddingLength = (8 - (dwPreferredLocaleLength % 8)) * sizeof(WCHAR);
	wStubDataLength -= (2 * sizeof(LPBYTE));
	wStubDataLength += (WORD)(dwPreferredLocaleLength * sizeof(WCHAR));
	wStubDataLength += bPaddingLength;

	bPaddingLength = 0;

	lpRequestStubData = (LPBYTE)calloc(wStubDataLength, sizeof(BYTE));

	if (lpRequestStubData == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	((PNTLM_LOGIN_REQ)lpRequestStubData)->orpcthis.version.MajorVersion = 5;
	((PNTLM_LOGIN_REQ)lpRequestStubData)->orpcthis.version.MinorVersion = 7;
	((PNTLM_LOGIN_REQ)lpRequestStubData)->orpcthis.cid = lpRemoteInstance->clsCasualityID;

	lpRequestStubDataPointer = (LPBYTE)&((PNTLM_LOGIN_REQ)lpRequestStubData)->NetworkResource.lpNetworkResource;

	((PNTLM_LOGIN_REQ)lpRequestStubData)->NetworkResource.dwReferentID = 0x00020000;
	((PNTLM_LOGIN_REQ)lpRequestStubData)->NetworkResource.dwLength = dwFixedNetworkResourceLength;
	((PNTLM_LOGIN_REQ)lpRequestStubData)->NetworkResource.dwMaxCount = dwFixedNetworkResourceLength;

	memcpy_s(lpRequestStubDataPointer, dwFixedNetworkResourceLength * sizeof(WCHAR), lpFixedNetworkResource, dwFixedNetworkResourceLength * sizeof(WCHAR));
	lpRequestStubDataPointer += ((ULONG64)dwFixedNetworkResourceLength + ((ULONG64)8 - (dwFixedNetworkResourceLength % 8)))* sizeof(WCHAR);

	((DWORD*)lpRequestStubDataPointer)[0] = 0x00020004;
	((DWORD*)lpRequestStubDataPointer)[1] = dwPreferredLocaleLength;
	((DWORD*)lpRequestStubDataPointer)[3] = dwPreferredLocaleLength;
	lpRequestStubDataPointer += 4 * sizeof(DWORD);

	memcpy_s(lpRequestStubDataPointer, dwPreferredLocaleLength * sizeof(WCHAR), szPreferredLocale, dwPreferredLocaleLength * sizeof(WCHAR));

	if (wStubDataLength % 16 != 0)
		bPaddingLength = (16 - (wStubDataLength % 16));
	wAuthInfoLength = sizeof(AUTH_INFO) - sizeof(LPVOID) + bPaddingLength;
	lpAuthInfo = (PAUTH_INFO)calloc(wAuthInfoLength, sizeof(BYTE));

	CreateAuthInfo(AUTH_INFO_AUTH_LEVEL_PKT, bPaddingLength, 0, lpRemoteInstance->AuthInfo.sContextID, NULL, lpAuthInfo);

	wCurrentPacketLength = sizeof(DCE_RPC_OPNUM_REQUEST);
	wCurrentPacketLength += wAuthInfoLength + wStubDataLength;
	wCurrentPacketLength -= (sizeof(PAUTH_INFO) + sizeof(LPBYTE) + sizeof(LPVOID));
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	CreateOpnumRequest(
		wCurrentPacketLength + sizeof(NTLMSSP_VERIFIER),
		sizeof(NTLMSSP_VERIFIER),
		dwCallID,
		wContextID,
		6, // IWbemLevel1Login::NTLMLogin
		&icIWbemLevel1Login.uuIPID,
		wStubDataLength,
		wAuthInfoLength,
		lpAuthInfo,
		lpRequestStubData,
		(PDCE_RPC_OPNUM_REQUEST)lpCurrentPacket);
	free(lpRequestStubData);
	free(lpAuthInfo);

	lpOldReallocBuffer = lpCurrentPacket;
	lpCurrentPacket = (LPBYTE)realloc(lpCurrentPacket, wCurrentPacketLength + sizeof(NTLMSSP_VERIFIER));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"realloc");
		free(lpOldReallocBuffer);
		return FALSE;
	}

	if (!CalculateMessageSignature(
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].lpSigningKey,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].wSigningKeyLength,
		lpCurrentPacket,
		wCurrentPacketLength,
		lpRemoteInstance->AuthInfo.arrAuthenticationContext[lpRemoteInstance->AuthInfo.sContextID].dwSequenceNumber++,
		(PNTLMSSP_VERIFIER)&lpCurrentPacket[wCurrentPacketLength]))
	{
		PRINT_ERROR(L"Failed to calculate NTLMSSP verifier\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketLength += sizeof(NTLMSSP_VERIFIER);

	WriteSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketLength);
	free(lpCurrentPacket);

	wCurrentPacketLength = sizeof(DCE_RPC_OPNUM_RESPONSE) - (sizeof(LPVOID) + sizeof(LPBYTE) + sizeof(PAUTH_INFO));
	lpCurrentPacket = (LPBYTE)malloc(wCurrentPacketLength);

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"malloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketLength, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketLength)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	wCurrentPacketLength = (WORD)(((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint +
		((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->DceRpcPacket.AuthLength +
		sizeof(AUTH_INFO) - sizeof(LPVOID));
	if ((((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint > 0) &&
		((((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint % 16) != 0))
		wCurrentPacketLength += (16 - (((PDCE_RPC_OPNUM_RESPONSE)lpCurrentPacket)->AllocHint % 16));
	free(lpCurrentPacket);

	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpc, (LPSTR)lpCurrentPacket, wCurrentPacketLength, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketLength)
	{
		PRINT_ERROR(L"Failed to read from socket\n");
		free(lpCurrentPacket);
		return FALSE;
	}

	lpRequestStubData = (LPBYTE)&((PNTLM_LOGIN_RESP)lpCurrentPacket)->ppNamespace.abData;
	
	lpicIWbemServices->lpOXID = (LPBYTE)calloc(8, sizeof(BYTE));

	if (lpicIWbemServices->lpOXID == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	memcpy_s(lpicIWbemServices->lpOXID, 8 * sizeof(BYTE), ((OBJREF_PROC_OUT*)lpRequestStubData)->u_objref.std.OXID, 8 * sizeof(BYTE));
	lpicIWbemServices->uuIPID = ((OBJREF_PROC_OUT*)lpRequestStubData)->u_objref.std.IPID;
	return TRUE;
}