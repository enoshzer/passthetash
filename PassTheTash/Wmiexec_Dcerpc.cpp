#include "Wmiexec_Dcerpc.h"

/// <summary>
///	Creates a AUTH3 dce/rpc packet request
/// </summary>
/// <param name="FragLength">The length of the whole packet</param>
/// <param name="AuthLength">The length of the AuthInfo data field</param>
/// <param name="CallID">The sequential id of the packet in the current conversation</param>
/// <param name="lpAuthInfo">A pointer to the authentication information struct</param>
/// <param name="lpDceRpcPacket">A pointer to the variable that will hold the packet's data</param>
VOID CreateDceRpcAuth3Request(
	IN WORD FragLength,
	IN WORD AuthLength,
	IN DWORD CallID,
	IN PAUTH_INFO lpAuthInfo,
	OUT PDCE_RPC_AUTH3_PACKET lpDceRpcPacket)
{
	CreateBasicDceRpcPacket(
		DCERPC_PACKET_TYPE_AUTH3,
		DCERPC_PACKET_FLAG_FIRST_FRAG | DCERPC_PACKET_FLAG_LAST_FRAG,
		FragLength,
		(lpAuthInfo == NULL) ? (AuthLength) : (AuthLength - 8),
		CallID,
		&lpDceRpcPacket->DceRpcPacket);
	lpDceRpcPacket->MaxXmitFrag = MAX_XMIT_FRAG;
	lpDceRpcPacket->MaxRecvFrag = MAX_RECV_FRAG;
	if (lpAuthInfo != NULL && AuthLength > 0)
		memcpy_s(&lpDceRpcPacket->AuthInfo, AuthLength, lpAuthInfo, AuthLength);
}

VOID CreateOpnumRequest(
	IN WORD FragLength,
	IN WORD AuthLength,
	IN DWORD CallID,
	IN WORD ContextID,
	IN WORD Opnum,
	IN OPTIONAL UUID* lpObjectUUID,
	IN DWORD dwStubDataLength,
	IN DWORD dwAuthInfoLength,
	IN OPTIONAL PAUTH_INFO lpAuthInfo,
	IN OPTIONAL LPBYTE lpStubData,
	OUT PDCE_RPC_OPNUM_REQUEST lpDceRpcMessage)
{
	LPBYTE lpStubDataPointer;
	CreateBasicDceRpcPacket(
		DCERPC_PACKET_TYPE_REQUEST,
		DCERPC_PACKET_FLAG_FIRST_FRAG | DCERPC_PACKET_FLAG_LAST_FRAG,
		FragLength,
		AuthLength,
		CallID,
		&lpDceRpcMessage->DceRpcPacket);

	lpDceRpcMessage->AllocHint =
		(dwStubDataLength + ((lpAuthInfo == NULL) ?
			0 : (AuthLength)));
	lpDceRpcMessage->ContextID = ContextID;
	lpDceRpcMessage->Opnum = Opnum;

	if (lpObjectUUID != NULL)
	{
		lpDceRpcMessage->ObjectUUID = *lpObjectUUID;
		lpStubDataPointer = (LPBYTE)&lpDceRpcMessage->StubData;
		lpDceRpcMessage->DceRpcPacket.PacketFlags |= DCERPC_PACKET_FLAG_OBJECT;
	}
	else
		lpStubDataPointer = (LPBYTE)&lpDceRpcMessage->ObjectUUID;

	if (lpStubData != NULL)
		memcpy_s(lpStubDataPointer, dwStubDataLength, lpStubData, dwStubDataLength);

	if (lpAuthInfo != NULL)
		memcpy_s(&lpStubDataPointer[dwStubDataLength], dwAuthInfoLength, lpAuthInfo, dwAuthInfoLength);
}

/// <summary>
/// Creates a dce/rpc request packet with context items and authentication information
/// </summary>
/// <param name="PacketType">Type of the request</param>
/// <param name="FragLength">The length of the whole packet</param>
/// <param name="AuthLength">The length of the AuthInfo data field</param>
/// <param name="CallID">The sequential id of the packet in the current conversation</param>
/// <param name="NumCtxItems">The number of context items</param>
/// <param name="lpContextItems">A pointer to the context items</param>
/// <param name="lpAuthInfo">A pointer to the authentication information struct</param>
/// <param name="lpDceRpcBindMessage">A pointer to the variable that will hold the packet's data</param>
VOID CreateDceRpcRequest(
	IN BYTE PacketType,
	IN WORD FragLength,
	IN WORD AuthLength,
	IN DWORD CallID,
	IN BYTE NumCtxItems,
	IN OPTIONAL PCONTEXT_ITEM lpContextItems,
	IN OPTIONAL PAUTH_INFO lpAuthInfo,
	OUT PDCE_RPC_FULL_REQUEST lpDceRpcBindMessage)
{
	CreateBasicDceRpcPacket(
		PacketType,
		DCERPC_PACKET_FLAG_FIRST_FRAG | DCERPC_PACKET_FLAG_LAST_FRAG,
		FragLength,
		(lpAuthInfo == NULL) ? (AuthLength) : (AuthLength - 8),
		CallID,
		&lpDceRpcBindMessage->DceRpcPacket);
	lpDceRpcBindMessage->MaxXmitFrag = MAX_XMIT_FRAG;
	lpDceRpcBindMessage->MaxRecvFrag = MAX_RECV_FRAG;
	lpDceRpcBindMessage->AssocGroup = 0;
	lpDceRpcBindMessage->NumCtxItems = NumCtxItems;

	if (lpContextItems != NULL && NumCtxItems > 0)
		memcpy_s(&lpDceRpcBindMessage->ContextItems, NumCtxItems * sizeof(CONTEXT_ITEM), lpContextItems, NumCtxItems * sizeof(CONTEXT_ITEM));

	if (lpAuthInfo != NULL && AuthLength > 0)
		memcpy_s((LPBYTE)(&lpDceRpcBindMessage->ContextItems) + (sizeof(CONTEXT_ITEM) * NumCtxItems), AuthLength, lpAuthInfo, AuthLength);
}

/// <summary>
///	Creates a basic dce/rpc packet with adequate data for the tool usage
/// </summary>
/// <param name="Type">Type of the request</param>
/// <param name="Flags">Flags of the request</param>
/// <param name="FragLength">The length of the whole packet</param>
/// <param name="AuthLength">The length of the AuthInfo data field</param>
/// <param name="CallID">The sequential id of the packet in the current conversation</param>
/// <param name="lpDceRpcPacket">A pointer to the variable that will hold the packet's data</param>
VOID CreateBasicDceRpcPacket(
	IN BYTE Type,
	IN BYTE Flags,
	IN WORD FragLength,
	IN WORD AuthLength,
	IN DWORD CallID,
	OUT PDCE_RPC_PACKET lpDceRpcPacket) 
{
	lpDceRpcPacket->Version = 5;
	lpDceRpcPacket->MinorVerison = 0;
	lpDceRpcPacket->PacketType = Type;
	lpDceRpcPacket->PacketFlags = Flags;
	lpDceRpcPacket->DataRepresentation = 0x10;
	lpDceRpcPacket->FragLength = FragLength;
	lpDceRpcPacket->AuthLength = AuthLength;
	lpDceRpcPacket->CallID = CallID;
}

BOOL CreateAlterContextPacket(
	IN DWORD dwCallID,
	IN UUID uuInterface,
	IN WORD wContextID,
	IN WORD wAuthLength,
	IN OPTIONAL PAUTH_INFO lpAuthInfo,
	OUT PDCE_RPC_FULL_REQUEST* lpDceRpcPacket,
	OUT LPWORD lpwDceRpcPacketLength)
{
	WORD wRequestLength;
	PDCE_RPC_FULL_REQUEST lpFullRequest;

	wRequestLength = sizeof(DCE_RPC_FULL_REQUEST) + wAuthLength + sizeof(CONTEXT_ITEM) -
		(sizeof(PCONTEXT_ITEM) + sizeof(PAUTH_INFO));
	lpFullRequest = (PDCE_RPC_FULL_REQUEST)calloc(wRequestLength, sizeof(BYTE));

	if (lpFullRequest == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateBasicDceRpcPacket(
		DCERPC_PACKET_TYPE_ALTER_CTX,
		DCERPC_PACKET_FLAG_FIRST_FRAG | DCERPC_PACKET_FLAG_LAST_FRAG,
		wRequestLength,
		wAuthLength,
		dwCallID,
		&lpFullRequest->DceRpcPacket);
	lpFullRequest->MaxXmitFrag = MAX_XMIT_FRAG;
	lpFullRequest->MaxRecvFrag = MAX_RECV_FRAG;
	lpFullRequest->NumCtxItems = 1;

	((PCONTEXT_ITEM)&lpFullRequest->ContextItems)[0].ContextID = wContextID;
	((PCONTEXT_ITEM)&lpFullRequest->ContextItems)[0].NumTransItems = 1;
	((PCONTEXT_ITEM)&lpFullRequest->ContextItems)[0].AbstractSyntax.Interface = uuInterface;
	((PCONTEXT_ITEM)&lpFullRequest->ContextItems)[0].AbstractSyntax.InterfaceVer = 0;
	((PCONTEXT_ITEM)&lpFullRequest->ContextItems)[0].AbstractSyntax.InterfaceVerMinor = 0;
	((PCONTEXT_ITEM)&lpFullRequest->ContextItems)[0].TransferSyntax.TransferSyntax = IID_INDR32bit;
	((PCONTEXT_ITEM)&lpFullRequest->ContextItems)[0].TransferSyntax.Ver = 2;

	if (wAuthLength > 0 && lpAuthInfo != NULL)
		memcpy_s(&((PCONTEXT_ITEM)&lpFullRequest->ContextItems)[1], wAuthLength, lpAuthInfo, wAuthLength);

	*lpDceRpcPacket = lpFullRequest;
	*lpwDceRpcPacketLength = wRequestLength;
	return TRUE;
}

BOOL CreateAuthBasedConnection(
	IN LPWSTR lpTargetHost,
	IN LPWSTR lpPort,
	IN LPWSTR lpUsername,
	IN LPWSTR lpDomain,
	IN LPWSTR lpHash,
	IN BYTE bRequestType,
	IN BYTE bContextItemsCount,
	IN  OUT PSHORT psAuthContextID,
	IN OPTIONAL PCONTEXT_ITEM lpContextItems,
	IN OPTIONAL BYTE AuthLevel,
	OUT LPBYTE* lpSingingKey,
	OUT PWORD lpwSigningKeyLength,
	IN OUT OPTIONAL SOCKET* lpDcerpcSocket,
	OUT LPDWORD lpdwCallID)
{
	BYTE bTmpAuthLevel;
	WORD wUsernameLength;
	WORD wDomainLength;
	WORD wNTLMSSPLength;
	WORD wAuthInfoLength;
	WORD wCurrentPacketLength;
	DWORD dwBytesRead;
	DWORD dwHexHashLength;
	DWORD dwComputerNameLength = 0;
	DWORD dwVerionNumbers[3];
	LPBYTE lpNTLMSSP;
	LPBYTE lpCurrentPacket;
	LPBYTE lpHexConvertedHash;
	LPBYTE lpTemporaryAttrPointer;
	LPWSTR lpComputerName;
	SOCKET sDcerpcSocket;
	ULONG64 ul64Timestamp;
	PAUTH_INFO lpAuthInfo;
	PNTLMSSP_CHALLENGE lpNTLMSSPChallenge;

	if (lpDcerpcSocket == NULL ||
		*lpDcerpcSocket == NULL)
	{
		if (CreateSocketConnection(lpTargetHost, lpPort, &sDcerpcSocket) != 0)
		{
			PRINT_ERROR(L"Failed to create Winsock socket\n");
			return FALSE;
		}
	}
	else
		sDcerpcSocket = *lpDcerpcSocket;


	if (AuthLevel == NULL)
		bTmpAuthLevel = AUTH_INFO_AUTH_LEVEL_CONNECT;
	else
		bTmpAuthLevel = AuthLevel;

	if (*lpdwCallID == 0)
		*lpdwCallID = 2;
	else
	{
		*lpdwCallID += 1;
	}

	wNTLMSSPLength = sizeof(NTLMSSP_NEGOTIATE);
	lpNTLMSSP = (LPBYTE)calloc(wNTLMSSPLength, sizeof(BYTE));

	if (lpNTLMSSP == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	wAuthInfoLength = sizeof(AUTH_INFO) - sizeof(LPVOID) + wNTLMSSPLength;
	lpAuthInfo = (PAUTH_INFO)calloc(wAuthInfoLength, sizeof(BYTE));

	if (lpAuthInfo == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!GetNtVersionNumbers(&dwVerionNumbers[0], &dwVerionNumbers[1], &dwVerionNumbers[2]))
	{
		PRINT_ERROR(L"Failed to get OS version number\n");
		return FALSE;
	}

	CreateNTLMSSPNegotiation(
		lpSingingKey != NULL,
		(BYTE)dwVerionNumbers[0], 
		(BYTE)dwVerionNumbers[1], 
		(WORD)dwVerionNumbers[2], 
		(PNTLMSSP_NEGOTIATE)lpNTLMSSP);
	CreateAuthInfo(bTmpAuthLevel, 0, wNTLMSSPLength, *psAuthContextID, lpNTLMSSP, lpAuthInfo);
	free(lpNTLMSSP);

	wCurrentPacketLength = sizeof(DCE_RPC_FULL_REQUEST) -
		sizeof(PCONTEXT_ITEM) + (bContextItemsCount * sizeof(CONTEXT_ITEM)) -
		sizeof(PAUTH_INFO) + wAuthInfoLength;
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateDceRpcRequest(bRequestType,
		wCurrentPacketLength,
		wAuthInfoLength,
		*lpdwCallID,
		bContextItemsCount,
		lpContextItems,
		lpAuthInfo, (PDCE_RPC_FULL_REQUEST)lpCurrentPacket);
	free(lpAuthInfo);

	WriteSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketLength);
	free(lpCurrentPacket);

	wCurrentPacketLength = sizeof(DCE_RPC_FULL_RESPONSE);
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketLength, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketLength)
	{
		if (dwBytesRead > 0)
		{
			if (*(DWORD*)&lpCurrentPacket[dwBytesRead - (2 * sizeof(DWORD))] == ERROR_ACCESS_DENIED)
			{
				PRINT_ERROR(L"Access denied on machine\n");
				free(lpCurrentPacket);
				return FALSE;
			}
			else
			{
				PRINT_ERROR(L"Server returned error no. %i\n", *(DWORD*)&lpCurrentPacket[dwBytesRead - (2 * sizeof(DWORD))]);
				free(lpCurrentPacket);
				return FALSE;
			}
		}
		else
		{
			PRINT_ERROR(L"Read socket error\n");
			free(lpCurrentPacket);
			return FALSE;
		}
	}

	wAuthInfoLength = ((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->DceRpcPacket.AuthLength;
	wCurrentPacketLength = sizeof(DCE_RPC_ADDITIONAL_FULL_DATA);
	wCurrentPacketLength += ((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen;
	if (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen != 6)
	{
		wCurrentPacketLength += (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen == 0) ? (2) :
			(6 - (((PDCE_RPC_FULL_RESPONSE)lpCurrentPacket)->ScndryAddrLen % 6));
	}
	wCurrentPacketLength += (bContextItemsCount * sizeof(CONTEXT_ITEM_ACK));
	wCurrentPacketLength += wAuthInfoLength;
	if (wAuthInfoLength > 0)
		wCurrentPacketLength += sizeof(AUTH_INFO) - sizeof(LPVOID);

	wCurrentPacketLength -= sizeof(LPBYTE) * 2;
	wCurrentPacketLength -= sizeof(PCONTEXT_ITEM_ACK);
	wCurrentPacketLength -= sizeof(PAUTH_INFO);
	free(lpCurrentPacket);

	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!ReadSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketLength, &dwBytesRead) ||
		dwBytesRead != wCurrentPacketLength)
	{
		if (dwBytesRead > 0)
		{
			if (*(DWORD*)&lpCurrentPacket[dwBytesRead - (2 * sizeof(DWORD))] == ERROR_ACCESS_DENIED)
			{
				PRINT_ERROR(L"Access denied on machine\n");
				free(lpCurrentPacket);
				return FALSE;
			}
			else
			{
				PRINT_ERROR(L"Server returned error no. %i\n", *(DWORD*)&lpCurrentPacket[dwBytesRead - (2 * sizeof(DWORD))]);
				free(lpCurrentPacket);
				return FALSE;
			}
		}
		else
		{
			PRINT_ERROR(L"Read socket error\n");
			free(lpCurrentPacket);
			return FALSE;
		}
	}

	dwHexHashLength = lstrlenW(lpHash) / 2;
	lpHexConvertedHash = (LPBYTE)calloc(dwHexHashLength, sizeof(BYTE));

	if (lpHexConvertedHash == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!ConvertStringToHex(lpHash, dwHexHashLength, lpHexConvertedHash))
	{
		PRINT_ERROR(L"Failed to convert string to hex value\n");
		free(lpHexConvertedHash);
		return FALSE;
	}

	lpNTLMSSPChallenge = (PNTLMSSP_CHALLENGE)&lpCurrentPacket[wCurrentPacketLength - wAuthInfoLength];

	if (!FindAttributeByType(lpNTLMSSPChallenge, TARGET_INFO_ITEM_TYPE_TIMESTAMP, &lpTemporaryAttrPointer, NULL))
	{
		PRINT_ERROR(L"Server timestamp not found\n");
		return FALSE;
	}

	ul64Timestamp = *(ULONG64*)lpTemporaryAttrPointer;

	lpComputerName = (LPWSTR)calloc(MAX_COMPUTERNAME_LENGTH + 1, sizeof(WCHAR));
	dwComputerNameLength = (MAX_COMPUTERNAME_LENGTH + 1);

	if (lpComputerName == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	if (!GetComputerNameW(lpComputerName, &dwComputerNameLength))
	{
		PRINT_ERROR_AUTO(L"GetComputerNameW");
		return FALSE;
	}

	dwComputerNameLength = dwComputerNameLength * sizeof(WCHAR);
	if (!CreateNTLMSSPChallengeResponse(
		lpUsername,
		lpDomain,
		lpHexConvertedHash,
		dwHexHashLength,
		lpNTLMSSPChallenge->Challenge,
		ul64Timestamp,
		lpNTLMSSPChallenge->TargetInfo.Length,
		((LPBYTE)lpNTLMSSPChallenge + lpNTLMSSPChallenge->TargetInfo.Offset),
		&wNTLMSSPLength,
		&lpNTLMSSP,
		lpwSigningKeyLength,
		lpSingingKey))
	{
		PRINT_ERROR(L"Failed to generate NTLMSSP AUTH3 response\n");
		return FALSE;
	}

	free(lpCurrentPacket);

	wUsernameLength = ((WORD)lstrlenW(lpUsername)) * sizeof(WCHAR);
	wDomainLength = ((WORD)lstrlenW(lpDomain)) * sizeof(WCHAR);
	wCurrentPacketLength = (WORD)sizeof(NTLMSSP_AUTH3) + wDomainLength + wUsernameLength + (WORD)dwComputerNameLength +
		wNTLMSSPLength + NTLM_SSP_MESSAGE_LM_RESPONSE_LENGTH;
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateNTLMSSPAuth3(
		lpNTLMSSP,
		wNTLMSSPLength,
		(LPBYTE)lpDomain,
		wDomainLength,
		(LPBYTE)lpUsername,
		wUsernameLength,
		(LPBYTE)lpComputerName,
		(WORD)dwComputerNameLength,
		(PNTLMSSP_AUTH3)lpCurrentPacket);

	free(lpNTLMSSP);
	lpNTLMSSP = lpCurrentPacket;
	wNTLMSSPLength = wCurrentPacketLength;

	wAuthInfoLength = sizeof(AUTH_INFO) - sizeof(LPVOID) + wNTLMSSPLength;
	lpAuthInfo = (PAUTH_INFO)calloc(wAuthInfoLength, sizeof(BYTE));

	if (lpAuthInfo == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateAuthInfo(bTmpAuthLevel, 0, wNTLMSSPLength, *psAuthContextID, lpNTLMSSP, lpAuthInfo);
	free(lpNTLMSSP);

	wCurrentPacketLength = sizeof(DCE_RPC_AUTH3_PACKET) - sizeof(PAUTH_INFO) + wAuthInfoLength;
	lpCurrentPacket = (LPBYTE)calloc(wCurrentPacketLength, sizeof(BYTE));

	if (lpCurrentPacket == NULL)
	{
		PRINT_ERROR_AUTO(L"calloc");
		return FALSE;
	}

	CreateDceRpcAuth3Request(wCurrentPacketLength, wAuthInfoLength, *lpdwCallID, lpAuthInfo, (PDCE_RPC_AUTH3_PACKET)lpCurrentPacket);
	free(lpAuthInfo);

	WriteSocketData(sDcerpcSocket, (LPSTR)lpCurrentPacket, wCurrentPacketLength);
	free(lpCurrentPacket);

	if (lpDcerpcSocket == NULL)
		return DisconnectSocket(sDcerpcSocket, TRUE);
	else if (*lpDcerpcSocket == NULL)
		*lpDcerpcSocket = sDcerpcSocket;

	return TRUE;
 }